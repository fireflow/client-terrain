import React from 'react';
import CommunicationPage from "~/views/Communication/CommunicationPage";
import customRender from "../utils/testHelper";
import Intervention from "~/models/intervention.model";
import OctDao from "~/models/oct.dao";
import {act} from "react-dom/test-utils";
import {fireEvent, waitFor} from "@testing-library/react-native";
import OctModel from "~/models/oct.model";
import AsyncStorage from "@react-native-async-storage/async-storage";

describe('CommunicationPage', () => {
    const intervention = new Intervention("abc");

    let spyGetOct;
    let spyGetAuthToken;

    beforeAll(() => {
        spyGetOct = jest.spyOn(OctDao, 'getOct');
        spyGetOct.mockResolvedValue(new OctModel(120, [210, 211], [420], [
            {
                "name": "INCENDIE",
                "ltn": "LTN QUATRE",
                "frequencies": [612, 643],
                "child": {
                    "name": "INCENDIE",
                    "ltn": "LTN CINQ",
                    "frequencies": [613, 644],
                    "child": {
                        "name": "INCENDIE",
                        "ltn": "LTN SIX",
                        "frequencies": [614, 645]
                    }
                }
            }
        ]));

        spyGetAuthToken = jest.spyOn(AsyncStorage, "getItem");
        spyGetAuthToken.mockResolvedValue("token");
    })

    afterEach(() => {
        spyGetOct.mockReset();
        spyGetAuthToken.mockReset();
    })

    afterAll(() => {
        jest.clearAllMocks();
    })

    //FIXME le component se render en mode loading, malgré le faite que les states soient bien setup (passe dans le then() de refreshOct())
    it('contains all element', async () => {
        const providerProps = { value: [{intervention: intervention}, jest.fn()]};
        const tree = customRender(<CommunicationPage/>, {providerProps})
        /*
        tree: ContextProvider
        tree -> first_child: CommunicationPage Component
         */
        //console.log(tree.container.children[0])

        await waitFor(() => {
            expect(tree.toJSON().children.length).toBe(2); //Pas le bon champs de retour
        })
    })

    it('renders correctly', () => {
        const providerProps = { value: [{intervention: intervention}, jest.fn()]};
        const tree = customRender(<CommunicationPage/>, {providerProps}).toJSON()
        expect(tree).toMatchSnapshot();
    })

    it('should not oct sectors because of no intervention', async () => {
        const providerProps = { value: [{intervention: undefined}, jest.fn()]};
        const container = customRender(<CommunicationPage/>, {providerProps})

        act(() => expect(spyGetOct).toBeCalledTimes(0))

        const element = await container.queryByTestId('octView');
        expect(element).toBeNull();

        //expect(container.getByTestId('octErrorText').props.children).toBe('ERREUR LORS DE LA RECUPERATION DE L\'OCT')
    })

    it('should not oct sectors because of failed request', async () => {
        spyGetOct.mockRejectedValueOnce(new Error('Failed'));

        const providerProps = { value: [{intervention: intervention}, jest.fn()]};
        const container = customRender(<CommunicationPage/>, {providerProps})

        act(() => expect(spyGetOct).toBeCalledTimes(0))

        const element = await container.queryByTestId('octView');
        expect(element).toBeNull();

        /*const webview = container.getByTestId("webview");
        fireEvent(webview, "error", {
            nativeEvent: { data: "Failed to load Webview" },
        })*/

        //FIXME trouver comment mock et controller les events de la webview
        //expect(container.getByTestId('octErrorText').props.children).toBe('ERREUR LORS DE LA RECUPERATION DE L\'OCT')
    })

    /*it('should display the right number of oct sectors', async () => {
        const providerProps = { value: [{intervention: intervention}, jest.fn()]};
        let container;
        await act(() => {
            container = customRender(<CommunicationPage/>, {providerProps})
        })

        act(() => expect(spyGetOct).toBeCalledTimes(1));

        act(() => {
            fireEvent.press(container.getByTestId("refreshOctButton"));
        });

        const element = container.queryByTestId('octView');
        expect(element).toBeTruthy();
        expect(element.children.length).toBe(4)
    })*/
})
