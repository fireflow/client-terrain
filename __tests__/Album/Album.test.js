import React from 'react';
import Alert from "~/views/Alert/Alert";
import Intervention from "~/models/intervention.model";
import customRender from "../utils/testHelper";
import {fireEvent, render} from "@testing-library/react-native";
import Album from "~/components/Album";

describe('Album', () => {
    const pictures = [{
            name: 'picture 1',
            description: 'First Picture',
            src: 'url'
        },
        {
            name: 'picture 2',
            description: 'Second Picture',
            src: 'url'
        }];

    it('contains all element', () => {
        const tree = render(<Album onInspectImage={jest.fn()} pictures={pictures}/>).toJSON();
        expect(tree.children.length).toBe(1);
    })

    it('display correct number of pictures', () => {
        const tree = render(<Album onInspectImage={jest.fn()} pictures={pictures}/>).toJSON();
        expect(tree.children[0].children.length).toBe(2);
    })

    it('display info message when there is no pictures', () => {
        const container = render(<Album onInspectImage={jest.fn()} pictures={[]}/>);

        expect(container.getByTestId('no-picture-msg').children[0]).toBe("L'intervention n'a aucune photo.");
    })

    it('renders correctly', () => {
        const tree = render(<Album onInspectImage={jest.fn()} pictures={pictures}/>).toJSON();

        expect(tree).toMatchSnapshot();
    })

    it('should inspect image on press', () => {
        const onInspectFn = jest.fn();
        const container = render(<Album onInspectImage={onInspectFn} pictures={pictures}/>);

        //const alertInstance = alert.dive().instance();
        fireEvent.press(container.getByTestId('picture-1'));

        expect(onInspectFn.mock.calls.length).toBe(1);
    })
})
