import React from 'react';
import EditTeam from "../../src/views/EditTeam/EditTeam";
import Unit, {UnitRoles} from "~/models/unit.model";
import customRender from "../utils/testHelper";
import Intervention from "~/models/intervention.model";
import {fireEvent, waitFor} from "@testing-library/react-native";
import { act } from 'react-dom/test-utils';
import UnitDao from "~/models/unit.dao";


describe('EditTeam', () => {
    afterEach(() => {
        jest.clearAllMocks();
    })


    const agentChiefUnit = new Unit('ID', 'Group-01', 'QSDF', 'CCF', 500, 'ABCD', UnitRoles.AgentChief, [], 1, {serial: 'azer', first_name: 'John', last_name: 'Doe'})
    const groupChiefUnit = new Unit('ID', 'Group-01', 'QSDF', 'CCF', 500, 'ABCD', UnitRoles.GroupChief, [], 1, {serial: 'mlkj', first_name: 'Hugo', last_name: 'Boichard'})
    const columnChiefUnit = new Unit('ID', 'Group-01', 'QSDF', 'CCF', 500, 'ABCD', UnitRoles.ColumnChief, [], 1, {serial: 'azer', first_name: 'John', last_name: 'Doe'})

    const groupChiefUnitWithChildren = new Unit('ID', 'Group-01', 'QSDF', 'CCF', 500, 'ABCD', UnitRoles.GroupChief, [
        new Unit('ID4', 'Group-02', 'azer', 'CCF', 500, 'ABCD', UnitRoles.AgentChief, [], 1, {serial: 'azer', first_name: 'John', last_name: 'Doe'}),
        new Unit('ID3', 'Group-03', 'qsdf', 'CCF', 500, 'ABCD', UnitRoles.AgentChief, [], 1, {serial: 'qsdf', first_name: 'Jeanne', last_name: 'Marlo'})
    ], 3, {serial: 'mlkj', first_name: 'Hugo', last_name: 'Boichard'})
    it('contains all element', () => {
        const providerProps = { value: [{unit: groupChiefUnit, intervention: new Intervention()}, jest.fn()]};
        const tree = customRender(<EditTeam navigation={null} />, {providerProps}).toJSON()
        expect(tree.children.length).toBe(2)
    })

    it('renders correctly', () => {
        const providerProps = { value: [{unit: groupChiefUnit, intervention: new Intervention()}, jest.fn()]};
        const tree = customRender(<EditTeam navigation={null} />, {providerProps}).toJSON()
        expect(tree).toMatchSnapshot()
    })

    it('should return the right title for group unit', () => {
        const providerProps = { value: [{unit: groupChiefUnit, intervention: new Intervention()}, jest.fn()]};
        const container = customRender(<EditTeam navigation={null} />, {providerProps})

        const text = container.getByTestId('view-title').children[0];

        expect(text).toBe('Gestion du groupe')
    })

    it('should return the right title for column unit', () => {
        const providerProps = { value: [{unit: columnChiefUnit, intervention: new Intervention()}, jest.fn()]};
        const container = customRender(<EditTeam navigation={null} />, {providerProps})

        const text = container.getByTestId('view-title').children[0];

        expect(text).toBe('Gestion de la colonne')
    })

    it('should return the right header for the available grid', () => {
        const providerProps = { value: [{unit: groupChiefUnit, intervention: new Intervention()}, jest.fn()]};
        const container = customRender(<EditTeam navigation={null} />, {providerProps})

        const text = container.getByTestId('header-available-grid').children.join('');

        expect(text).toBe('Unités disponibles')
    })

    it('should return the right header for the group grid', () => {
        const providerProps = { value: [{unit: groupChiefUnit, intervention: new Intervention()}, jest.fn()]};
        const container = customRender(<EditTeam navigation={null} />, {providerProps})

        const text = container.getByTestId('header-team-grid').children[0];

        expect(text).toBe('Unités du groupe')
    })

    it('should return the right header for the column grid', () => {
        const providerProps = { value: [{unit: columnChiefUnit, intervention: new Intervention()}, jest.fn()]};
        const container = customRender(<EditTeam navigation={null} />, {providerProps})

        const text = container.getByTestId('header-team-grid').children[0];

        expect(text).toBe('Groupes de la colonne')
    })

    it('should assign units correctly', async () => {
        const spyUnitAssign= jest.spyOn(UnitDao, 'assign')
        spyUnitAssign.mockResolvedValueOnce({});

        const spyGetAvailableUnits = jest.spyOn(UnitDao, 'getAvailable')
        spyGetAvailableUnits.mockResolvedValueOnce([
            new Unit('ID4', 'Group-02', 'azer', 'CCF', 500, 'ABCD', UnitRoles.AgentChief, [], 1, {serial: 'azer', first_name: 'John', last_name: 'Doe'}),
            new Unit('ID3', 'Group-03', 'qsdf', 'CCF', 500, 'ABCD', UnitRoles.AgentChief, [], 1, {serial: 'qsdf', first_name: 'Jeanne', last_name: 'Marlo'})
        ]);

        const providerProps = { value: [{unit: groupChiefUnit, intervention: new Intervention()}, jest.fn()]};
        const container = customRender(<EditTeam />, {providerProps});

        await waitFor(() => expect(spyGetAvailableUnits).toBeCalledTimes(1));

        const availableUnits = await container.findAllByTestId('availableUnit');

        expect(availableUnits.length).toBe(2)

        fireEvent.press(availableUnits[0])

        act(() => {
            fireEvent.press(container.getByTestId('addBtn'))
        });

        await waitFor(() => expect(spyUnitAssign).toBeCalledTimes(1))

        const newAvailableUnits= container.findAllByTestId('availableUnit');

        expect((await newAvailableUnits).length).toBe(1)

        spyGetAvailableUnits.mockClear();
    })

    it('should remove assigned units correctly', async () => {
        const spyUnitAssign= jest.spyOn(UnitDao, 'assign')
        spyUnitAssign.mockResolvedValueOnce({});

        const spyGetAvailableUnits = jest.spyOn(UnitDao, 'getAvailable')
        spyGetAvailableUnits.mockResolvedValueOnce([]);

        const providerProps = { value: [{unit: groupChiefUnitWithChildren, intervention: new Intervention()}, jest.fn()]};
        const container = customRender(<EditTeam />, {providerProps});

        await waitFor(() => expect(spyGetAvailableUnits).toBeCalledTimes(1));

        const unitsToRemove = await container.findAllByTestId('assignedUnit');

        expect(unitsToRemove.length).toBe(2)

        fireEvent.press(unitsToRemove[0])

        act(() => {
            fireEvent.press(container.getByTestId('removeBtn'))
        });

        await waitFor(() => expect(spyUnitAssign).toBeCalledTimes(1))

        const newUnitsToRemove = container.findAllByTestId('assignedUnit');

        expect((await newUnitsToRemove).length).toBe(1)

        //expect(text).toBe('Unités du groupe')
    })
})
