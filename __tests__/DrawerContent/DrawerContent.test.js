import React from 'react';
import renderer from 'react-test-renderer';
import {fireEvent, render} from "@testing-library/react-native";
import shallowRenderer from 'react-test-renderer/shallow';
import DrawerContent from "~/components/DrawerContent";
import {shallow} from "enzyme";

describe('DrawerContent', () => {
    it('contains all element', () => {
        const drawerContent = shallow(<DrawerContent navigation={{
            navigate: jest.fn()
        }} />)
        expect(drawerContent.children().length).toBe(9);
    })

    it('renders correctly', () => {
        const result = render(<DrawerContent navigation={{
            navigate: jest.fn()
        }} />).toJSON()
        expect(result).toMatchSnapshot()
    })

    it('redirects correctly', () => {
        const drawerContent = render(<DrawerContent navigation={{
            navigate: jest.fn()
        }} />)

        fireEvent.press(drawerContent.getByTestId('toAlert'))
        expect(drawerContent.container.props.navigation.navigate.mock.calls[0][0]).toBe('Alert')
    })

    it('closes correctly', () => {
        const drawerContent = render(<DrawerContent navigation={{
            closeDrawer: jest.fn()
        }} />)
        fireEvent.press(drawerContent.getByTestId('close'))
        //drawerContent.findWhere((node) => node.prop('testID') === 'close').simulate('press')
        expect(drawerContent.container.props.navigation.closeDrawer.mock.calls.length).toBe(1)
    })

    it('redirects to Home correctly', () => {
        const drawerContent = render(<DrawerContent navigation={{
            navigate: jest.fn()
        }} />)
        fireEvent.press(drawerContent.getByTestId('toHome'))
        //drawerContent.findWhere((node) => node.prop('testID') === 'toHome').simulate('press')
        expect(drawerContent.container.props.navigation.navigate.mock.calls[0][0]).toBe('Home')
    })

    it('redirects to Communication correctly', () => {
        const drawerContent = render(<DrawerContent navigation={{
            navigate: jest.fn()
        }} />)
        fireEvent.press(drawerContent.getByTestId('toCommunication'))
        //drawerContent.findWhere((node) => node.prop('testID') === 'toCommunication').simulate('press')
        expect(drawerContent.container.props.navigation.navigate.mock.calls[0][0]).toBe('Communication')
    })

    it('redirects to Status correctly', () => {
        const drawerContent = render(<DrawerContent navigation={{
            navigate: jest.fn()
        }} />)
        fireEvent.press(drawerContent.getByTestId('toStatus'))
        //drawerContent.findWhere((node) => node.prop('testID') === 'toStatus').simulate('press')
        expect(drawerContent.container.props.navigation.navigate.mock.calls[0][0]).toBe('Status')
    })

    it('redirects to Alert correctly', () => {
        const drawerContent = render(<DrawerContent navigation={{
            navigate: jest.fn()
        }} />)
        fireEvent.press(drawerContent.getByTestId('toAlert'))
        //drawerContent.findWhere((node) => node.prop('testID') === 'toAlert').simulate('press')
        expect(drawerContent.container.props.navigation.navigate.mock.calls[0][0]).toBe('Alert')
    })

    it('redirects to Info correctly', () => {
        const drawerContent = render(<DrawerContent navigation={{
            navigate: jest.fn()
        }} />)
        fireEvent.press(drawerContent.getByTestId('toInfo'))
        //drawerContent.findWhere((node) => node.prop('testID') === 'toInfo').simulate('press')
        expect(drawerContent.container.props.navigation.navigate.mock.calls[0][0]).toBe('Information')
    })

    it('redirects to Profile correctly', () => {
        const drawerContent = render(<DrawerContent navigation={{
            navigate: jest.fn()
        }} />)
        fireEvent.press(drawerContent.getByTestId('toProfile'))
        //drawerContent.findWhere((node) => node.prop('testID') === 'toProfile').simulate('press')
        expect(drawerContent.container.props.navigation.navigate.mock.calls[0][0]).toBe('ProfileAndTeam')
    })
})
