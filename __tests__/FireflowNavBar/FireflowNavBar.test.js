import React from 'react';
import {shallow} from "enzyme";
import {fireEvent, render} from "@testing-library/react-native";
import FireflowNavBar from "~/components/FireflowNavBar";

describe('FireflowNavBar', () => {
    it('contains all element', () => {
        const tree = render(<FireflowNavBar />).toJSON()
        expect(tree.children.length).toBe(1);
    })

    it('renders correctly', () => {
        const tree = render(<FireflowNavBar />).toJSON()
        expect(tree).toMatchSnapshot();
    })

    it('redirects correctly', () => {
        const loginRedirect = render(<FireflowNavBar navigation={{
            navigate: jest.fn()
        }} />)
        fireEvent.press(loginRedirect.getByTestId('toAlert'))
        expect(loginRedirect.container.props.navigation.navigate.mock.calls[0][0]).toBe('Alert')
    })

    it('opens correctly', () => {
        const loginRedirect = render(<FireflowNavBar navigation={{
            openDrawer: jest.fn()
        }} />)
        fireEvent.press(loginRedirect.getByTestId('open'))
        expect(loginRedirect.container.props.navigation.openDrawer.mock.calls.length).toBe(1)
    })

    it('redirects to Home correctly', () => {
        const loginRedirect = render(<FireflowNavBar navigation={{
            navigate: jest.fn()
        }} />)
        fireEvent.press(loginRedirect.getByTestId('toHome'))
        expect(loginRedirect.container.props.navigation.navigate.mock.calls[0][0]).toBe('Home')
    })

    it('redirects to Communication correctly', () => {
        const loginRedirect = render(<FireflowNavBar navigation={{
            navigate: jest.fn()
        }} />)
        fireEvent.press(loginRedirect.getByTestId('toCommunication'))
        expect(loginRedirect.container.props.navigation.navigate.mock.calls[0][0]).toBe('Communication')
    })

    it('redirects to Status correctly', () => {
        const loginRedirect = render(<FireflowNavBar navigation={{
            navigate: jest.fn()
        }} />)
        fireEvent.press(loginRedirect.getByTestId('toStatus'))
        expect(loginRedirect.container.props.navigation.navigate.mock.calls[0][0]).toBe('Status')
    })

    it('redirects to Alert correctly', () => {
        const loginRedirect = render(<FireflowNavBar navigation={{
            navigate: jest.fn()
        }} />)
        fireEvent.press(loginRedirect.getByTestId('toAlert'))
        expect(loginRedirect.container.props.navigation.navigate.mock.calls[0][0]).toBe('Alert')
    })

    it('redirects to Info correctly', () => {
        const loginRedirect = render(<FireflowNavBar navigation={{
            navigate: jest.fn()
        }} />)
        fireEvent.press(loginRedirect.getByTestId('toInfo'))
        expect(loginRedirect.container.props.navigation.navigate.mock.calls[0][0]).toBe('Information')
    })

    it('redirects to Profile correctly', () => {
        const loginRedirect = render(<FireflowNavBar navigation={{
            navigate: jest.fn()
        }} />)
        fireEvent.press(loginRedirect.getByTestId('toProfile'))
        expect(loginRedirect.container.props.navigation.navigate.mock.calls[0][0]).toBe('ProfileAndTeam')
    })
})
