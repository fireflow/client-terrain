import React from 'react';
import renderer from 'react-test-renderer';
import {fireEvent, render} from '@testing-library/react-native';
import { shallow } from 'enzyme';
import LessAndMoreField from "~/components/LessAndMoreField";

describe('LessAndMoreField', () => {
    it('contains all element', () => {
        const tree = render(<LessAndMoreField />).toJSON()
        expect(tree.children.length).toBe(2);
    })

    it('renders correctly', () => {
        const tree = render(<LessAndMoreField />).toJSON()
        expect(tree).toMatchSnapshot();
    })

    it('should add one to the value', () => {
        const lessAndMoreField = render(<LessAndMoreField value={5} />)
        fireEvent.press(lessAndMoreField.getByTestId("onAddButton"))
        expect(lessAndMoreField.getByTestId("valueText").children[0]).toBe("6")
    })

    it('should subtract one to the value', () => {
        const lessAndMoreField = render(<LessAndMoreField value={5} />)
        fireEvent.press(lessAndMoreField.getByTestId("onMinusButton"))
        expect(lessAndMoreField.getByTestId("valueText").children[0]).toBe("4")
    })

    it('should cancel subtract one to the value', () => {
        const lessAndMoreField = render(<LessAndMoreField value={1} />)
        fireEvent.press(lessAndMoreField.getByTestId("onMinusButton"))
        expect(lessAndMoreField.getByTestId("valueText").children[0]).toBe("1")
    })
})
