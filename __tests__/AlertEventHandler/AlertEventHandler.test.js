import React from 'react';
import AlertEventHandler from "~/components/AlertEventHandler";
import {render} from "@testing-library/react-native";
import customRender from "../utils/testHelper";
import EditTeam from "~/views/EditTeam/EditTeam";
import EventService from "~/services/events/EventService";
import {EventsType} from "~/services/events/types";

describe('AlertEventHandler', () => {
    const event = {
        author: '123',
        type: EventsType.Alert,
        value: {
            message: 'alerte'
        }
    }

    it('contains all element', () => {
        const tree = render(<AlertEventHandler />).toJSON()
        expect(tree.children).toBe(null);
    })

    it('renders correctly', () => {
        const tree = render(<AlertEventHandler />).toJSON()
        expect(tree).toMatchSnapshot();
    })

    it('register event handler for alert events', () => {
        const interventionEventService = new EventService();
        const spyEventHandler = jest.spyOn(interventionEventService, 'registerEventHandler');
        const providerProps = { value: [{interventionEventService}, jest.fn()]};

        const container = customRender(<AlertEventHandler />, {providerProps});

        expect(spyEventHandler).toBeCalled();

    })
})
