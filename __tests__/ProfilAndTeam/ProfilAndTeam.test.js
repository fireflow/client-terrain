import React from 'react';
import ProfileAndTeam from '~/views/ProfilAndTeam/ProfileAndTeam';
import Unit, {UnitRoles} from "~/models/unit.model";
import UnitDao from "~/models/unit.dao";
import Intervention from "~/models/intervention.model";
import customRender from "../utils/testHelper";
import {act} from "react-dom/test-utils";
import {fireEvent, waitFor} from "@testing-library/react-native";

describe('ProfilAndTeam', () => {
    const agentChiefUnit = new Unit('ID', 'Group-01', 'QSDF', 'CCF', 500, 'ABCD', UnitRoles.AgentChief, [], 1, {serial: 'azer', first_name: 'John', last_name: 'Doe'})
    const groupChiefUnit = new Unit('ID', 'Group-01', 'QSDF', 'CCF', 500, 'ABCD', UnitRoles.GroupChief, [], 1, {serial: 'azer', first_name: 'John', last_name: 'Doe'})
    const columnChiefUnit = new Unit('ID', 'Group-01', 'QSDF', 'CCF', 500, 'ABCD', UnitRoles.ColumnChief, [], 1, {serial: 'azer', first_name: 'John', last_name: 'Doe'})

    let spyUnitGetMe;
    afterEach(() => {
        spyUnitGetMe.mockClear();
    })

    it('contains all element', async () => {
        spyUnitGetMe = jest.spyOn(UnitDao, 'getMe').mockResolvedValue(groupChiefUnit);
        const providerProps = { value: [{unit: groupChiefUnit, intervention: new Intervention(), vehicles: []}, jest.fn()]};
        const tree = customRender(<ProfileAndTeam navigation={{ navigate: jest.fn() }} route={{}} />, {providerProps});
        
        await waitFor(() => {
            expect(tree.toJSON().children.length).toBe(3)
        })
    })

    it('renders correctly', async () => {
        spyUnitGetMe = jest.spyOn(UnitDao, 'getMe').mockResolvedValue(groupChiefUnit);
        const providerProps = { value: [{unit: groupChiefUnit, intervention: new Intervention(), vehicles: []}, jest.fn()]};
        const tree = customRender(<ProfileAndTeam navigation={{ navigate: jest.fn() }} route={{}} />, {providerProps});
        
        await waitFor(() => {
            expect(tree.toJSON()).toMatchSnapshot()
        })  
    })

    it('should return the group chief edit button text', async () => {
        spyUnitGetMe = jest.spyOn(UnitDao, 'getMe').mockResolvedValue(groupChiefUnit);
        const providerProps = { value: [{unit: groupChiefUnit, intervention: new Intervention(), vehicles: []}, jest.fn()]};
            
        const container = customRender(<ProfileAndTeam navigation={{ navigate: jest.fn() }} route={{}} />, {providerProps});

        await waitFor(() => {
            const text = container.getByTestId("toTeamEdit");
            expect(text.props.children).toBe('Gérer le groupe')
        })
    })

    it('should not find a edit button for agent chief', async () => {
        spyUnitGetMe = jest.spyOn(UnitDao, 'getMe').mockResolvedValue(agentChiefUnit);
        const providerProps = { value: [{unit: agentChiefUnit, intervention: new Intervention(), vehicles: []}, jest.fn()]};
        
        const container = customRender(<ProfileAndTeam navigation={{ navigate: jest.fn() }} route={{}} />, {providerProps});

        await waitFor(() => {
            const text = container.queryByTestId("team-edit-button");
            expect(text).toBeNull();
        })
    })

    it('should return the column chief edit button text', async () => {
        spyUnitGetMe = jest.spyOn(UnitDao, 'getMe').mockResolvedValue(columnChiefUnit);
        const providerProps = { value: [{unit: columnChiefUnit, intervention: new Intervention(), vehicles: []}, jest.fn()]};
        
        const container = customRender(<ProfileAndTeam navigation={{ navigate: jest.fn() }} route={{}} />, {providerProps});

        await waitFor(() => {
            const text = container.getByTestId("toTeamEdit");
            expect(text.props.children).toBe('Gérer la colonne')
        })
    })

    it('redirects to Edit Team correctly', async () => {
        spyUnitGetMe = jest.spyOn(UnitDao, 'getMe').mockResolvedValue(columnChiefUnit);
        const providerProps = { value: [{unit: columnChiefUnit, intervention: new Intervention(), vehicles: []}, jest.fn()]};
        
        const container = customRender(<ProfileAndTeam route={{}} navigation={{
            navigate: jest.fn()
        }} />, {providerProps});

        await waitFor(() => {
            fireEvent.press(container.getByTestId('toTeamEdit'))
            expect(container.container.props.navigation.navigate.mock.calls[0][0]).toBe('EditTeam')
        })
    })

    it('disconnect and redirect to Login correctly', async () => {
        spyUnitGetMe = jest.spyOn(UnitDao, 'getMe').mockResolvedValue(columnChiefUnit);
        const providerProps = { value: [{unit: columnChiefUnit, intervention: new Intervention(), vehicles: []}, jest.fn()]};
        
        const container = customRender(<ProfileAndTeam route={{}} navigation={{
            navigate: jest.fn()
        }} />, {providerProps});

        await waitFor(() => {
            fireEvent.press(container.getByTestId('userDisconnectBtn'))
            expect(container.container.props.navigation.navigate.mock.calls[0][0]).toBe('Login')
        })
    })

    /*it('should return the right style depending on the index and the length given', () => {
        const profilAndTeam = shallow(<AppProvider>
            <ProfilAndTeam />
        </AppProvider>)
        expect(profilAndTeam.find(ProfilAndTeam).dive().instance().checkLastRow(9, 10)).toMatchObject({ borderBottomWidth : 2 })
        expect(profilAndTeam.find(ProfilAndTeam).dive().instance().checkLastRow(8, 10)).toMatchObject({})
    })*/
})
