import Unit, {UnitRoles} from "~/models/unit.model";
import Intervention from "~/models/intervention.model";
import customRender from "../utils/testHelper";
import ProfileAndTeam from "~/views/ProfilAndTeam/ProfileAndTeam";
import {fireEvent} from "@testing-library/react-native";
import React from "react";
import AgentChiefProfile from "~/views/ProfilAndTeam/AgentChiefProfile";
import UnitDao from "~/models/unit.dao";
import Vehicle from "~/models/vehicle.model";

describe('AgentChiefProfile', () => {
    const agentChiefUnit = new Unit('ID', 'Group-01', 'QSDF', 'CCF', 500, 'ABCD', UnitRoles.AgentChief, [], 1, {serial: 'azer', first_name: 'John', last_name: 'Doe'})
    //const groupChiefUnit = new Unit('ID', 'Group-01', 'QSDF', 'CCF', 500, 'ABCD', UnitRoles.GroupChief, [], 1, {serial: 'azer', first_name: 'John', last_name: 'Doe'})
    //const columnChiefUnit = new Unit('ID', 'Group-01', 'QSDF', 'CCF', 500, 'ABCD', UnitRoles.ColumnChief, [], 1, {serial: 'azer', first_name: 'John', last_name: 'Doe'})

    it('contains all element', () => {
        const providerProps = { value: [{unit: agentChiefUnit, intervention: new Intervention(), vehicles: []}, jest.fn()]};
        const tree = customRender(<AgentChiefProfile unit={agentChiefUnit} />, {providerProps}).toJSON()
        /*
        tree: ContextProvider
        tree -> first_child: ProfileAndTeam Component
         */
        expect(tree.children.length).toBe(2);
    })

    it('renders correctly', () => {
        const providerProps = { value: [{unit: agentChiefUnit, intervention: new Intervention(), vehicles: []}, jest.fn()]};
        const tree = customRender(<AgentChiefProfile unit={agentChiefUnit} />, {providerProps}).toJSON();
        expect(tree).toMatchSnapshot();
    })

    it('should update nb of men correctly', () => {

        UnitDao.update = (unit) => {
            expect(unit.nb_men).toBe(2)

            return Promise.resolve(unit)
        };

        const providerProps = { value: [{unit: agentChiefUnit, intervention: new Intervention(), vehicles: []}, jest.fn()]};
        const container = customRender(<AgentChiefProfile unit={agentChiefUnit} />, {providerProps})

        fireEvent.press(container.getAllByTestId("onAddButton")[0])


        //expect(text.props.children).toBe('Gérer le groupe')
    })
})