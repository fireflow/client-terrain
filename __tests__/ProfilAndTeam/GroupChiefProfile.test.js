import Unit, {UnitRoles} from "~/models/unit.model";
import Intervention from "~/models/intervention.model";
import customRender from "../utils/testHelper";
import ProfileAndTeam from "~/views/ProfilAndTeam/ProfileAndTeam";
import {fireEvent} from "@testing-library/react-native";
import React from "react";
import AgentChiefProfile from "~/views/ProfilAndTeam/AgentChiefProfile";
import UnitDao from "~/models/unit.dao";
import GroupChiefProfile from "~/views/ProfilAndTeam/GroupChiefProfile";

describe('GroupChiefProfile', () => {
    //const agentChiefUnit = new Unit('ID', 'Group-01', 'QSDF', 'CCF', 500, 'ABCD', UnitRoles.AgentChief, [], 1, {serial: 'azer', first_name: 'John', last_name: 'Doe'})
    const groupChiefUnit = new Unit('ID1', 'Group-01', 'aqwz', 'CCF', 500, 'ABCD', UnitRoles.GroupChief, [
        new Unit('ID2', 'Group-02', 'azer', 'CCF', 500, 'ABCD', UnitRoles.AgentChief, [], 1, {serial: 'azer', first_name: 'John', last_name: 'Doe'}),
        new Unit('ID3', 'Group-03', 'qsdf', 'CCF', 500, 'ABCD', UnitRoles.AgentChief, [], 1, {serial: 'qsdf', first_name: 'Jeanne', last_name: 'Marlo'})
    ], 3, {serial: 'aqwz', first_name: 'Hugo', last_name: 'Boichard'})
    //const columnChiefUnit = new Unit('ID', 'Group-01', 'QSDF', 'CCF', 500, 'ABCD', UnitRoles.ColumnChief, [], 1, {serial: 'azer', first_name: 'John', last_name: 'Doe'})

    it('contains all element', () => {
        const providerProps = { value: [{unit: groupChiefUnit, intervention: new Intervention()}, jest.fn()]};
        const tree = customRender(<GroupChiefProfile unit={groupChiefUnit} />, {providerProps}).toJSON()
        /*
        tree: ContextProvider
        tree -> first_child: ProfileAndTeam Component
         */
        expect(tree.children.length).toBe(1);
    })

    it('renders correctly', () => {
        const providerProps = { value: [{unit: groupChiefUnit, intervention: new Intervention()}, jest.fn()]};
        const tree = customRender(<GroupChiefProfile unit={groupChiefUnit} />, {providerProps}).toJSON();
        expect(tree).toMatchSnapshot();
    })

    //FIXME en Debug les rows du tableau sont bien mappé mais impossible à retrouver dans le tree
    it('should map children units correctly', () => {
        const providerProps = { value: [{unit: groupChiefUnit, intervention: new Intervention()}, jest.fn()]};
        const tree = customRender(<GroupChiefProfile unit={groupChiefUnit} />, {providerProps}).toJSON()
        /*
        tree: ContextProvider
        tree -> first_child: ProfileAndTeam Component
         */
        //expect(tree.children[0].children[1].children).toBe(3);
    })
})