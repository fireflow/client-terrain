import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';
import LoginPage from '~/views/Login/Login';
import AuthDao from '~/models/auth.dao';
import customRender from "../utils/testHelper";
import Session from "~/services/Session.service";
import Vehicle from "~/models/vehicle.model";
import NotificationHelper from "~/services/NotificationHelper";
import VehicleDao from "~/models/vehicle.dao";

describe('LoginPage', () => {
    const emptySession = new Session();
    const vehicle = new Vehicle('123');


    afterEach(() => {
        jest.clearAllMocks();
    })

    it('contains all element', () => {
        const tree = render(<LoginPage />).toJSON();
        expect(tree.children.length).toBe(5);
    });

    it('renders correctly', () => {
        const tree = render(<LoginPage />).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('update serial input state correctly', () => {
        const container = render(<LoginPage />);

        const input = container.getByTestId('serialInput');

        fireEvent.changeText(input, 'azer');
        expect(input.props.value).toBe('azer');

        fireEvent.changeText(input, 'azerty');
        expect(input.props.value).toBe('azerty');
    });

    it('update password input state correctly', () => {
        const container = render(<LoginPage />);

        const input = container.getByTestId('passwordInput');

        fireEvent.changeText(input, 'password');
        expect(input.props.value).toBe('password');

        fireEvent.changeText(input, 'passwor');
        expect(input.props.value).toBe('passwor');
    });

    it('submit invalid serial', async () => {
        const spyNotification = jest.spyOn(NotificationHelper, 'Error');

        NotificationHelper.Error = spyNotification.mockImplementation((title, desc) => {
            expect(title).toBe('Connexion Impossible');
            expect(desc).toBe('Aucun identifiant fournit.');
        });

        const container = render(<LoginPage />);

        const serialInput = container.getByTestId('serialInput');
        const passwordInput = container.getByTestId('passwordInput');
        const departmentInput = container.getByTestId('departmentInput');

        fireEvent.changeText(serialInput, '');
        expect(serialInput.props.value).toBe('');

        fireEvent.changeText(passwordInput, 'password');
        expect(passwordInput.props.value).toBe('password');

        fireEvent.changeText(departmentInput, '44');
        expect(departmentInput.props.value).toBe('44');

        fireEvent.press(container.getByTestId('loginBtn'));

        await waitFor(() => expect(spyNotification).toBeCalledTimes(1));
    });

    it('submit invalid password', async () => {
        const spyNotification = jest.spyOn(NotificationHelper, 'Error');

        NotificationHelper.Error = spyNotification.mockImplementation((title, desc) => {
            expect(title).toBe('Connexion Impossible');
            expect(desc).toBe('Aucun mot de passe fournit.');
        });

        const container = render(<LoginPage />);

        const serialInput = container.getByTestId('serialInput');
        const passwordInput = container.getByTestId('passwordInput');
        const departmentInput = container.getByTestId('departmentInput');

        fireEvent.changeText(serialInput, 'serial');
        expect(serialInput.props.value).toBe('serial');

        fireEvent.changeText(passwordInput, '');
        expect(passwordInput.props.value).toBe('');

        fireEvent.changeText(departmentInput, '44');
        expect(departmentInput.props.value).toBe('44');

        fireEvent.press(container.getByTestId('loginBtn'));

        await waitFor(() => expect(spyNotification).toBeCalledTimes(1));

    });

    it('submit invalid department', async () => {
        const spyNotification = jest.spyOn(NotificationHelper, 'Error');

        NotificationHelper.Error = spyNotification.mockImplementation((title, desc) => {
            expect(title).toBe('Connexion Impossible');
            expect(desc).toBe('Aucun département fournit.');
        });

        const container = render(<LoginPage/>);

        const serialInput = container.getByTestId('serialInput');
        const passwordInput = container.getByTestId('passwordInput');
        const departmentInput = container.getByTestId('departmentInput');

        fireEvent.changeText(serialInput, 'serial');
        expect(serialInput.props.value).toBe('serial');

        fireEvent.changeText(passwordInput, 'password');
        expect(passwordInput.props.value).toBe('password');

        fireEvent.changeText(departmentInput, '');
        expect(departmentInput.props.value).toBe('');

        fireEvent.press(container.getByTestId('loginBtn'));

        await waitFor(() => expect(spyNotification).toBeCalledTimes(1));
    });


    it('send vehicle login info correctly and display user Form', async () => {
        const spyLogin = jest.spyOn(AuthDao, 'loginVehicle');
        const spyVehicleGetMe = jest.spyOn(VehicleDao, 'getMe');
        const spyNotification = jest.spyOn(NotificationHelper, 'Success');

        NotificationHelper.Success = spyNotification.mockImplementation((title, desc) => {
            expect(title).toBe('Connexion');
            expect(desc).toBe('Vous êtes connecté au véhicule !');
        });

        AuthDao.loginVehicle = spyLogin.mockImplementation((serial, password, department) => {
            expect(serial).toBe('azer');
            expect(password).toBe(
                '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'
            ); // Equal password
            expect(department).toBe(44)

            return Promise.resolve();
        });

        spyVehicleGetMe.mockResolvedValue(vehicle);

        const providerProps = { value: [{session: emptySession}, jest.fn()]};
        const container = customRender(
            <LoginPage
                navigation={{
                    navigate: jest.fn(),
                }}
            />, {providerProps}
        );

        const serialInput = container.getByTestId('serialInput');
        const passwordInput = container.getByTestId('passwordInput');
        const departmentInput = container.getByTestId('departmentInput');

        fireEvent.changeText(serialInput, 'azer');
        expect(serialInput.props.value).toBe('azer');

        fireEvent.changeText(passwordInput, 'password');
        expect(passwordInput.props.value).toBe('password');

        fireEvent.changeText(departmentInput, '44');
        expect(departmentInput.props.value).toBe('44');

        fireEvent.press(container.getByTestId('loginBtn'));

        await waitFor(() => expect(spyLogin).toBeCalledTimes(1));
        await waitFor(() => expect(spyVehicleGetMe).toBeCalledTimes(1));
        await waitFor(() => expect(spyNotification).toBeCalledTimes(1));


        //expect(container.queryByTestId("departmentInput")).toBeNull();

    });

    it('send user login info correctly and redirect to Home', async () => {
        const spyLogin = jest.spyOn(AuthDao, 'loginUser');

        AuthDao.loginUser = spyLogin.mockImplementation((serial, password) => {
            expect(serial).toBe('azer');
            expect(password).toBe(
                '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'
            ); // Equal password

            return Promise.resolve();
        });

        emptySession.createVehicleSession(vehicle);

        const providerProps = { value: [{session: emptySession}, jest.fn()]};
        const container = customRender(
            <LoginPage
                navigation={{
                    navigate: jest.fn(),
                }}
            />, {providerProps}
        );

        const serialInput = container.getByTestId('serialInput');
        const passwordInput = container.getByTestId('passwordInput');

        fireEvent.changeText(serialInput, 'azer');
        expect(serialInput.props.value).toBe('azer');

        fireEvent.changeText(passwordInput, 'password');
        expect(passwordInput.props.value).toBe('password');

        fireEvent.press(container.getByTestId('loginBtn'));

        await waitFor(() => expect(spyLogin).toBeCalledTimes(1));

    });

    it('disconnect vehicle and display vehicle login form', async () => {

        emptySession.createVehicleSession(vehicle);

        const providerProps = { value: [{session: emptySession}, jest.fn()]};
        const container = customRender(
            <LoginPage
                navigation={{
                    navigate: jest.fn(),
                }}
            />, {providerProps}
        );

        const serialInput = container.getByTestId('serialInput');
        const passwordInput = container.getByTestId('passwordInput');

        fireEvent.changeText(serialInput, 'azer');
        expect(serialInput.props.value).toBe('azer');

        fireEvent.changeText(passwordInput, 'password');
        expect(passwordInput.props.value).toBe('password');

        expect(container.queryByTestId("departmentInput")).toBeNull();

        fireEvent.press(container.getByTestId('disconnectBtn'));
        //manually remove vehicle from session because Context update doesn't work during texting
        emptySession.deleteVehicleSession();

        expect(serialInput.props.value).toBe('');

        expect(passwordInput.props.value).toBe('');

        //expect(container.queryByTestId("departmentInput")).toBeTruthy();
    });
});
