import {fireEvent, render} from "@testing-library/react-native";
import React from "react";
import ImageInfo from "~/components/ImageInfo";

describe('ImageInfo', () => {
    const albumPicture = {
        name: 'name',
        description: 'description',
        src: 'url'
    }
    it('contains all element', () => {
        const tree = render(<ImageInfo isNewPicture={false} picture={albumPicture} onClose={jest.fn()} onValid={jest.fn()}/>).toJSON();
        expect(tree.children.length).toBe(2);
    })

    it('renders correctly', () => {
        const tree = render(<ImageInfo isNewPicture={false} picture={albumPicture} onClose={jest.fn()} onValid={jest.fn()}/>).toJSON();

        expect(tree).toMatchSnapshot();
    })

    it('update fields correctly', () => {
        const container = render(<ImageInfo isNewPicture={true} picture={albumPicture} onClose={jest.fn()} onValid={jest.fn()}/>);

        const nameInput = container.getByTestId('name-input');
        const descInput = container.getByTestId('description-input');

        fireEvent.changeText(nameInput, "image");
        expect(nameInput.props.value).toBe("image");

        fireEvent.changeText(descInput, "une super description");
        expect(descInput.props.value).toBe("une super description");
    })

    it('validate fields correctly', () => {
        const validateFn = jest.fn();
        const container = render(<ImageInfo isNewPicture={true} picture={albumPicture} onClose={jest.fn()} onValid={validateFn}/>);

        fireEvent.press(container.getByTestId("validate-btn"));

        expect(validateFn).toHaveBeenCalled();

    })

    it('cancel correctly', () => {
        const cancelFn = jest.fn();
        const container = render(<ImageInfo isNewPicture={true} picture={albumPicture} onClose={cancelFn} onValid={jest.fn()}/>);

        fireEvent.press(container.getByTestId("cancel-btn"));

        expect(cancelFn).toHaveBeenCalled();

    })
})