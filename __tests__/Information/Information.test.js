import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import Information from '~/views/Information/Information'
import Intervention from "~/models/intervention.model";
import { appContext } from '~/store/context';
import { exp } from 'react-native-reanimated';
import customRender from "../utils/testHelper";

describe('Information', () => {
    const intervention = new Intervention(
        'abcd',
        '0',
        '0',
        'incipient',
        'civil',
        'abcd',
        'Feu au Mans',
        '52 rue joinville, Le Mans 72100',
        'Ceci est la description de l\'intervention au mans.',
        '2022-01-12T07:45:28.41061Z'
    )

    it('contains all element', () => {
        /*const tree = renderer.create(
            <appContext.Provider value={{
                intervention: intervention,
                updateIntervention: () => {}
            }}>
                <Information />
            </appContext.Provider>
        ).toJSON()*/
        const providerProps = { value: [{intervention: intervention}, jest.fn()]};
        const tree = customRender(<Information/>, {providerProps}).toJSON();

        expect(tree).toMatchSnapshot();
    })

    it('contains all element when there is no intervention', () => {
        const providerProps = { value: [{unit: undefined, intervention: undefined}, jest.fn()]};
        const tree = customRender(<Information/>, {providerProps}).toJSON();

        expect(tree.children.length).toBe(2);
        expect(tree.children[1].children[0].children[0]).toBe('Aucun chantier en cours.');
    })

    it('contains date element when there is an intervention', () => {
        const providerProps = { value: [{unit: undefined, intervention: intervention}, jest.fn()]};
        const tree = customRender(<Information/>, {providerProps}).toJSON();

        expect(tree.children.length).toBe(3);
        expect(tree.children[1].children[0].children[0].children[1].children[0].children[1].children[0]).toBe('12 janvier 2022, 08:45');
    })
})