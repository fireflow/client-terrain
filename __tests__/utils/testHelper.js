import {render} from "@testing-library/react-native";
import {AppContext} from "~/store/context";
import {NavigationContext} from "@react-navigation/native";
import React from "react";
import EventService from "~/services/events/EventService";


/**
 * A custom render to setup providers. Extends regular
 * render options with `providerProps` to allow injecting
 * different scenarios to test with.
 *
 * @see https://testing-library.com/docs/react-testing-library/setup#custom-render
 */
const customRender = (ui, {providerProps, ...renderOptions}) => {
    // fake NavigationContext value data
    const navContext = {
        isFocused: () => true,
        // addListener returns an unsubscribe function.
        addListener: jest.fn(() => jest.fn())
    }

    const initialState = {
        orgEventService: new EventService(),
        interventionEventService: new EventService(),
        fireZonePolygon: []
    }

    const completedProps = {value: [{...initialState, ...providerProps.value[0]}, providerProps.value[1]]}

    return render(
        <AppContext.Provider {...completedProps}>
            <NavigationContext.Provider value={navContext}>
                {ui}
            </NavigationContext.Provider>
        </AppContext.Provider>,
        renderOptions,
    )
}

export default customRender;