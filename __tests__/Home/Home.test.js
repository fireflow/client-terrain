import React from 'react';
import Home from "~/views/Home/Home";
import customRender from "../utils/testHelper";
import Intervention from "~/models/intervention.model";
import {fireEvent, waitFor} from "@testing-library/react-native";
import {act} from "react-dom/test-utils";
import {Platform} from "react-native";

describe('Home', () => {

    const providerProps = { value: [{unit: null, intervention: new Intervention()}, jest.fn()]};
    it('contains all element', () => {
        const tree = customRender(<Home route={{}}/>, {providerProps}).toJSON()
        expect(tree.children.length).toBe(2);
    })

    it('renders correctly', () => {
        const tree = customRender(<Home route={{}}/>, {providerProps}).toJSON()
        expect(tree).toMatchSnapshot();
    })

    /*it('display legend Panel correctly', () => {
        const container = customRender(<Home route={{}}/>, {providerProps})

        expect(container.queryByTestId('legendPanelView')).toBeNull()

        fireEvent.press(container.getByTestId('toggleLegendBtn'));

        expect(container.queryByTestId('legendPanelView')).toBeTruthy()
    })*/

    /*it('request Location permission and setup map correctly', async () => {
        const spyFglocation = jest.spyOn(Location, 'requestForegroundPermissionsAsync');
        spyFglocation.mockResolvedValueOnce({status: 'granted'})

        const spyBglocation = jest.spyOn(Location, 'requestBackgroundPermissionsAsync');
        spyBglocation.mockResolvedValueOnce({status: 'granted'})


        const container = customRender(<Home />, {providerProps})
    })*/

    /*it('should display filter list correctly', async () => {
        const container = customRender(<Home route={{}}/>, {providerProps});

        expect(container.queryAllByTestId('filterListItem').length).toBe(0);

        expect(Platform.OS).toBe('android');

        const filterList = container.getByTestId('filterList');
        fireEvent.press(filterList);

        const filters = container.queryAllByTestId('filterListItem');
        expect(filters.length).toBe(3)

        fireEvent.press(filters[0]);

        expect(container.queryAllByTestId('filterListItem').length).toBe(2);
        expect(container.queryAllByTestId('activeFilterListItem').length).toBe(1);

    })*/
})
