import React from 'react';
import renderer from 'react-test-renderer';
import LoginRedirect from '~/views/Login/LoginRedirect';
import { AppProvider } from '~/store/context';
import customRender from '../utils/testHelper';
import User from '~/models/user.model';
import Session from '~/services/Session.service';
import UserDao from '~/models/user.dao';
import UnitDao from '~/models/unit.dao';
import Unit, { UnitRoles } from '~/models/unit.model';
import Intervention from '~/models/intervention.model';
import InterventionDao from '~/models/intervention.dao';
import Vehicle from '~/models/vehicle.model';
import Grade from '~/models/grade.model';
import VehicleDao from '~/models/vehicle.dao';
import GradeDao from '~/models/grade.dao';
import { waitFor } from '@testing-library/react-native';

describe('LoginRedirect', () => {
    const user = new User(
        'ABC',
        'DEF',
        'azer',
        'John',
        'Doe',
        'picture.jpeg',
        '1'
    );
    const unit = new Unit(
        'ID',
        'Group-01',
        'QSDF',
        'VehicleID',
        500,
        'poi',
        UnitRoles.AgentChief,
        [],
        1,
        { serial: 'azer', first_name: 'John', last_name: 'Doe' }
    );
    const vehicle = new Vehicle('vehicleID', 'CCF', 'vulcain01', 'DEF');
    const grades = [new Grade('1', 'DEF', 'Caporal')];
    const intervention = new Intervention('poi');
    const session = new Session();

    let spyUserGetMe;
    let spyUnitGetMe;
    let spyVehicleGet;
    let spyGradeGet;
    let spyInterventionGetAssigned;
    beforeEach(() => {
        spyUserGetMe = jest.spyOn(UserDao, 'getMe').mockResolvedValue(user);

        spyUnitGetMe = jest.spyOn(UnitDao, 'getMe').mockResolvedValue(unit);

        spyVehicleGet = jest
            .spyOn(VehicleDao, 'getMe')
            .mockResolvedValue(vehicle);

        spyGradeGet = jest
            .spyOn(GradeDao, 'getGrades')
            .mockResolvedValue(grades);

        spyInterventionGetAssigned = jest
            .spyOn(InterventionDao, 'getAssigned')
            .mockResolvedValue(intervention);

        session.deleteVehicleSession();
        session.createUserSession();
    });

    afterEach(() => {
        spyUserGetMe.mockClear();
        spyUnitGetMe.mockClear();
        spyVehicleGet.mockClear();
        spyGradeGet.mockClear();
        spyInterventionGetAssigned.mockClear();
        session.deleteUserSession();
    });

    it('contains all element', () => {
        const providerProps = { value: [{ session: undefined }, jest.fn()] };
        const tree = customRender(<LoginRedirect />, {
            providerProps,
        }).toJSON();
        expect(tree).toBe(null);
    });

    it('renders correctly', () => {
        const providerProps = { value: [{ session: undefined }, jest.fn()] };
        const tree = customRender(<LoginRedirect />, {
            providerProps,
        }).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('should redirect to home because already logged', async () => {
        session.createUserSession(user);

        const providerProps = { value: [{ session }, jest.fn()] };
        const container = customRender(
            <LoginRedirect
                navigation={{
                    navigate: jest.fn(),
                }}
            />,
            { providerProps }
        );

        await waitFor(() => expect(spyUserGetMe).not.toHaveBeenCalled());

        expect(
            container.container.props.navigation.navigate.mock.calls[0][0]
        ).toBe('MainPage');
    });

    it('should successful login and redirect to home', async () => {
        const providerProps = { value: [{ session }, jest.fn()] };
        const container = customRender(
            <LoginRedirect
                navigation={{
                    navigate: jest.fn(),
                }}
            />,
            { providerProps }
        );

        await waitFor(() => expect(spyVehicleGet).toHaveBeenCalledTimes(1));
        await waitFor(() => expect(spyUserGetMe).toHaveBeenCalledTimes(1));
        await waitFor(() => expect(spyUnitGetMe).toHaveBeenCalledTimes(1));
        await waitFor(() => expect(spyGradeGet).toHaveBeenCalledTimes(1));
        await waitFor(() => expect(spyInterventionGetAssigned).toHaveBeenCalledTimes(1));

        expect(
            container.container.props.navigation.navigate.mock.calls[0][0]
        ).toBe('MainPage');
    });

    it('should fail login and redirect to login', async () => {
        spyUnitGetMe.mockRejectedValueOnce(new Error());

        const providerProps = { value: [{ session }, jest.fn()] };
        const container = customRender(
            <LoginRedirect
                navigation={{
                    navigate: jest.fn(),
                }}
            />,
            { providerProps }
        );

        await waitFor(() => expect(spyVehicleGet).toHaveBeenCalledTimes(1));
        await waitFor(() => expect(spyUserGetMe).toHaveBeenCalledTimes(1));
        await waitFor(() => expect(spyUnitGetMe).toHaveBeenCalledTimes(1));
        await waitFor(() =>
            expect(spyInterventionGetAssigned).toHaveBeenCalledTimes(0)
        );

        expect(
            container.container.props.navigation.navigate.mock.calls[0][0]
        ).toBe('Login');
    });
});
