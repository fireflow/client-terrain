import customRender from "../utils/testHelper";
import {fireEvent} from "@testing-library/react-native";
import React from "react";
import SelectList from "~/components/SelectList";


const options = [
    {
        name: "camion",
        id: "1"
    },
    {
        name: "voiture",
        id: "2"
    },
    {
        name: "moto",
        id: "3"
    }
]



describe('SelectList', () => {

    it('contains all element', () => {
        const selected = options[0].id;
        const onUpdate = (value) => {
            //selected = value;
        }
        const providerProps = { value: [{}, jest.fn()]};
        const tree = customRender(<SelectList options={options} selected={selected} text={"Vehicules"} onItemSelected={onUpdate}/>, {providerProps}).toJSON();
        expect(tree.children.length).toBe(2);
    })

    it('renders correctly', () => {
        const selected = options[0].id;
        const onUpdate = (value) => {
            //selected = value;
        }
        const providerProps = { value: [{}, jest.fn()]};
        const tree = customRender(<SelectList options={options} selected={selected} text={"Vehicules"} onItemSelected={onUpdate}/>, {providerProps}).toJSON();
        expect(tree).toMatchSnapshot();
    })

    it('should change selected value', () => {
        let selected = options[0].id;
        const onUpdate = (value) => {
            selected = value;
        }
        const providerProps = { value: [{}, jest.fn()]};
        const select = customRender(<SelectList options={options} selected={selected} text={"Vehicules"} onItemSelected={onUpdate}/>, {providerProps});

        //const alertInstance = alert.dive().instance();
        //expect(alert.queryByTestId('confirmModal')).toBeNull()
        expect(selected).toBe('1');
        fireEvent.press(select.getByTestId('option-2'));

        expect(selected).toBe('2');
        //expect(alert.getByTestId('confirmModal')).toBeTruthy()
    })

    /*it('should hide the confirm popup', () => {
        const providerProps = { value: [{intervention: intervention}, jest.fn()]};
        const alert = customRender(<SelectList/>, {providerProps});

        //const alertInstance = alert.dive().instance();
        expect(alert.queryByTestId('confirmModal')).toBeNull()

        fireEvent.press(alert.getByTestId('showConfirmButton'));
        expect(alert.getByTestId('confirmModal')).toBeTruthy();

        fireEvent.press(alert.getByTestId('hideConfirmButton'));
        expect(alert.queryByTestId('confirmModal')).toBeNull()
    })*/
})