import React from 'react';
import Alert from "~/views/Alert/Alert";
import Intervention from "~/models/intervention.model";
import customRender from "../utils/testHelper";
import {fireEvent} from "@testing-library/react-native";

describe('Alert', () => {
    const intervention = new Intervention('abcd')

    it('contains all element', () => {
        const providerProps = { value: [{intervention: intervention}, jest.fn()]};
        const tree = customRender(<Alert/>, {providerProps}).toJSON();
        expect(tree.children.length).toBe(2);
    })

    it('contains all element when there is no intervention', () => {
        const providerProps = { value: [{intervention: undefined}, jest.fn()]};
        const tree = customRender(<Alert/>, {providerProps}).toJSON();
        expect(tree.children.length).toBe(2);
        expect(tree.children[1].children[0].children[0]).toBe('Aucun chantier en cours.');
    })

    it('renders correctly', () => {
        const providerProps = { value: [{intervention: intervention}, jest.fn()]};
        const tree = customRender(<Alert/>, {providerProps}).toJSON();
        expect(tree).toMatchSnapshot();
    })

    it('should show the confirm popup', () => {
        const providerProps = { value: [{intervention: intervention}, jest.fn()]};
        const alert = customRender(<Alert/>, {providerProps});

        //const alertInstance = alert.dive().instance();
        expect(alert.queryByTestId('confirmModal')).toBeNull()
        fireEvent.press(alert.getByTestId('showConfirmButton'));

        expect(alert.getByTestId('confirmModal')).toBeTruthy()
    })

    it('should hide the confirm popup', () => {
        const providerProps = { value: [{intervention: intervention}, jest.fn()]};
        const alert = customRender(<Alert/>, {providerProps});

        //const alertInstance = alert.dive().instance();
        expect(alert.queryByTestId('confirmModal')).toBeNull()

        fireEvent.press(alert.getByTestId('showConfirmButton'));
        expect(alert.getByTestId('confirmModal')).toBeTruthy();

        fireEvent.press(alert.getByTestId('hideConfirmButton'));
        expect(alert.queryByTestId('confirmModal')).toBeNull()
    })
})
