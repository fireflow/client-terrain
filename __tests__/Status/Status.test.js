import React from 'react';
import renderer from 'react-test-renderer';
import Status from "../../src/views/Status/StatusPage";
import {appContext} from "~/store/context";
import Intervention from "~/models/intervention.model";
import customRender from "../utils/testHelper";
import {act, fireEvent, waitFor} from "@testing-library/react-native";
import Unit from "~/models/unit.model";
import {EventStatus} from "~/models/event.model";

describe('Status', () => {
    const intervention = new Intervention('abcd')
    const unit = new Unit().fromJson({id: '123', status: EventStatus.InFirehouse})

    it('contains all element', () => {
        const providerProps = { value: [{intervention: intervention, unit: unit}, jest.fn()]};
        const tree = customRender(<Status/>, {providerProps}).toJSON();
        expect(tree.children.length).toBe(2);
    })

    it('renders correctly', () => {
        const providerProps = { value: [{intervention: intervention, unit: unit}, jest.fn()]};
        const tree = customRender(<Status/>, {providerProps}).toJSON();
        expect(tree).toMatchSnapshot();
    })

    it('contains all element when there is no intervention', () => {
        const providerProps = { value: [{intervention: undefined, unit: unit}, jest.fn()]};
        const tree = customRender(<Status/>, {providerProps}).toJSON();
        expect(tree.children.length).toBe(2);
        expect(tree.children[1].children[0].children[0]).toBe('Aucun chantier en cours.');
    })

    it('contains all element when there is an intervention', () => {
        const providerProps = { value: [{intervention: intervention, unit: unit}, jest.fn()]};
        const tree = customRender(<Status/>, {providerProps}).toJSON();
        expect(tree.children.length).toBe(2);
        expect(tree.children[1].children[0].children[1].children[0]).toBe('En caserne');
    })

    it('contains all element when unit is in Firehouse', () => {
        const providerProps = { value: [{intervention: intervention, unit: unit}, jest.fn()]};
        const tree = customRender(<Status/>, {providerProps});
        expect(tree.toJSON().children.length).toBe(2);

        fireEvent.press(tree.getByTestId('statusInFirehouseButton'))

        expect(tree.toJSON().children[1].children[0].children[1].children[0]).toBe('En caserne');
    })

    it('contains all element when unit is in transit', async () => {
        const providerProps = { value: [{intervention: intervention, unit: unit}, jest.fn()]};
        const tree = customRender(<Status/>, {providerProps});
        expect(tree.toJSON().children.length).toBe(2);


        fireEvent.press(tree.getByTestId('statusTransitButton'));
        await waitFor(() => {
            expect(tree.toJSON().children[1].children[0].children[1].children[0]).toBe('En transit');
        })

    })

    it('contains all element when unit is engaged', () => {
        const providerProps = { value: [{intervention: intervention, unit: unit}, jest.fn()]};
        const tree = customRender(<Status/>, {providerProps});
        expect(tree.toJSON().children.length).toBe(2);

        fireEvent.press(tree.getByTestId('statusEngagedButton'))

        expect(tree.toJSON().children[1].children[0].children[1].children[0]).toBe('Engagé');
    })

    it('contains all element when unit is disengaged', () => {
        const providerProps = { value: [{intervention: intervention, unit: unit}, jest.fn()]};
        const tree = customRender(<Status/>, {providerProps});
        expect(tree.toJSON().children.length).toBe(2);

        fireEvent.press(tree.getByTestId('statusEngagedButton'))
        fireEvent.press(tree.getByTestId('statusDisengagedButton'))

        expect(tree.toJSON().children[1].children[0].children[1].children[0]).toBe('Désengagé');
    })

    it('fail to disengage when unit is not engaged', () => {
        const providerProps = { value: [{intervention: intervention, unit: unit}, jest.fn()]};
        const tree = customRender(<Status/>, {providerProps});
        expect(tree.toJSON().children.length).toBe(2);

        expect(tree.queryByTestId('statusDisengagedButton')).toBeNull()
    })
})
