import {fireEvent, render} from "@testing-library/react-native";
import React from "react";
import ImageInfo from "~/components/ImageInfo";
import ImagePickerButtons from "~/components/ImagePicker";
import * as ImagePicker from "expo-image-picker";

describe('ImagePicker', () => {

    it('contains all element', () => {
        const tree = render(<ImagePickerButtons onImagePick={jest.fn()}/>).toJSON();
        expect(tree.children.length).toBe(3);
    })

    it('renders correctly', () => {
        const tree = render(<ImagePickerButtons onImagePick={jest.fn()}/>).toJSON();

        expect(tree).toMatchSnapshot();
    })

    /*it('open Gallery picture selection', () => {
        const onImagePickFn = jest.fn();
        //const spyImagePicker = jest.spyOn(ImagePicker, 'launchImageLibraryAsync');
        //spyImagePicker.mockResolvedValue({});

        const container = render(<ImagePickerButtons onImagePick={onImagePickFn}/>);

        fireEvent.press(container.getByTestId("gallery-btn"));

        //expect(spyImagePicker).toHaveBeenCalled();
        expect(onImagePickFn).toHaveBeenCalled();

    })*/
})