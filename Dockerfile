FROM node:latest

ENV ADB_IP="192.168.1.1"
ENV REACT_NATIVE_PACKAGER_HOSTNAME="192.255.255.255"

EXPOSE 19000
EXPOSE 19001
EXPOSE 19006

RUN apt-get update && apt-get install -y android-tools-adb

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package.json /usr/src/app/

RUN npm install
RUN npm install expo-cli

COPY . /usr/src/app

EXPOSE 8081
CMD [ "npm", "run", "web" ]
