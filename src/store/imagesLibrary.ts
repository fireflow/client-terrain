import warningMarker from '~/assets/images/markers/warning.png';
import waterSourceMarker from '~/assets/images/markers/waterSource.png';

const images = {
    warningMarker,
    waterSourceMarker,
};

export default images;
