import Session from '~/services/Session.service';
import Intervention, { Coordinate } from '~/models/intervention.model';
import Unit from '~/models/unit.model';
import Grade from '~/models/grade.dao';
import { EventStatus } from '~/models/event.model';
import User from "~/models/user.model";
import Vehicle from "~/models/vehicle.model";
import { LocationObject } from 'expo-location/build/Location.types';

export type ActionsMap = {
    CreateUserSession: User;
    CreateVehicleSession: Vehicle;
    DeleteUserSession: undefined;
    DeleteVehicleSession: undefined;
    SetIntervention: Intervention;
    UnsetIntervention: undefined;
    SetFireZonePolygon: Coordinate[];
    UnsetFireZonePolygon: undefined;
    SetCurrentStatus: EventStatus;
    UnsetCurrentStatus: undefined;
    SetUnit: Unit;
    UnsetUnit: undefined;
    SetCurrentScreen: string;
    SetProfilPicture: string;
    UnsetProfilPicture: undefined;
    SetGrades: Grade[];
    UnsetGrades: undefined;
    SetCurrentLocation: LocationObject;
    UnsetCurrentLocation: undefined;
};

export type Actions = {
    [Key in keyof ActionsMap]: {
        type: Key;
        payload: ActionsMap[Key];
    };
}[keyof ActionsMap];

/* Session Reducer */

export const sessionReducer = (
    state: Session,
    action: Actions
): Session => {
    switch (action.type) {
        case 'CreateUserSession':
            state.createUserSession(action.payload);
            return state;
        case 'CreateVehicleSession':
            state.createVehicleSession(action.payload);
            return state;
        case 'DeleteUserSession':
            state.deleteUserSession();
            return state;
        case 'DeleteVehicleSession':
            state.deleteVehicleSession();
            return state;
        default:
            return state;
    }
};

/* Intervention Reducer */

export const interventionReducer = (
    state: Intervention | undefined,
    action: Actions
): Intervention | undefined => {
    switch (action.type) {
        case 'SetIntervention':
            return action.payload;
        case 'UnsetIntervention':
            return undefined;
        default:
            return state;
    }
};

/* FireZonePolygon Reducer */

export const firezoneReducer = (
    state: Coordinate[] | undefined,
    action: Actions
): Coordinate[] | undefined => {
    switch (action.type) {
        case 'SetFireZonePolygon':
            return action.payload;
        case 'UnsetFireZonePolygon':
            return undefined;
        default:
            return state;
    }
};

/* UserLocation Reducer */

export const userLocationReducer = (
        state: LocationObject | undefined,
        action: Actions
): LocationObject | undefined => {
    switch (action.type) {
        case 'SetCurrentLocation':
            return action.payload;
        case 'UnsetCurrentLocation':
            return undefined;
        default:
            return state;
    }
};

/* Unit Reducer */

export const unitReducer = (
    state: Unit | undefined,
    action: Actions
): Unit | undefined => {
    switch (action.type) {
        case 'SetUnit':
            return action.payload;
        case 'UnsetUnit':
            return undefined;
        case 'SetCurrentStatus':
            if (!!state) {
                state.status = action.payload
            }
            return state;
        default:
            return state;
    }
};

/* Grades Reducer */

export const gradesReducer = (
    state: Grade[] | undefined,
    action: Actions
): Grade[] | undefined => {
    switch (action.type) {
        case 'SetGrades':
            return action.payload;
        case 'UnsetGrades':
            return undefined;
        default:
            return state;
    }
};

/* Current Screen Reducer */

export const currentScreenReducer = (
    state: string | undefined,
    action: Actions
): string | undefined => {
    switch (action.type) {
        case 'SetCurrentScreen':
            return action.payload;
        default:
            return state;
    }
};

/* Profil Picture Reducer */

export const profilPictureReducer = (
    state: string | undefined,
    action: Actions
): string | undefined => {
    switch (action.type) {
        case 'SetProfilPicture':
            return action.payload;
        case 'UnsetProfilPicture':
            return undefined;
        default:
            return state;
    }
};
