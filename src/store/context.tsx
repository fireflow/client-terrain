import React, {
    createContext,
    useCallback,
    useContext,
    useReducer,
} from 'react';
import Intervention, { Coordinate } from '~/models/intervention.model';
import Session from '~/services/Session.service';
import Unit from '~/models/unit.model';
import Grade from '~/models/grade.model';
import EventService from '~/services/events/EventService';
import * as Reducers from '~/store/reducers';
import { LocationObject } from 'expo-location/build/Location.types';

//------------------------------------------

export type InitialStateType = {
    session: Session;
    unit?: Unit;
    grades?: Grade[];
    intervention?: Intervention;
    fireZonePolygon: Coordinate[];
    currentScreen?: string;
    profilePicture?: string;
    orgEventService: EventService;
    interventionEventService: EventService;
    userLocation?: LocationObject;
};

const initialState: InitialStateType = {
    orgEventService: new EventService(),
    interventionEventService: new EventService(),
    session: new Session(),
    unit: undefined,
    profilePicture: undefined,
    grades: undefined,
    intervention: undefined,
    fireZonePolygon: [],
    currentScreen: 'Home',
    userLocation: undefined
};

export type Dispatcher = <
    Type extends Reducers.Actions['type'],
    Payload extends Reducers.ActionsMap[Type]
>(
    type: Type,
    // This line makes it so if there shouldn't be a payload then
    // you only need to call the function with the type, but if
    // there should be a payload then you need the second argument.
    ...payload: Payload extends undefined ? [undefined?] : [Payload]
) => void;

type ContextInterface = readonly [InitialStateType, Dispatcher];

export const AppContext = createContext<ContextInterface>([
    initialState,
    () => {},
]);



const mainReducer = (state: InitialStateType, action: Reducers.Actions) => ({
    session: Reducers.sessionReducer(state.session, action),
    intervention: Reducers.interventionReducer(state.intervention, action),
    fireZonePolygon: Reducers.firezoneReducer(state.fireZonePolygon, action),
    unit: Reducers.unitReducer(state.unit, action),
    profilePicture: Reducers.profilPictureReducer(state.profilePicture, action),
    grades: Reducers.gradesReducer(state.grades, action),
    currentScreen: Reducers.currentScreenReducer(state.currentScreen, action),
    orgEventService: state.orgEventService,
    interventionEventService: state.interventionEventService,
    userLocation: Reducers.userLocationReducer(state.userLocation, action),
});

export const AppProvider: React.FC = ({ children }) => {
    // @ts-ignore
    const [state, _dispatch] = useReducer(mainReducer, initialState);

    const dispatch: Dispatcher = useCallback((type, ...payload) => {
        // @ts-ignore
        _dispatch({ type, payload: payload[0] } as Actions);
    }, []);
    return (
        <AppContext.Provider value={[state, dispatch]}>
            {children}
        </AppContext.Provider>
    );
};

export const ContextConsumer = AppContext.Consumer;
export const ContextConsumerHook = () => useContext(AppContext);
