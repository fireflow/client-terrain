import {Platform, StyleSheet, Text, View} from "react-native";
import MapView, {Polygon} from "react-native-maps";
import React, {useEffect, useState} from 'react';
import {DrawerNavigationHelpers} from "@react-navigation/drawer/lib/typescript/src/types";
import {Button} from "react-native-paper";
import {initial} from "lodash";
import WeatherDao from "~/models/weather.dao";
import WeatherModel from "~/models/weather.model";
import {EventPayload, EventsType, UnitPayload} from "~/services/events/types";
import FireflowNavBar from "~/components/FireflowNavBar";
import {ContextConsumerHook} from "~/store/context";
import uuid from "react-native-uuid";
import {Sector} from "~/models/oct.model";
import InterventionDao from "~/models/intervention.dao";
import ApplicationConf from "~/ApplicationConf";
import {RouteProp} from "@react-navigation/native";
import {ViewParams} from '~/views/ViewParams';
import NotificationHelper from "~/services/NotificationHelper";
import {Coordinate} from "~/models/intervention.model";
import MapWeather from "~/components/MapWeather";
import Cartography from "~/components/Cartography";

export type filterItem = {
    name: string,
    id: string,
    active: boolean
}

let _mapView: MapView | null;

type HomePageProps = {
    navigation:  DrawerNavigationHelpers,
    route: RouteProp<ViewParams, 'Home'>,
}

export default function HomePage(props: HomePageProps) {
    const [context, dispatch] = ContextConsumerHook();
    const [weather, setWeather] = useState(new WeatherModel());
    const [dataLoaded, setDataLoaded] = useState(false);
    const [dataRequested, setDataRequested] = useState(false);
    // const [legendOpen, setLegendOpen] = useState(false);
    const [unitsLocations, setUnitsLocations] = useState<UnitPayload[]>([]);
    //const [showMarkers, setShowMarkers] = useState(true);
    const [editingFire, setEditingFire] = useState(false);
    const [editingSubArea, setEditingSubArea] = useState(false);
    const [fireStatus] = useState(2);
    const [addingToPolygon, setAddingToPolygon] = useState(true);

    const handleUnitLocation = (payload: EventPayload) => {
        const unitPayload = payload as UnitPayload;
        setUnitsLocations([
            ...unitsLocations.filter(u => u.value.unitId !== unitPayload.value.unitId),
            unitPayload
        ])
    }

    useEffect(() => {
        context.orgEventService.registerEventHandler(EventsType.UnitLocation, handleUnitLocation);
    }, []);

    useEffect(() => {
        if (!context.intervention) {
            return
        }
        context.orgEventService.registerEventHandler(EventsType.InterventionAssigned, handleAssignedIntervention);
    }, [context.intervention]);

    const handleAssignedIntervention = () => {
        const orgId = context.session?.user?.org_id
        InterventionDao.getAssigned().then(intervention => {
            if (!!intervention.id) {
                // Update the intervention in app context
                dispatch("SetIntervention", intervention);

                if (orgId != null) {
                    context.interventionEventService.connect(ApplicationConf.endpoint.interventionWebsocket(orgId, intervention.id));
                }
            }
        }).catch(() => dispatch("UnsetIntervention"));
        NotificationHelper.schedulePushNotification({
            content: {
                title: "Intervention Assignée",
                body: "Vous venez d'être assigné à une intervention",
            },
            trigger: {seconds: 1},
        })
    }

    const calculateCentroid = () => {
        if (context.fireZonePolygon.length === 0) {
            return { latitude:0, longitude:0 }
        }
        let pts = context.fireZonePolygon;
        let first = pts[0], last = pts[pts.length-1];
        if (first.latitude != last.latitude || first.longitude != last.longitude)
            pts = [ ...pts, first ];
        let twicearea=0,
            x=0, y=0,
            nPts = pts.length,
            p1, p2, f;
        for ( let i=0, j=nPts-1 ; i<nPts ; j=i++ ) {
            p1 = pts[i]; p2 = pts[j];
            f = p1.latitude*p2.longitude - p2.latitude*p1.longitude;
            twicearea += f;
            x += ( p1.latitude + p2.latitude ) * f;
            y += ( p1.longitude + p2.longitude ) * f;
        }
        f = twicearea * 3;
        return { latitude:x/f, longitude:y/f };
    };

    const getSector = (sector: Sector) => {
        if (!context.intervention?.id) {
            return
        }
        const coordinates = JSON.parse(sector.area!).map((a: number[]) => { return { latitude: a[0], longitude: a[1] }})
        if (coordinates.latitude && coordinates.longitude)
            return (
                <Polygon
                    key={`${uuid.v4()}`}
                    coordinates={coordinates}
                    strokeColor={polygonTypes[fireStatus].stokeColor}
                    fillColor={polygonTypes[fireStatus].fillColor}
                    geodesic={true}
                    strokeWidth={6}
                />
            )
        return null
    }

    const removeFromPolygon = () => {
        if (context.fireZonePolygon.length === 0) {
            return
        }
        const newPolygon = initial(context.fireZonePolygon);
        dispatch("SetFireZonePolygon", newPolygon);
    }

    const getWeather = (coordinates: Coordinate) => {
        setDataLoaded(false);
        setDataRequested(false);

        WeatherDao.getWeatherByCoordinates(coordinates)
            .then(data => {
                setWeather(data);
                setDataLoaded(true);
            })
            .catch(err => console.error(err))
    }

    const setMapView = (mapView: MapView) => _mapView = mapView

    if (Platform.OS === "web")
        return (
            <View style={styles.container}>
                <FireflowNavBar navigation={props.navigation}/>
                <View style={styles.contained}>
                    <Text>Cette page n'est disponible que sur la version tablette !</Text>
                </View>
            </View>
        )
    return (
        <View style={styles.container}>
            <FireflowNavBar navigation={props.navigation}/>
            <View style={styles.contained}>
                <Cartography
                    unitsLocations={unitsLocations}
                    fireStatus={fireStatus}
                    editingFire={editingFire}
                    addingToPolygon={addingToPolygon}
                    getWeather={getWeather}
                    calculateCentroid={calculateCentroid}
                    getSector={getSector}
                    navigation={props.navigation}
                    route={props.route}
                    mapView={_mapView}
                    setMapView={setMapView}
                />
                <View style={{
                    position: 'absolute',
                    ...styles.overlay
                }}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                    }}>
                        {/*<View style={{*/}
                        {/*    width: 80,*/}
                        {/*    height: 80,*/}
                        {/*    backgroundColor: 'rgba(0,0,0,0.6)',*/}
                        {/*    borderRadius: 20,*/}
                        {/*    marginLeft: 10*/}
                        {/*}}>*/}
                        {/*    <IconButton*/}
                        {/*        testID="toggleLegendBtn"*/}
                        {/*        children={false}*/}
                        {/*        icon='comment-question-outline'*/}
                        {/*        size={45}*/}
                        {/*        color='#fff'*/}
                        {/*        onPress={() => setLegendOpen(!legendOpen) }*/}
                        {/*    />*/}
                        {/*</View>*/}
                    </View>
                    <MapWeather
                        dataRequested={dataRequested}
                        dataLoaded={dataLoaded}
                        weather={weather}
                        getWeather={() => getWeather(calculateCentroid())}
                        mapView={_mapView}
                        calculateCentroid={calculateCentroid}
                    />
                </View>
                <View
                    style={{
                        position: 'absolute',
                        bottom: 10,
                        right: 10
                  }}>
                    { !editingFire && !editingSubArea &&
                    <Button
                        style={{ margin: 5 }}
                        icon="pencil"
                        mode="contained"
                        color={"#f64c4c"}
                        onPress={() => setEditingFire(true)}
                    >
                        Edition Secteur
                    </Button>
                    }
                    { (editingFire || editingSubArea) &&
                    <View>
                        <Button
                            style={{ margin: 5 }}
                            icon="pencil"
                            mode="contained"
                            color={"#f64c4c"}
                            onPress={() => setAddingToPolygon(true)}
                        >
                            Ajout
                        </Button>
                        <Button
                            style={{ margin: 5 }}
                            icon="undo"
                            mode="contained"
                            color={"#f64c4c"}
                            onPress={() => removeFromPolygon()}
                        >
                            Annuler l'action
                        </Button>
                        <Button
                            style={{ margin: 5 }}
                            icon="check"
                            mode="contained"
                            color={"#f64c4c"}
                            onPress={() => {
                                setEditingFire(false)
                                setEditingSubArea(false)
                            }}
                        >
                            Terminer
                        </Button>
                    </View>
                    }
                </View>
                {/*{ legendOpen &&*/}
                {/*<View*/}
                {/*    style={{*/}
                {/*        position: 'absolute',*/}
                {/*        ...styles.sub_overlay,*/}
                {/*        alignSelf: "flex-start"*/}
                {/*    }}*/}
                {/*    testID="legendPanelView"*/}
                {/*>*/}
                {/*    <View style={{*/}
                {/*        flex: 0.7,*/}
                {/*        alignItems: 'center',*/}
                {/*        justifyContent: 'space-evenly',*/}
                {/*        width: 250,*/}
                {/*        height: 150,*/}
                {/*        zIndex: 10,*/}
                {/*        backgroundColor: 'rgba(0,0,0,0.6)',*/}
                {/*        borderRadius: 20,*/}
                {/*    }}>*/}
                {/*        <View style={styles.legend_display}>*/}
                {/*            <Image*/}
                {/*                style={{ marginRight: 10, width: 50, height: 50}}*/}
                {/*                source={require('../../assets/images/markers/firetruck.png')}*/}
                {/*            />*/}
                {/*            <Text style={styles.text}>Unité déployée</Text>*/}
                {/*        </View>*/}
                {/*        <View style={styles.legend_display}>*/}
                {/*            <Image*/}
                {/*                style={{ marginRight: 10, width: 50, height: 50 }}*/}
                {/*                source={require('../../assets/images/markers/fire.png')}*/}
                {/*            />*/}
                {/*            <Text style={styles.text}>Présence de feu</Text>*/}
                {/*        </View>*/}
                {/*        <View style={styles.legend_display}>*/}
                {/*            <Image*/}
                {/*                style={{ marginRight: 10, width: 50, height: 50 }}*/}
                {/*                source={require('../../assets/images/markers/waterSource.jpg')}*/}
                {/*            />*/}
                {/*            <Text style={styles.text}>Point d'eau</Text>*/}
                {/*        </View>*/}
                {/*        <View style={styles.legend_display}>*/}
                {/*            <View*/}
                {/*                style={{ width: 50, height: 10, marginRight: 10,  backgroundColor: 'rgb(0,0,0)' }}*/}
                {/*            />*/}
                {/*            <Text style={styles.text}>Feu Sous contrôle</Text>*/}
                {/*        </View>*/}
                {/*        <View style={styles.legend_display}>*/}
                {/*            <View*/}
                {/*                style={{ width: 50, height: 10, marginRight: 10,  backgroundColor: 'rgb(238,255,0)' }}*/}
                {/*            />*/}
                {/*            <Text style={styles.text}>Feu Faible</Text>*/}
                {/*        </View>*/}
                {/*        <View style={styles.legend_display}>*/}
                {/*            <View*/}
                {/*                style={{ width: 50, height: 10, marginRight: 10,  backgroundColor: 'rgb(255,115,0)' }}*/}
                {/*            />*/}
                {/*            <Text style={styles.text}>Feu Fort</Text>*/}
                {/*        </View>*/}
                {/*        <View style={styles.legend_display}>*/}
                {/*            <View*/}
                {/*                style={{ width: 50, height: 10, marginRight: 10,  backgroundColor: 'rgb(255,0,0)' }}*/}
                {/*            />*/}
                {/*            <Text style={styles.text}>Feu Critique</Text>*/}
                {/*        </View>*/}
                {/*    </View>*/}
                {/*</View>*/}
                {/*}*/}
            </View>
        </View>
    )
}


const polygonTypes = [
    {
        stokeColor: 'rgb(0,0,0)',
        fillColor: 'rgba(0,0,0,0.26)',
        message: 'Sous contrôle'
    },
    {
        stokeColor: 'rgb(238,255,0)',
        fillColor: 'rgba(238,255,0,0.26)',
        message: 'Faible'
    },
    {
        stokeColor: 'rgb(255,115,0)',
        fillColor: 'rgba(255,115,0,0.26)',
        message: 'Fort'
    },
    {
        stokeColor: 'rgb(255,0,0)',
        fillColor: 'rgba(255,0,0,0.26)',
        message: 'Critique'
    },
]

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        height: '100%',
    },
    contained: {
        flex: 5.5,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
    },
    overlay: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        padding: 10,
        top: 10,
        width: '100%',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    sub_overlay: {
        flexDirection: 'column',
        justifyContent: 'flex-end',
        flex: 1,
        padding: 10,
        top: 10,
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    legend_display: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#fff'
    },
    iText: {
        fontSize: 20,
        fontStyle: 'italic',
        color: '#fff'
    },
    map: {
        width: '100%',
        height: '100%',
    },
});
