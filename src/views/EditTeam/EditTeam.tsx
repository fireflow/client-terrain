import React, { useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Button } from 'react-native-paper';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { StackNavigationHelpers } from '@react-navigation/stack/lib/typescript/src/types';
import { ScrollView } from 'react-native-gesture-handler';
import FireFlowButton from '../../components/FireflowButton';
import { ContextConsumerHook } from '~/store/context';
import { showMessage } from 'react-native-flash-message';
import Unit, { UnitRoles } from '~/models/unit.model';
import UnitDao from '~/models/unit.dao';
import { useFocusEffect } from '@react-navigation/native';

type EditTeamProps = {
    navigation: StackNavigationHelpers;
};

export default function EditTeam(props: EditTeamProps) {
    const [availableUnits, setAvailableUnits] = useState<Unit[]>([]);
    const [toAdd, setToAdd] = useState<string[]>([]);
    const [toRemove, setToRemove] = useState<string[]>([]);

    const [context, dispatch] = ContextConsumerHook();

    useFocusEffect(
        React.useCallback(() => {
            getAvailableUnits();
        }, [])
    );

    // Functions
    const getAvailableUnits = () => {
        UnitDao.getAvailable(context!.intervention!.id!)
            .then((units: Unit[]) => {
                setAvailableUnits(units);
            })
            .catch((err: Error) => displayError(err.message));
    };

    const getTitle = (role: string) => {
        switch (role) {
            case UnitRoles.GroupChief:
                return 'Gestion du groupe';
            case UnitRoles.ColumnChief:
                return 'Gestion de la colonne';
            default:
                return "Gestion de l'unité";
        }
    };

    const getHeaderAvailableGrid = (role: string) => {
        switch (role) {
            case UnitRoles.GroupChief:
                return 'Unités';
            case UnitRoles.ColumnChief:
                return 'Groupes';
            default:
                return '';
        }
    };

    const getHeaderTeamGrid = (role: string) => {
        switch (role) {
            case UnitRoles.GroupChief:
                return 'Unités du groupe';
            case UnitRoles.ColumnChief:
                return 'Groupes de la colonne';
            default:
                return '';
        }
    };

    const displayError = (err: string) => {
        showMessage({
            message: 'Erreur de communication avec le serveur',
            description: err,
            type: 'danger',
            icon: 'danger',
        });
    };

    // Events
    const onBack = () => {
        props.navigation.goBack();
    };

    const onAdd = () => {
        if (!toAdd.length) return;

        // Add a request to the server when connected with it
        const unitsToAdd = availableUnits.filter((item) =>
            toAdd.includes(item.id!)
        );
        const unitIds = unitsToAdd.map((unit: Unit) => unit.id!);

        // update units on server
        UnitDao.assign(context!.intervention!.id!, unitIds)
            .then(() => {
                // Update the value in the app context
                const unit: Unit = context!.unit!;
                unit!.children = unit!.children!.concat(unitsToAdd);
                dispatch('SetUnit', unit);

                // update component state
                setAvailableUnits(
                    availableUnits.filter((item) => !toAdd.includes(item.id!))
                );
                setToAdd([]);
            })
            .catch((err: Error) => console.log(err.message));
    };

    const onSelectToAdd = (id: string) => {
        if (toAdd.includes(id)) {
            setToAdd((currToAdd) => currToAdd.filter((item) => item !== id));
            return;
        }
        setToAdd((currToAdd) => currToAdd.concat(id));
    };

    const onRemove = () => {
        if (!toRemove.length) return;

        // Add a request to the server when connected with it
        const unitsToRemove = context!.unit!.children!.filter((item: Unit) =>
            toRemove.includes(item.id!)
        );
        const unitIds = unitsToRemove.map((unit: Unit) => unit.id!);

        // update units on server
        UnitDao.assign(context!.intervention!.id!, unitIds)
            .then(() => {
                // Update the value in the app context
                const unit: Unit = context!.unit!;
                unit!.children = unit!.children!.filter(
                    (item) => !toRemove.includes(item.id!)
                );
                dispatch('SetUnit', unit);

                // update component state
                setAvailableUnits((currAvailableUnits) =>
                    currAvailableUnits.concat(unitsToRemove)
                );
                setToRemove([]);
            })
            .catch((err: Error) => displayError(err.message));
    };

    const onSelectToRemove = (id: string) => {
        if (toRemove.includes(id)) {
            setToRemove((currToRemove) =>
                currToRemove.filter((item) => item !== id)
            );
            return;
        }

        setToRemove((currToRemove) => currToRemove.concat(id));
    };

    const openUnitInfo = (unitId: string) => {
        props.navigation.push('ProfileAndTeam', { unitId: unitId });
    };

    return (
        <View style={styles.EditTeam}>
            <View style={{ width: 1000 }}>
                <View style={styles.TitleRow}>
                    <View>
                        <Button
                            children={false}
                            labelStyle={{ fontSize: 100 }}
                            icon="arrow-left"
                            mode="text"
                            color="#FB4B4F"
                            onPress={onBack}
                        />
                    </View>
                    <View>
                        <Text style={[styles.Title]} testID="view-title">
                            {getTitle(context.unit?.role!)}
                        </Text>
                    </View>
                    <View />
                </View>
            </View>

            <View style={[styles.Row, { alignItems: 'center', height: '75%' }]}>
                <View style={{ marginRight: 60, width: 340, height: '100%' }}>
                    <Grid style={{ maxHeight: 75 }}>
                        <Col style={styles.GridCol}>
                            <Row style={styles.GridRow}>
                                <Text
                                    style={styles.GridHeaderText}
                                    testID="header-available-grid"
                                >
                                    {getHeaderAvailableGrid(
                                        context.unit?.role!
                                    )}{' '}
                                    disponibles
                                </Text>
                            </Row>
                        </Col>
                    </Grid>
                    <Grid>
                        <Col style={[styles.GridCol, { borderTopWidth: 0 }]}>
                            <ScrollView>
                                {availableUnits.map((item: Unit) => {
                                    return (
                                        <Row
                                            key={item.id}
                                            style={styles.GridRow}
                                        >
                                            <Text
                                                testID="availableUnit"
                                                onLongPress={() =>
                                                    openUnitInfo(item.id!)
                                                }
                                                onPress={() => {
                                                    onSelectToAdd(item.id!);
                                                }}
                                                style={
                                                    toAdd.includes(item.id!)
                                                        ? styles.GridTextSelected
                                                        : styles.GridText
                                                }
                                            >
                                                {item.name}
                                            </Text>
                                        </Row>
                                    );
                                })}
                            </ScrollView>
                        </Col>
                    </Grid>
                </View>

                <View>
                    <FireFlowButton
                        testID="addBtn"
                        text="Ajouter"
                        width={259}
                        height={70}
                        onPress={onAdd}
                    />
                    <View style={{ marginTop: 15 }}>
                        <FireFlowButton
                            testID="removeBtn"
                            text="Retirer"
                            width={259}
                            height={70}
                            onPress={onRemove}
                        />
                    </View>
                </View>

                <View style={{ marginLeft: 60, width: 340, height: '100%' }}>
                    <Grid style={{ maxHeight: 75 }}>
                        <Col style={styles.GridCol}>
                            <Row style={styles.GridRow}>
                                <Text
                                    style={styles.GridHeaderText}
                                    testID="header-team-grid"
                                >
                                    {getHeaderTeamGrid(context.unit?.role!)}
                                </Text>
                            </Row>
                        </Col>
                    </Grid>
                    <Grid>
                        <Col style={[styles.GridCol, { borderTopWidth: 0 }]}>
                            <ScrollView>
                                {context?.unit?.children?.map((item: Unit) => {
                                    return (
                                        <Row
                                            key={item.id}
                                            style={styles.GridRow}
                                        >
                                            <Text
                                                onLongPress={() =>
                                                    openUnitInfo(item.id!)
                                                }
                                                testID="assignedUnit"
                                                onPress={() => {
                                                    onSelectToRemove(item.id!);
                                                }}
                                                style={
                                                    toRemove.includes(item.id!)
                                                        ? styles.GridTextSelected
                                                        : styles.GridText
                                                }
                                            >
                                                {item.name}
                                            </Text>
                                        </Row>
                                    );
                                })}
                            </ScrollView>
                        </Col>
                    </Grid>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    EditTeam: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    Row: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    Title: {
        fontSize: 40,
        fontFamily: 'Poppins_600SemiBold',
        textAlign: 'center',
    },
    TitleRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        alignSelf: 'stretch',
    },
    GridRow: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
    },
    GridCol: {
        borderWidth: 2,
    },
    GridHeaderText: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 25,
    },
    GridText: {
        fontFamily: 'Poppins_400Regular',
        fontSize: 25,
        color: '#000000',
    },
    GridTextSelected: {
        fontFamily: 'Poppins_600SemiBold',
        color: '#FB4B4F',
        fontSize: 25,
    },
});
