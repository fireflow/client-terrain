import React, { useEffect, useState } from 'react';
import { Button } from 'react-native-paper';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Dimensions,
} from 'react-native';
import { DrawerNavigationHelpers } from '@react-navigation/drawer/lib/typescript/src/types';
import { CustomRouteProp } from '~/models/route.model';
import { EventsType } from '~/services/events/types';
import { ContextConsumerHook } from '~/store/context';
import FireFlowButton from '../../components/FireflowButton';
import FireflowNavBar from '../../components/FireflowNavBar';

type AlertProps = {
    navigation: DrawerNavigationHelpers;
    route: CustomRouteProp<any>;
};

const { width } = Dimensions.get('window');

export default function AlertPage(props: AlertProps) {
    const [context] = ContextConsumerHook();
    const [isConfirmVisible, setIsConfirmVisible] = useState(false);
    const [isAlertSend, setIsAlertSend] = useState(false);

    useEffect(() => {
        if (!context.intervention) return;
        // connect in case of disconnection
        context.interventionEventService.connect();
    }, []);

    // Events
    const showConfirm = () => {
        setIsConfirmVisible(true);
    };

    const hideConfirm = () => {
        setIsConfirmVisible(false);
    };

    const onConfirmPressed = () => {
        context.interventionEventService.sendMessage(EventsType.Alert, {
            message: ``,
        });
        setIsConfirmVisible(false);
        setIsAlertSend(true);
    };

    return (
        <View style={styles.Container}>
            <FireflowNavBar navigation={props.navigation} />
            {context.intervention ? (
                <View style={styles.Contained}>
                    <Text style={[styles.Text, { color: '#000000' }]}>
                        Une urgence est une notification envoyée à toutes les
                        tablettes connectées à Fireflow présentes dans
                        l’intervention. La notification envoyée permet aux
                        autres unités de vous localiser.
                    </Text>
                    <Button
                        testID="showConfirmButton"
                        mode="contained"
                        onPress={showConfirm}
                        style={[
                            styles.AlertButton,
                            {
                                backgroundColor: isAlertSend
                                    ? '#ff745d'
                                    : '#f2f2f2',
                            },
                        ]}
                        disabled={isAlertSend}
                        labelStyle={{
                            fontFamily: 'Poppins_600SemiBold',
                            textTransform: 'none',
                            fontSize: 66,
                            color: isAlertSend ? '#f2f2f2' : '#ff745d',
                        }}
                    >
                        {isAlertSend ? 'Urgence envoyée' : 'Urgence'}
                    </Button>
                </View>
            ) : (
                <View style={styles.Contained}>
                    <Text style={styles.title}>Aucun chantier en cours.</Text>
                </View>
            )}
            {isConfirmVisible && (
                <TouchableOpacity
                    testID="confirmModal"
                    style={styles.ConfirmBackground}
                    onPress={hideConfirm}
                >
                    <TouchableWithoutFeedback>
                        <View style={styles.Confirm}>
                            <Text style={[styles.Text, { marginBottom: 20 }]}>
                                Êtes-vous sûr de vouloir annoncer une urgence ?
                            </Text>
                            <View style={styles.Row}>
                                <View style={{ marginRight: 20 }}>
                                    <FireFlowButton
                                        testID="hideConfirmButton"
                                        text={'Annuler'}
                                        width={259}
                                        height={70}
                                        onPress={hideConfirm}
                                    />
                                </View>
                                <FireFlowButton
                                    text={'Envoyer'}
                                    width={259}
                                    height={70}
                                    color={'#54b854'}
                                    onPress={onConfirmPressed}
                                />
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </TouchableOpacity>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    Container: {
        flex: 1,
        flexDirection: 'row',
        height: '100%',
    },
    Contained: {
        flex: 5.5,
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        zIndex: 0,
    },
    Row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    Text: {
        fontSize: 20,
        fontFamily: 'Poppins_600SemiBold',
        textAlign: 'center',
        width: '75%',
    },
    AlertButton: {
        borderWidth: 4,
        borderRadius: 30,
        borderColor: '#ff745d',
        marginTop: 40,
        width: '60%',
    },
    ConfirmBackground: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        zIndex: 1,
    },
    Confirm: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        elevation: 5,
        borderRadius: 7,
        transform: [{ translateX: -(width * 0.2) }, { translateY: -90 }],
        borderWidth: 0,
        padding: 30,
        zIndex: 1,
    },
    title: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 45,
    },
});
