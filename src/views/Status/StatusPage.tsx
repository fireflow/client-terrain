import {StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Button} from 'react-native-paper';
import FireflowNavBar from '../../components/FireflowNavBar';
import {DrawerNavigationHelpers} from '@react-navigation/drawer/lib/typescript/src/types';
import {CustomRouteProp} from '~/models/route.model';
import {ContextConsumerHook} from '~/store/context';
import {EventsType} from '~/services/events/types';
import {EventStatus, EventStatusFormatted} from '~/models/event.model';

const colors = {
    green: '#54b854',
    yellow: '#e6cb20',
    orange: '#ffa45a',
    red: '#ff745d',
};

type StatusProps = {
    navigation: DrawerNavigationHelpers;
    route: CustomRouteProp<any>;
};

export default function StatusPage(props: StatusProps) {
    const [context, dispatch] = ContextConsumerHook();
    const [currentStatus, setCurrentStatus] = useState<EventStatus>(context.unit?.status || EventStatus.InFirehouse);

    useEffect(() => {
        if (!context.intervention) return;
        // connect in case of disconnection
        context.interventionEventService.connect();
        dispatch('SetCurrentStatus', context.unit?.status || EventStatus.InFirehouse);
    }, []);

    const onStatusPressed = (status: EventStatus) => {
        if (
            context.unit?.status &&
            status !== context.unit.status
        ) {
            dispatch('SetCurrentStatus', status);
            setCurrentStatus(status);
            context.interventionEventService.sendMessage(EventsType.Status, {
                status: status,
                unitId: context.unit?.id!,
            });
        }
    };

    const status = EventStatusFormatted[currentStatus]
    return (
        <View style={styles.container}>
            <FireflowNavBar navigation={props.navigation} />
            {context.intervention ? (
                <View style={styles.contained}>
                    <View style={{ alignItems: 'center' }}>
                        <Text
                            style={{
                                fontSize: 30,
                                fontFamily: 'Poppins_700Bold',
                            }}
                        >
                            Statut actuel:
                        </Text>
                        <Text
                            testID="statusText"
                            style={{
                                color: status.color,
                                fontSize: 80,
                                fontFamily: 'Poppins_700Bold',
                            }}
                        >
                            {status.label}
                        </Text>
                    </View>
                    <Button
                        testID="statusInFirehouseButton"
                        labelStyle={styles.label}
                        mode="contained"
                        uppercase={false}
                        color={colors.green}
                        style={styles.button}
                        onPress={() => onStatusPressed(EventStatus.InFirehouse)}
                    >
                        En caserne
                    </Button>
                    <Button
                        testID="statusTransitButton"
                        labelStyle={styles.label}
                        mode="contained"
                        uppercase={false}
                        color={colors.yellow}
                        style={styles.button}
                        onPress={() => onStatusPressed(EventStatus.Transit)}
                    >
                        En transit
                    </Button>
                    {currentStatus == EventStatus.Engaged ? (
                        <Button
                            testID="statusDisengagedButton"
                            labelStyle={styles.label}
                            mode="contained"
                            uppercase={false}
                            color={colors.red}
                            style={styles.button}
                            onPress={() =>
                                onStatusPressed(EventStatus.Disengaged)
                            }
                        >
                            Désengagé
                        </Button>
                    ) : (
                        <Button
                            testID="statusEngagedButton"
                            labelStyle={styles.label}
                            mode="contained"
                            uppercase={false}
                            color={colors.orange}
                            style={styles.button}
                            onPress={() => onStatusPressed(EventStatus.Engaged)}
                        >
                            Engagé
                        </Button>
                    )}
                </View>
            ) : (
                <View style={styles.contained}>
                    <Text style={styles.title}>Aucun chantier en cours.</Text>
                </View>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        height: '100%',
    },
    contained: {
        flex: 5.5,
        backgroundColor: '#f0f0f0',
        width: '70%',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        height: '100%',
    },
    label: {
        fontSize: 66,
        fontFamily: 'Poppins_700Bold',
        color: '#fff',
    },
    button: {
        borderRadius: 30,
        width: '40%',
    },
    title: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 45,
    },
});
