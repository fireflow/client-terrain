import 'moment/min/locales';
import Moment from 'moment';
import React, { useRef, useState } from 'react';
import { StyleSheet, Text, ScrollView, View, SafeAreaView } from 'react-native';
import { DrawerNavigationHelpers } from '@react-navigation/drawer/lib/typescript/src/types';
import { CustomRouteProp } from '~/models/route.model';
import { ContextConsumerHook } from '~/store/context';
import { useFocusEffect } from '@react-navigation/native';
import { AnimatedFAB } from 'react-native-paper';
import * as ImagePicker from 'expo-image-picker';
import FireflowNavBar from '~/components/FireflowNavBar';
import ImagePickerButtons from '~/components/ImagePicker';
import ImageInfo from '~/components/ImageInfo';
import Album, { AlbumRef, AlbumPicture } from '~/components/Album';
import Modal, { ModalRef } from '~/components/Modal';
import NotificationHelper from '~/services/NotificationHelper';
import MediaDao from '~/models/media.dao';
import Media from '~/models/media.model';
import Gallery from 'react-native-image-gallery';
import { Button } from 'react-native-paper';

const statusColors = {
    growth: '#FF0000',
    incipient: '#ffa45a',
    'fully developed': '#ffa45a',
    decay: '#ffa45a',
    extinct: '#54b854',
};

const statusTranslations = {
    growth: 'Propagation libre',
    incipient: 'Départ du feu',
    'fully developed': 'Feu contrôlé',
    decay: 'Surveillance du feu',
    extinct: 'Éteint',
};

type InformationProps = {
    navigation: DrawerNavigationHelpers;
    route: CustomRouteProp<any>;
};

export default function Information(props: InformationProps) {
    const [context] = ContextConsumerHook();
    const albumRef = useRef<AlbumRef>(null);
    const modalAddImage = useRef<ModalRef>(null);
    const modalImageInfo = useRef<ModalRef>(null);
    const [isNewPicture, setIsNewPicture] = useState<boolean>(false);
    const [picture, setPicture] = useState<AlbumPicture>({
        name: '',
        description: '',
        src: '',
    });
    const [pictures, setPictures] = useState<AlbumPicture[]>([]);
    const [pictureIndex, setPictureIndex] = useState<number>(-1);

    useFocusEffect(
        React.useCallback(() => {
            MediaDao.getInterventionPictures(context.intervention?.id!).then(
                (interventionPictures: Media[]) => {
                    setPictures(
                        interventionPictures.map((picture: Media) => {
                            return {
                                name: picture.name!,
                                description: picture.description!,
                                src: picture.link!,
                            };
                        })
                    );
                }
            );
        }, [])
    );

    const renderDate = (date: Date, format: string): string => {
        Moment.locale('fr');
        return Moment(date).format(format);
    };

    // Functions
    const addImage = (result: ImagePicker.ImagePickerResult) => {
        if (result.cancelled) return;

        setPicture({
            name: '',
            description: '',
            src: result.uri,
        });
        setIsNewPicture(true);
        modalImageInfo.current?.openSelect();
        modalAddImage.current?.closeSelect();
    };

    // Events
    const onAddImage = () => {
        modalAddImage.current?.openSelect();
    };

    const onValidImage = (image: AlbumPicture) => {
        if (!image.name)
            return NotificationHelper.Error(
                "Echec de l'ajout de la photo",
                "Veuillez ajouter un nom à l'image."
            );
        else if (image.name.length < 3)
            return NotificationHelper.Error(
                "Echec de l'ajout de la photo",
                "Le nom de l'image doit avoir au minimum 3 charactères."
            );

        MediaDao.uploadInterventionPicture(context.intervention?.id!, image, context.userLocation!)
            .then(() => {
                setPictures([
                    ...pictures,
                    {
                        name: image.name,
                        description: image.description,
                        src: image.src,
                    },
                ]);
                NotificationHelper.Success(
                    "Mise à jour de l'album",
                    "La photo a bien été ajoutée à l'album de l'intervention."
                );
            })
            .catch((err: Error) =>
                NotificationHelper.Error(
                    "Echec de l'ajout de la photo",
                    err.message
                )
            );
        modalImageInfo.current?.closeSelect();
    };

    const onInspectImage = (i: number) => setPictureIndex(i);

    const onImageInfo = () => {
        setPicture(pictures[pictureIndex]);
        setIsNewPicture(false);
        modalImageInfo.current?.openSelect();
    };

    return (
        <View style={styles.container}>
            {pictureIndex == -1 && (
                <FireflowNavBar navigation={props.navigation} />
            )}
            {context.intervention ? (
                <>
                    <SafeAreaView style={styles.contained}>
                        <ScrollView
                            scrollEnabled={pictureIndex == -1}
                            contentContainerStyle={{
                                alignItems: 'center',
                                justifyContent: 'space-evenly',
                            }}
                        >
                            <Text style={[styles.title, { marginTop: 50 }]}>
                                Informations sur le chantier actuel
                            </Text>
                            <View style={styles.informations}>
                                <View style={styles.column}>
                                    <View style={styles.row}>
                                        <Text
                                            style={[
                                                styles.text,
                                                {
                                                    fontFamily:
                                                        'Poppins_600SemiBold',
                                                    fontSize: 40,
                                                },
                                            ]}
                                        >
                                            {context.intervention.name}
                                        </Text>
                                        <View>
                                            <Text
                                                style={[
                                                    styles.status,
                                                    {
                                                        backgroundColor: (
                                                            statusColors as any
                                                        )[
                                                            context.intervention
                                                                .status!
                                                        ],
                                                    },
                                                ]}
                                            >
                                                {
                                                    (statusTranslations as any)[
                                                        context.intervention
                                                            .status!
                                                    ]
                                                }
                                            </Text>
                                        </View>
                                    </View>
                                    <Text
                                        style={[
                                            styles.text,
                                            {
                                                fontSize: 25,
                                                fontStyle: 'italic',
                                                color: '#505450',
                                            },
                                        ]}
                                    >
                                        {renderDate(
                                            context.intervention.creation_date!,
                                            'DD MMMM YYYY, HH:mm'
                                        )}
                                    </Text>
                                </View>
                                <View style={{ maxWidth: 850, marginTop: 35 }}>
                                    <Text style={styles.text}>
                                        {context.intervention.description}
                                    </Text>
                                </View>
                                <Text
                                    style={[
                                        styles.text,
                                        {
                                            fontFamily: 'Poppins_600SemiBold',
                                            fontSize: 40,
                                            marginTop: 150,
                                        },
                                    ]}
                                >
                                    Album de l'intervention
                                </Text>
                            </View>
                            <Album
                                ref={albumRef}
                                pictures={pictures}
                                onInspectImage={onInspectImage}
                            />
                        </ScrollView>
                    </SafeAreaView>

                    {pictureIndex == -1 && (
                        <AnimatedFAB
                            uppercase={false}
                            theme={{
                                fonts: {
                                    medium: {
                                        fontFamily: 'Poppins_600SemiBold',
                                    },
                                },
                            }}
                            icon={'plus'}
                            label={'Ajouter une image'}
                            animateFrom={'right'}
                            iconMode={'static'}
                            color="white"
                            onPress={onAddImage}
                            extended={true}
                            style={styles.AddPictureButton}
                        />
                    )}
                </>
            ) : (
                <View
                    style={[
                        styles.contained,
                        { justifyContent: 'center', alignItems: 'center' },
                    ]}
                >
                    <Text style={styles.title}>Aucun chantier en cours.</Text>
                </View>
            )}

            {pictureIndex != -1 && (
                <>
                    <Gallery
                        style={{
                            position: 'absolute',
                            width: '100%',
                            height: '100%',
                            backgroundColor: 'black',
                        }}
                        images={pictures.map((picture) => {
                            return { source: { uri: picture.src } };
                        })}
                        initialPage={pictureIndex}
                    />
                    <View
                        style={{
                            flexDirection: 'row',
                            position: 'absolute',
                            top: 16,
                            left: 16,
                        }}
                    >
                        <Button
                            children={false}
                            labelStyle={{ fontSize: 50 }}
                            icon="close"
                            mode="text"
                            color="#FB4B4F"
                            onPress={() => {
                                setPictureIndex(-1);
                            }}
                        />
                        <Button
                            children={false}
                            labelStyle={{ fontSize: 50 }}
                            icon="information"
                            mode="text"
                            color="#FB4B4F"
                            onPress={onImageInfo}
                        />
                    </View>
                </>
            )}

            <Modal ref={modalAddImage}>
                <ImagePickerButtons onImagePick={addImage} />
            </Modal>

            <Modal ref={modalImageInfo}>
                <ImageInfo
                    picture={picture}
                    isNewPicture={isNewPicture}
                    onClose={() => modalImageInfo.current?.closeSelect()}
                    onValid={onValidImage}
                />
            </Modal>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    contained: {
        flex: 5.5,
        backgroundColor: '#f0f0f0',
        width: '70%',
    },
    informations: {
        width: '80%',
        marginTop: 50,
    },
    title: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 45,
    },
    text: {
        fontFamily: 'Poppins_400Regular',
        fontSize: 25,
    },
    status: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 25,
        color: 'white',
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 10,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    column: {
        flexDirection: 'column',
    },
    AddPictureButton: {
        bottom: 16,
        right: 16,
        position: 'absolute',
        backgroundColor: '#FB4B4F',
    },
});
