import { Dispatcher, InitialStateType } from '~/store/context';
import UserDao from '~/models/user.dao';
import UnitDao from '~/models/unit.dao';
import Unit from '~/models/unit.model';
import ApplicationConf from '~/ApplicationConf';
import InterventionDao from '~/models/intervention.dao';
import VehicleDao from '~/models/vehicle.dao';
import GradeDao from '~/models/grade.dao';

export const authenticateCheckRedirection = (
    context: InitialStateType,
    dispatch: Dispatcher
): Promise<void> => {
    return new Promise((resolve, reject) => {
        if (context.session?.isVehicleAuth() && context.session?.isUserAuth()) {
            resolve();
        } else {
            VehicleDao.getMe()
                .then((vehicle) => {
                    context.session?.createVehicleSession(vehicle);

                    UserDao.getMe()
                        .then((user) => {
                            context.session?.createUserSession(user);
                            dispatch('SetProfilPicture', user.profile_picture!);

                            UnitDao.getMe()
                                .then(async (unit: Unit) => {
                                    // Update the unit in app context
                                    dispatch('SetUnit', unit);

                                    if (!!user.org_id) {
                                        context.orgEventService.userId =
                                            context.session?.user?.id!;
                                        context.interventionEventService.userId =
                                            context.session?.user?.id!;

                                        context.orgEventService.connect(
                                            ApplicationConf.endpoint.orgWebsocket(
                                                user.org_id
                                            )
                                        );

                                        const grades = await GradeDao.getGrades(
                                            user.org_id
                                        );
                                        dispatch('SetGrades', grades);

                                        InterventionDao.getAssigned()
                                            .then((intervention) => {
                                                if (!!intervention.id) {
                                                    // Update the intervention in app context
                                                    dispatch(
                                                        'SetIntervention',
                                                        intervention
                                                    );

                                                    if (user.org_id != null) {
                                                        context.interventionEventService.connect(
                                                            ApplicationConf.endpoint.interventionWebsocket(
                                                                user.org_id,
                                                                intervention.id
                                                            )
                                                        );
                                                    }
                                                }
                                            })
                                            .catch((err: Error) =>
                                                dispatch('UnsetIntervention')
                                            );
                                    }

                                    // Reset the current screen in app context
                                    dispatch('SetCurrentScreen', 'Home');
                                    resolve();
                                })
                                .catch((err: Error) => reject(err));
                        })
                        .catch((err: Error) => reject(err));
                })
                .catch((err: Error) => reject(err));
        }
    });
};
