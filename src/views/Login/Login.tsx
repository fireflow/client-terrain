import {
    StyleSheet,
    Text,
    View,
    TextInput,
    KeyboardAvoidingView,
} from 'react-native';
import React, { useState } from 'react';
import { Button } from 'react-native-paper';
import { Image } from 'react-native';
import { StackNavigationHelpers } from '@react-navigation/stack/lib/typescript/src/types';
import { ContextConsumerHook } from '~/store/context';
import { CustomRouteProp } from '~/models/route.model';
import { authenticateCheckRedirection } from '~/views/Login/utils/authUtils';
import NotificationHelper from '~/services/NotificationHelper';
import AuthDao from '~/models/auth.dao';
import VehicleDao from '~/models/vehicle.dao';
import { JSHash, CONSTANTS } from 'react-native-hash';
import AsyncStorage from '@react-native-async-storage/async-storage';

type LoginProps = {
    navigation: StackNavigationHelpers;
    route: CustomRouteProp<any>;
};

export default function LoginPage(props: LoginProps) {
    const [context, dispatch] = ContextConsumerHook();
    const [department, setDepartment] = useState('');
    const [serial, setSerial] = useState('');
    const [password, setPassword] = useState('');

    const onLoginVehicle = () => {
        if (department === '')
            return NotificationHelper.Error(
                'Connexion Impossible',
                'Aucun département fournit.'
            );
        if (serial === '')
            return NotificationHelper.Error(
                'Connexion Impossible',
                'Aucun identifiant fournit.'
            );
        if (password === '')
            return NotificationHelper.Error(
                'Connexion Impossible',
                'Aucun mot de passe fournit.'
            );

        JSHash(password, CONSTANTS.HashAlgorithms.sha256)
            .then((hash) => {
                AuthDao.loginVehicle(serial, hash, parseInt(department))
                    .then(() => {
                        VehicleDao.getMe().then((vehicle) => {
                            dispatch('CreateVehicleSession', vehicle);
                            setSerial('');
                            setPassword('');
                            NotificationHelper.Success(
                                'Connexion',
                                'Vous êtes connecté au véhicule !'
                            );
                        });
                    })
                    .catch((err) => {
                        NotificationHelper.Error(
                            'Connexion Impossible',
                            getVehicleError(err)
                        );
                    });
            })
            .catch((e) => console.log(e));
    };

    const onLoginUser = () => {
        if (serial === '')
            return NotificationHelper.Error(
                'Connexion Impossible',
                'Aucun identifiant fournit.'
            );
        if (password === '')
            return NotificationHelper.Error(
                'Connexion Impossible',
                'Aucun mot de passe fournit.'
            );

        JSHash(password, CONSTANTS.HashAlgorithms.sha256)
            .then((hash) => {
                AuthDao.loginUser(serial, hash)
                    .then(() => {
                        authenticateCheckRedirection(context, dispatch)
                            .then(() => {
                                NotificationHelper.Success(
                                    'Connexion',
                                    'Vous êtes connecté à Fireflow !'
                                );
                                props.navigation.navigate('MainPage');
                            })
                            .catch((err: Error) =>
                                NotificationHelper.Error(
                                    'Connexion Impossible',
                                    err.message
                                )
                            );
                    })
                    .catch((err) => {
                        NotificationHelper.Error(
                            'Connexion Impossible',
                            getLoginUserError(err)
                        );
                    });
            })
            .catch((e) => console.log(e));
    };

    const onDisconnectVehicle = () => {
        NotificationHelper.Success(
            'Déconnexion',
            'Le véhicule à bien été déconnecté.'
        );
        dispatch('DeleteVehicleSession');
        AsyncStorage.clear();
        setDepartment('');
        setSerial('');
        setPassword('');
    };

    const getLoginUserError = (err: any): string => {
        switch (err.code) {
            case 5:
                return 'Identifiant inconnu.';
            case 6:
                return 'Mot de passe incorrect.';
            default:
                return err.message;
        }
    };

    const getVehicleError = (err: any): string => {
        switch (err.code) {
            case 4:
                return 'Département inconnu.';
            case 5:
                return 'Identifiant inconnu.';
            case 6:
                return 'Mot de passe incorrect.';
            default:
                return err.message;
        }
    };

    return (
        <View style={[styles.container, { overflow: 'hidden' }]}>
            <View style={[styles.circle, { top: -160, left: 0 }]} />
            <View style={[styles.circle, { top: -50, left: -160 }]} />
            <View style={[styles.circle, { bottom: -160, right: 0 }]} />
            <View style={[styles.circle, { bottom: -50, right: -160 }]} />
            {context.session?.isVehicleAuth() && (
                <View
                    style={{
                        position: 'absolute',
                        zIndex: 1,
                        top: 30,
                        right: 40,
                        alignItems: 'flex-end',
                    }}
                >
                    <Button
                        testID="disconnectBtn"
                        mode="contained"
                        icon="exit-to-app"
                        contentStyle={{ backgroundColor: '#FB4B4F' }}
                        labelStyle={{
                            fontFamily: 'Poppins_600SemiBold',
                            fontSize: 16,
                            textTransform: 'none',
                        }}
                        onPress={() => onDisconnectVehicle()}
                    >
                        Déconnecter le véhicule
                    </Button>
                    <Text style={styles.vehicleLabel}>
                        {`${context.session?.vehicle?.name} / ${context.session?.vehicle?.serial}`}
                    </Text>
                </View>
            )}
            <View style={styles.contained}>
                <Image
                    style={styles.logo}
                    source={require('~/assets/fireflow-logo.png')}
                />
                <Text
                    style={{
                        fontSize: 20,
                        fontFamily: 'Poppins_600SemiBold',
                        marginTop: -50,
                    }}
                >
                    {context.session?.isVehicleAuth()
                        ? "Connectez-vous pour accéder à l'application"
                        : "Associez un véhicule pour accéder à l'application"}
                </Text>
                <KeyboardAvoidingView
                    behavior="padding"
                    style={{ width: '25%' }}
                >
                    {!context.session?.isVehicleAuth() && (
                        <TextInput
                            testID="departmentInput"
                            style={[styles.textInput, { marginTop: 20 }]}
                            textAlignVertical={'center'}
                            placeholder="Département du véhicule"
                            value={department}
                            keyboardType={'number-pad'}
                            onChangeText={(newDepartment) =>
                                setDepartment(newDepartment)
                            }
                        />
                    )}
                    <TextInput
                        testID="serialInput"
                        style={[styles.textInput, { marginTop: 20 }]}
                        textAlignVertical={'center'}
                        placeholder={
                            context.session?.isVehicleAuth()
                                ? 'Entrez votre identifiant'
                                : "Identifiant du véhicule"
                        }
                        value={serial}
                        onChangeText={(newSerial) => setSerial(newSerial)}
                    />
                    <TextInput
                        testID="passwordInput"
                        style={[styles.textInput, { marginTop: 15 }]}
                        textAlignVertical={'center'}
                        placeholder={
                            context.session?.isVehicleAuth()
                                ? 'Entrez votre mot de passe'
                                : 'Mot de passe du véhicule'
                        }
                        secureTextEntry={true}
                        value={password}
                        onChangeText={(newPassword) => setPassword(newPassword)}
                    />
                    <Button
                        testID="loginBtn"
                        mode="contained"
                        onPress={() => {
                            context.session?.isVehicleAuth()
                                ? onLoginUser()
                                : onLoginVehicle();
                        }}
                        style={{ marginTop: 20 }}
                        contentStyle={{ backgroundColor: '#6A93F4' }}
                        labelStyle={{
                            fontFamily: 'Poppins_600SemiBold',
                            fontSize: 25,
                            textTransform: 'none',
                        }}
                    >
                        Connexion
                    </Button>
                </KeyboardAvoidingView>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        height: '100%',
    },
    contained: {
        flex: 5,
        backgroundColor: '#f0f0f0',
        width: '70%',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
    },
    textInput: {
        paddingTop: 25,
        paddingBottom: 25,
        paddingLeft: 30,
        borderWidth: 0,
        backgroundColor: '#fff',
        borderColor: '#FB4B4F',
        borderRadius: 30,
        fontFamily: 'Poppins_400Regular',
        fontSize: 16,
    },
    circle: {
        position: 'absolute',
        zIndex: 1,
        width: 290,
        height: 290,
        borderRadius: 290 / 2,
        backgroundColor: '#FB4B4F70',
    },
    logo: {
        width: 372,
        height: 372,
        resizeMode: 'stretch',
        margin: 0,
    },
    vehicleLabel: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 25,
        marginTop: 10,
    },
});
