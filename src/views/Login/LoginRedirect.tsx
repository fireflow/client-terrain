import React, { useEffect } from 'react';
import { StackNavigationHelpers } from '@react-navigation/stack/lib/typescript/src/types';
import { Route } from '@react-navigation/native';
import { ContextConsumerHook } from '~/store/context';

import { authenticateCheckRedirection } from '~/views/Login/utils/authUtils';

type LoginProps = {
    navigation: StackNavigationHelpers;
    router: Route<any>;
};
export default function LoginRedirect(props: LoginProps) {
    const [context, dispatch] = ContextConsumerHook();

    useEffect(() => {
        authenticateCheckRedirection(context, dispatch)
            .then(() => props.navigation.navigate('MainPage'))
            .catch(() => props.navigation.navigate('Login'));
    }, []);

    return <></>;
}
