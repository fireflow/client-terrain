import { StyleSheet, View, Text } from 'react-native';
import React, { useEffect, useState } from 'react';
import { DrawerNavigationHelpers } from '@react-navigation/drawer/lib/typescript/src/types';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CustomRouteProp } from '~/models/route.model';
import { ContextConsumerHook } from '~/store/context';
import FireflowNavBar from '~/components/FireflowNavBar';
import { WebView } from 'react-native-webview';
import env from '~/GetEnv';

type CommunicationPageProps = {
    navigation: DrawerNavigationHelpers;
    route: CustomRouteProp<any>;
};

export default function CommunicationPage(props: CommunicationPageProps) {
    const [context] = ContextConsumerHook();
    const [token, setToken] = useState<string | null>(null);
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<boolean>(false);

    useEffect(() => {
        const getToken = async () =>
            setToken(await AsyncStorage.getItem('fireflow-auth'));
        getToken();
    }, []);

    return (
        <View style={styles.container}>
            <FireflowNavBar navigation={props.navigation} />
            <View style={styles.containedColumn}>
                <View style={{ width: '100%', height: '100%' }}>
                    {context.intervention ? (
                        <>
                            {token && (
                                <>
                                    <WebView
                                        testID={'webview'}
                                        source={{
                                            uri: `${
                                                env.pcClientUrl
                                            }/webview/${context.intervention
                                                ?.id!}?token=${token}`,
                                        }}
                                        onLoadStart={() => setLoading(true)}
                                        onLoad={() => setLoading(false)}
                                        onError={() => {
                                            setLoading(false);
                                            setError(true);
                                        }}
                                    />
                                    {loading && (
                                        <View style={styles.contained}>
                                            <Text style={styles.title}>
                                                Chargement de l'oct...
                                            </Text>
                                        </View>
                                    )}
                                    {error && (
                                        <View style={styles.contained}>
                                            <Text
                                                testID={'octErrorText'}
                                                style={styles.title}
                                            >
                                                ERREUR LORS DE LA RECUPERATION
                                                DE L'OCT
                                            </Text>
                                        </View>
                                    )}
                                </>
                            )}
                        </>
                    ) : (
                        <View style={styles.contained}>
                            <Text style={styles.title}>
                                Aucun chantier en cours.
                            </Text>
                        </View>
                    )}
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        height: '100%',
    },
    contained: {
        flex: 5.5,
        backgroundColor: '#f0f0f0',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
    },
    containedColumn: {
        flex: 5.5,
        backgroundColor: '#f0f0f0',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
    },
    title: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 45,
    },
});
