import { StyleSheet, Text, View } from 'react-native';
import React, { useEffect, useState } from 'react';
import { DrawerNavigationHelpers } from '@react-navigation/drawer/lib/typescript/src/types';
import { CustomRouteProp } from '~/models/route.model';
import {
    ActivityIndicator,
    Divider,
    IconButton,
    Surface,
} from 'react-native-paper';
import OctDao from '~/models/oct.dao';
import { ContextConsumerHook } from '~/store/context';
import OctModel, { Sector } from '~/models/oct.model';
import NotificationHelper from '~/services/NotificationHelper';
import FireflowNavBar from '~/components/FireflowNavBar';
import uuid from 'react-native-uuid';

type CommunicationPageProps = {
    navigation: DrawerNavigationHelpers;
    route: CustomRouteProp<any>;
};

export default function CommunicationPage(props: CommunicationPageProps) {
    const [context] = ContextConsumerHook();

    const [oct, setOct] = useState(new OctModel());
    const [dataLoaded, setDataLoaded] = useState(false);
    const [error, setError] = useState(false);

    const refreshOct = async () => {
        setDataLoaded(false);
        setError(false);
        if (context?.intervention?.id) {
            OctDao.getOct(context.intervention.id)
                .then((data) => {
                    setOct(data);
                    setDataLoaded(true);
                })
                .catch((err: Error) => {
                    setDataLoaded(true);
                    setError(true);
                    NotificationHelper.Error(
                        'Failed to Update OCT',
                        err.message
                    );
                });
        } else {
            setDataLoaded(true);
            setError(true);
        }
    };

    const getSector = (sector: Sector) => {
        return (
            <View style={styles.containedRow} key={`${uuid.v4()}`}>
                <Surface style={styles.surface}>
                    <View>
                        {sector.frequencies.map((frq) => (
                            <Text style={{ fontSize: 20 }} key={`${uuid.v4()}`}>
                                {frq}
                            </Text>
                        ))}
                    </View>
                    <Divider style={styles.divider} />
                    <View>
                        <Text style={{ fontSize: 20 }}>{sector.name}</Text>
                        <Text style={{ fontSize: 20 }}>{sector.ltn}</Text>
                    </View>
                </Surface>
                {sector.child && getSector(sector.child)}
            </View>
        );
    };

    useEffect(() => {
        refreshOct();
    }, []);

    if (!dataLoaded) {
        return (
            <View style={styles.container}>
                <FireflowNavBar navigation={props.navigation} />
                <IconButton
                    color={'#f64c4c'}
                    icon="refresh"
                    size={50}
                    onPress={() => refreshOct()}
                />
                <View style={styles.containedColumn}>
                    <Text style={{ fontSize: 50, fontWeight: 'bold' }}>
                        OCT DE COMMUNICATION
                    </Text>
                    <View style={styles.containedRow}>
                        <ActivityIndicator
                            animating={true}
                            size={250}
                            color={'#f64c4c'}
                        />
                    </View>
                </View>
            </View>
        );
    }

    if (
        error ||
        !oct ||
        !oct.codis ||
        !oct.pccos ||
        !oct.transit ||
        !oct.sectors
    )
        return (
            <View style={styles.container}>
                <FireflowNavBar navigation={props.navigation} />
                <IconButton
                    color={'#f64c4c'}
                    icon="refresh"
                    size={50}
                    onPress={() => refreshOct()}
                />
                <View style={styles.containedColumn}>
                    <Text style={{ fontSize: 50, fontWeight: 'bold' }}>
                        OCT DE COMMUNICATION
                    </Text>
                    <View style={styles.containedRow}>
                        <Text
                            testID="octErrorText"
                            style={{ fontSize: 50, fontWeight: 'bold' }}
                        >
                            ERREUR LORS DE LA RECUPERATION DE L'OCT
                        </Text>
                    </View>
                </View>
            </View>
        );

    return (
        <View style={styles.container}>
            <FireflowNavBar navigation={props.navigation} />
            <IconButton
                color={'#f64c4c'}
                icon="refresh"
                size={50}
                onPress={() => refreshOct()}
            />
            <View style={styles.containedColumn}>
                <Text style={{ fontSize: 50, fontWeight: 'bold' }}>
                    OCT DE COMMUNICATION
                </Text>
                <View style={styles.containedRow}>
                    <Surface style={styles.surface}>
                        <Text style={{ fontSize: 20 }}>{oct.codis}</Text>
                        <Divider style={styles.divider} />
                        <Text style={{ fontSize: 20 }}>{'CODIS'}</Text>
                    </Surface>
                </View>
                <View style={styles.containedRow}>
                    <Surface style={styles.surface}>
                        <View>
                            {oct.pccos.map((frq) => (
                                <Text
                                    style={{ fontSize: 20 }}
                                    key={`${uuid.v4()}`}
                                >
                                    {frq}
                                </Text>
                            ))}
                        </View>
                        <Divider style={styles.divider} />
                        <View>
                            <Text style={{ fontSize: 20 }}>{'PC/COS'}</Text>
                        </View>
                    </Surface>
                    <Surface style={styles.surface}>
                        <View>
                            {oct.transit.map((frq) => (
                                <Text
                                    style={{ fontSize: 20 }}
                                    key={`${uuid.v4()}`}
                                >
                                    {frq}
                                </Text>
                            ))}
                        </View>
                        <Divider style={styles.divider} />
                        <View>
                            <Text style={{ fontSize: 20 }}>{'Transit'}</Text>
                        </View>
                    </Surface>
                </View>
                {oct.sectors.map((sec) => getSector(sec))}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        height: '100%',
    },
    contained: {
        flex: 5.5,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
    },
    containedColumn: {
        flex: 5.1,
        backgroundColor: '#f0f0f0',
        width: '70%',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        height: '100%',
    },
    containedRow: {
        flex: 5.5,
        backgroundColor: '#f0f0f0',
        flexDirection: 'row',
        width: '70%',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        height: '100%',
    },
    label: {
        fontSize: 70,
        fontFamily: 'Poppins_700Bold',
        color: '#fff',
    },
    button: {
        borderRadius: 30,
        width: '40%',
    },
    divider: {
        height: 100,
        margin: 20,
        width: 1,
        color: '#000000',
    },
    line: {
        height: 5,
        width: 25,
        color: '#000000',
    },
    surface: {
        flexDirection: 'row-reverse',
        padding: 8,
        minHeight: 80,
        minWidth: 120,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4,
    },
    modal: {
        backgroundColor: 'white',
        position: 'absolute',
        top: '25%',
        left: '25%',
        minWidth: '50%',
        height: '50%',
        justifyContent: 'space-evenly',
    },
});
