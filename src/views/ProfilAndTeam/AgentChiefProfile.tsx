import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {ContextConsumerHook} from '~/store/context';
import LessAndMoreField from '~/components/LessAndMoreField';
import Unit from '~/models/unit.model';
import UnitDao from '~/models/unit.dao';
import NotificationHelper from '~/services/NotificationHelper';
import {VehiclesTypesFormatted} from "~/models/vehicle.model";
import WaterGauge from "~/components/WaterGauge";

type AgentChiefProfileProps = {
    unit: Unit;
    readOnly?: boolean;
};

export default function AgentChiefProfile(props: AgentChiefProfileProps) {
    //const vehiclesModal = useRef<ModalRef>(null);
    const [context, dispatch] = ContextConsumerHook();

    //const onSelectVehicle = () => vehiclesModal.current?.openSelect();
    //const onSelectWaterCapacity = () =>
      //  waterCapacityModal.current?.openSelect();
    //const onCancelWaterCapacity = () =>
      //  waterCapacityModal.current?.closeSelect();

    const updateUnit = (unit: Unit, successText: string) => {
        UnitDao.update(unit)
            .then(() => {
                // Update the value in the app context
                dispatch('SetUnit', unit);
                NotificationHelper.Success(
                    "Mise à jour de l'unité",
                    successText
                );
            })
            .catch((err: Error) =>
                NotificationHelper.Error(
                    "Echec de la mise à jour de l'unité",
                    err.message
                )
            );
    };

    const onNbMenUpdate = (value: number) => {
        const unit: Unit = context.unit!;
        unit.nb_men = value;
        updateUnit(unit, "Le nombre d'hommes a bien été mis à jour.");
    };

    // const onVehicleSelected = (vehicleId: string) => {
    //     const unit: Unit = context.unit!;
    //     unit.vehicle_id = vehicleId;
    //     updateUnit(unit, 'Le type de véhicule a bien été mis à jour.');
    //     //vehiclesModal.current?.closeSelect();
    // };

    const onWaterCapacityUpdate = (value: number) => {
        const unit: Unit = context.unit!;
        unit.capacity = value;
        updateUnit(unit, "La capacité d'eau a bien été mise à jour.");
    };

    return (
        <>
            <View style={[styles.Row, { marginTop: 60 }]}>
                <LessAndMoreField
                    value={props.unit.nb_men!}
                    text="Nombre d'hommes :"
                    onUpdate={onNbMenUpdate}
                    readOnly={props.readOnly}
                    negativeErrorMessage={
                        "Le nombre d'hommes ne peut pas être nulle."
                    }
                />
                { context.session.vehicle && context.session.vehicle.type &&
                    <View style={{marginLeft: 100}}>
                        <Text style={{fontFamily: 'Poppins_600SemiBold', fontSize: 25, marginBottom: 20}}>
                            Type de Véhicule :
                        </Text>
                        <View style={{...styles.Row, justifyContent: "space-around", alignItems: "center"}}>
                            <Text style={{fontSize: 40, fontFamily: 'Poppins_600SemiBold'}}>
                                {VehiclesTypesFormatted[context.session.vehicle.type].label}
                            </Text>
                            <Image

                                style={{width: 125, aspectRatio: 4 / 3}}
                                source={VehiclesTypesFormatted[context.session.vehicle.type].icon}
                            />
                        </View>
                    </View>
                }
                <WaterGauge
                    value={props.unit.capacity!}
                    maxValue={context.session.vehicle?.water_capacity!}
                    onUpdate={onWaterCapacityUpdate}
                    readOnly={props.readOnly}
                    negativeErrorMessage={
                        "Le nombre d'hommes ne peut pas être nulle."
                    }
                />
            </View>
            {/* <Modal ref={vehiclesModal}>
                <SelectList
                    options={context.vehicles!}
                    selected={props.unit.vehicle_id}
                    onItemSelected={onVehicleSelected}
                    text="Sélectionner votre véhicule dans la liste :"
                />
            </Modal> */}

        </>
    );
}

const styles = StyleSheet.create({
    Row: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    Title: {
        fontSize: 40,
        fontFamily: 'Poppins_600SemiBold',
    },
    Text: {
        fontFamily: 'Poppins_400Regular',
        fontSize: 30,
    },
});
