import React, { useState, useRef } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-paper';
import * as ImagePicker from 'expo-image-picker';
import { StackNavigationHelpers } from '@react-navigation/stack/lib/typescript/src/types';
import { useFocusEffect, RouteProp } from '@react-navigation/native';
import { ContextConsumerHook } from '~/store/context';
import NotificationHelper from '~/services/NotificationHelper';
import Unit, { UnitRoles } from '~/models/unit.model';
import UnitDao from '~/models/unit.dao';
import MediaDao from '~/models/media.dao';
import { ViewParams } from '~/views/ViewParams';
import AgentChiefProfile from '~/views/ProfilAndTeam/AgentChiefProfile';
import GroupChiefProfile from '~/views/ProfilAndTeam/GroupChiefProfile';
import ColumnChiefProfile from '~/views/ProfilAndTeam/ColumnChiefProfile';
import FireFlowButton from '~/components/FireflowButton';
import ProfilPicture from '~/components/ProfilPicture';
import Modal, { ModalRef } from '~/components/Modal';
import ImagePickerButtons from '~/components/ImagePicker';
import AuthDao from '~/models/auth.dao';
import AsyncStorage from '@react-native-async-storage/async-storage';

type ProfileAndTeamProps = {
    route: RouteProp<ViewParams, 'ProfilAndTeam'>;
    navigation: StackNavigationHelpers;
};
export default function ProfileAndTeam(props: ProfileAndTeamProps) {
    const [context, dispatch] = ContextConsumerHook();
    const [unit, setUnit] = useState<Unit>();
    const [parentUnit, setParentUnit] = useState<Unit>(); // TODO: A mettre directement dans la première requête
    const modal = useRef<ModalRef>(null);
    const isCurrentUnit = !props.route.params?.unitId ? true : false;

    useFocusEffect(
        // Retrieve unit data
        React.useCallback(() => {
            setUnit(undefined);
            if (isCurrentUnit) {
                UnitDao.getMe()
                    .then((unit: Unit) => {
                        dispatch('SetUnit', unit);
                        setUnit(unit);
                        loadParentUnit(unit.parent_id);
                    })
                    .catch((err: Error) =>
                        NotificationHelper.Error(
                            'Echec de la récupération des données',
                            err.message
                        )
                    );
            } else {
                UnitDao.get(props.route.params.unitId!)
                    .then((unit: Unit) => {
                        setUnit(unit);
                        loadParentUnit(unit.parent_id);
                    })
                    .catch((err: Error) =>
                        NotificationHelper.Error(
                            'Echec de la récupération des données',
                            err.message
                        )
                    );
            }
        }, [])
    );

    // Functions
    const getEditButtonText = (role: UnitRoles) => {
        switch (role) {
            case UnitRoles.GroupChief:
                return 'Gérer le groupe';
            case UnitRoles.ColumnChief:
                return 'Gérer la colonne';
            default:
                return "Gérer l'unité";
        }
    };

    const loadParentUnit = (parent_id?: string) => {
        if (parent_id) {
            UnitDao.get(parent_id)
                .then((unit: Unit) => {
                    setParentUnit(unit);
                })
                .catch((err: Error) =>
                    NotificationHelper.Error(
                        'Echec de la récupération des données',
                        err.message
                    )
                );
        }
    };

    const openUnitInfo = (unitId: string) => {
        props.navigation.push('ProfileAndTeam', { unitId: unitId });
    };

    const getGrade = (): string => {
        return (
            context.grades?.find(
                (grade) => grade.id === context.session?.user?.grade_id
            )?.name! || 'Grade non défini'
        );
    };

    // Events
    const onBack = () => {
        props.navigation.goBack();
    };

    const onDisconnect = () => {
        AuthDao.logoutUser()
            .then(() => {
                NotificationHelper.Success(
                    'Déconnexion',
                    'Vous avez bien été déconnecté de Fireflow.'
                );
            })
            .catch((err: Error) => {
                NotificationHelper.Error('Erreur de déconnexion', err.message);
                dispatch("DeleteVehicleSession");
                AsyncStorage.clear();
            })
            .finally(() => {
                context.interventionEventService.disconnect();
                context.orgEventService.disconnect();
                dispatch("DeleteUserSession");
                dispatch('UnsetIntervention');
                props.navigation.navigate('Login');
            });
    };

    const onEditTeam = () => {
        props.navigation.navigate('EditTeam');
    };

    const onProfilPicturePicked = (result: ImagePicker.ImagePickerResult) => {
        if (result.cancelled) return;

        MediaDao.uploadProfilePicture(result)
            .then(() => {
                dispatch('SetProfilPicture', result.uri!);
                NotificationHelper.Success(
                    "Mise à jour de l'unité",
                    'La photo de profil a bien été mise à jour.'
                );
            })
            .catch((err: Error) =>
                NotificationHelper.Error(
                    'Echec de la mise à jour de la photo de profil',
                    err.message
                )
            );

        modal.current?.closeSelect();
    };

    if (!unit) {
        return <></>;
    }

    return (
        <View style={styles.ProfilAndTeam}>
            <View style={{ width: 1000 }}>
                <View style={styles.TitleRow}>
                    <View>
                        <Button
                            children={false}
                            labelStyle={{ fontSize: 100 }}
                            icon="arrow-left"
                            mode="text"
                            color="#FB4B4F"
                            onPress={onBack}
                        />
                    </View>
                    <View>
                        <Text style={[styles.Title]}>Profil & équipe</Text>
                    </View>
                    <View />
                </View>
            </View>

            <View style={[styles.Row, styles.Center]}>
                <View style={[styles.Row, styles.Center, { marginRight: 150 }]}>
                    <ProfilPicture
                        editable={isCurrentUnit}
                        onPress={() => modal.current?.openSelect()}
                        source={context.profilePicture}
                    />
                    <View style={{ marginLeft: 25 }}>
                        <Text
                            style={[styles.Text, { fontSize: 45 }]}
                        >{`${unit.leader_data?.last_name} ${unit.leader_data?.first_name}`}</Text>
                        <Text style={styles.Text}>{getGrade()}</Text>
                        <Text style={[styles.Text, styles.Bold]}>
                            {unit.name}
                        </Text>
                        {unit?.role !== UnitRoles.ColumnChief && parentUnit && (
                            <Text
                                style={[styles.Text, styles.Bold]}
                                onLongPress={() => openUnitInfo(parentUnit.id!)}
                            >
                                {parentUnit.name}
                            </Text>
                        )}
                    </View>
                </View>
                {isCurrentUnit && (
                    <View>
                        <FireFlowButton
                            text="Déconnexion"
                            width={259}
                            height={70}
                            onPress={onDisconnect}
                            testID="userDisconnectBtn"
                        />
                        {context.intervention &&
                            unit?.role !== UnitRoles.AgentChief && (
                                <View style={{ marginTop: 15 }}>
                                    <FireFlowButton
                                        text={getEditButtonText(unit?.role!)}
                                        width={259}
                                        height={70}
                                        onPress={onEditTeam}
                                        testID="toTeamEdit"
                                    />
                                </View>
                            )}
                    </View>
                )}
            </View>

            {unit?.role === UnitRoles.AgentChief && (
                <AgentChiefProfile unit={unit} readOnly={!isCurrentUnit} />
            )}

            {unit?.role === UnitRoles.GroupChief && (
                <GroupChiefProfile unit={unit} navigation={props.navigation} />
            )}

            {unit?.role === UnitRoles.ColumnChief && (
                <ColumnChiefProfile unit={unit} navigation={props.navigation} />
            )}

            <Modal ref={modal}>
                <ImagePickerButtons onImagePick={onProfilPicturePicked} />
            </Modal>
        </View>
    );
}

const styles = StyleSheet.create({
    ProfilAndTeam: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Row: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    Center: {
        alignItems: 'center',
    },
    Bold: {
        fontWeight: 'bold',
    },
    Title: {
        fontSize: 40,
        fontFamily: 'Poppins_600SemiBold',
    },
    Text: {
        fontFamily: 'Poppins_400Regular',
        fontSize: 30,
    },
    TitleRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        alignSelf: 'stretch',
        marginBottom: 20,
    },
    GridRow: {
        borderLeftWidth: 2,
        borderTopWidth: 2,
    },
    GridCol: {
        borderRightWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    GridHeaderText: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 25,
    },
    GridText: {
        fontFamily: 'Poppins_400Regular',
        fontSize: 25,
    },
});
