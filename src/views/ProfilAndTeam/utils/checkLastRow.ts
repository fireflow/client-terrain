export const checkLastRow = (index: number, length: number): Object => {
    if (length - 1 === index) return { borderBottomWidth: 2 };
    return {};
};
