import { ScrollView } from 'react-native-gesture-handler';
import { Col, Grid, Row } from 'react-native-easy-grid';
import { StyleSheet, Text, View } from 'react-native';
import Unit from '~/models/unit.model';
import React from 'react';
import { ContextConsumerHook } from '~/store/context';
import { checkLastRow } from '~/views/ProfilAndTeam/utils/checkLastRow';
import { StackNavigationHelpers } from '@react-navigation/stack/lib/typescript/src/types';

type ColumnChiefProfileProps = {
    navigation: StackNavigationHelpers;
    unit: Unit;
};
export default function ColumnChiefProfile(props: ColumnChiefProfileProps) {
    const [context] = ContextConsumerHook();

    const openUnitInfo = (unitId: string) => {
        props.navigation.push('ProfileAndTeam', { unitId: unitId });
    };

    return (
        <View style={{ marginTop: 20, width: 1000, maxHeight: '30%' }}>
            <ScrollView>
                <Grid>
                    <Row style={styles.GridRow} size={2}>
                        <Col style={styles.GridCol}>
                            <Text style={styles.GridHeaderText}>Groupes</Text>
                        </Col>
                        <Col style={styles.GridCol}>
                            <Text style={styles.GridHeaderText}>
                                Nombre d'hommes
                            </Text>
                        </Col>
                        <Col style={styles.GridCol}>
                            <Text style={styles.GridHeaderText}>
                                Responsable du groupe
                            </Text>
                        </Col>
                    </Row>
                    {context.intervention && props.unit.children?.length ? (
                        props.unit.children?.map(
                            (item: Unit, i: number, arr: Unit[]) => {
                                return (
                                    <Row
                                        key={i}
                                        style={[
                                            styles.GridRow,
                                            checkLastRow(i, arr.length),
                                        ]}
                                        size={3}
                                    >
                                        <Col style={styles.GridCol}>
                                            <Text
                                                style={styles.GridText}
                                                onLongPress={() =>
                                                    openUnitInfo(item.id!)
                                                }
                                            >
                                                {item.name}
                                            </Text>
                                        </Col>
                                        <Col style={styles.GridCol}>
                                            <Text style={styles.GridText}>
                                                {item.nb_men}
                                            </Text>
                                        </Col>
                                        <Col style={styles.GridCol}>
                                            <Text style={styles.GridText}>
                                                {`${item.leader_data?.last_name.toUpperCase()} ${
                                                    item.leader_data?.first_name
                                                }`}
                                            </Text>
                                        </Col>
                                    </Row>
                                );
                            }
                        )
                    ) : (
                        <Row
                            size={4}
                            style={[styles.GridRow, { borderBottomWidth: 2 }]}
                        >
                            <Col style={styles.GridCol}>
                                <Text style={styles.GridText}>
                                    {context.intervention
                                        ? 'Aucun groupe assigné.'
                                        : 'Aucun chantier en cours.'}
                                </Text>
                            </Col>
                        </Row>
                    )}
                </Grid>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    ProfilAndTeam: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    Row: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    Center: {
        alignItems: 'center',
    },
    Bold: {
        fontWeight: 'bold',
    },
    Title: {
        fontSize: 40,
        fontFamily: 'Poppins_600SemiBold',
    },
    Text: {
        fontFamily: 'Poppins_400Regular',
        fontSize: 30,
    },
    TitleRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        alignSelf: 'stretch',
        marginBottom: 20,
    },
    GridRow: {
        borderLeftWidth: 2,
        borderTopWidth: 2,
    },
    GridCol: {
        borderRightWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    GridHeaderText: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 25,
    },
    GridText: {
        fontFamily: 'Poppins_400Regular',
        fontSize: 25,
    },
});
