//const path = 'https://fireflow-server.azurewebsites.net'
import AsyncStorage from '@react-native-async-storage/async-storage';
import env from './GetEnv';

const path = !!env.apiUrl ? env.apiUrl : 'http://localhost:4242';
const websocketEndpoint = !!env.webSocketEndpoint
    ? env.webSocketEndpoint
    : 'ws://localhost:4242';

const ApplicationConf = {
    endpoint: {
        interventionWebsocket(orgId: string, interventionId: string): string {
            //return `${websocketEndpoint}/org/${orgId}/intervention/${interventionId}/ws`
            return `${websocketEndpoint}/intervention/${interventionId}/ws`;
        },
        orgWebsocket(orgId: string): string {
            return `${websocketEndpoint}/org/${orgId}/ws`;
        },
    },
    async getHeaders(): Promise<Record<string, string>> {
        return {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${await AsyncStorage.getItem(
                'fireflow-auth'
            )}`,
        };
    },

    async getAuthorization(): Promise<string> {
        return `Bearer ${await AsyncStorage.getItem('fireflow-auth')}`;
    },
    auth: {
        loginUser(): string {
            return `${path}/auth/login`;
        },
        logoutUser(): string {
            return `${path}/auth/logout`;
        },
        loginVehicle(): string {
            return `${path}/vehicle/login`;
        },
    },
    user: {
        getMe(): string {
            return `${path}/user/me`;
        },
    },
    intervention: {
        getAssigned(): string {
            return `${path}/intervention/assigned`;
        },
    },
    unit: {
        get(unitId: string): string {
            return `${path}/unit/${unitId}`;
        },
        getMe(): string {
            return `${path}/unit/me`;
        },
        getAvailable(interventionId: string): string {
            return `${path}/intervention/${interventionId}/unit`;
        },
        update(unitId: string): string {
            return `${path}/unit/${unitId}`;
        },
        assign(interventionId: string): string {
            return `${path}/intervention/${interventionId}/unit/assign`;
        },
    },
    weather: {
        getByCoordinate(lat: number, lon: number) {
            return `${path}/weather?lat=${lat}&long=${lon}`;
        },
    },
    oct: {
        get(interventionId: string) {
            return `${path}/intervention/${interventionId}/oct`;
        },
    },
    vehicle: {
        getMe(): string {
            return `${path}/vehicle/me`;
        },
    },
    marker: {
        getAll(interventionId: string) {
            return `${path}/intervention/${interventionId}/marker`;
        },
        getById(markerId: string) {
            return `${path}/marker/${markerId}`;
        },
        create(interventionId: string) {
            return `${path}/intervention/${interventionId}/marker`;
        },
        update(markerId: string) {
            return `${path}/marker/${markerId}`;
        },
        delete(markerId: string) {
            return `${path}/marker/${markerId}`;
        },
        add: `${path}/marker`,
    },
    media: {
        getInterventionPictures(interventionId: string): string {
            return `${path}/intervention/${interventionId}/pictures`;
        },
        uploadInterventionPicture(interventionId: string): string {
            return `${path}/media/upload/intervention/${interventionId}`;
        },
        uploadProfilePicture(): string {
            return `${path}/media/upload/profile_picture`;
        },
    },
    grade: {
        getGrades(orgId: string) {
            return `${path}/org/${orgId}/grade`;
        },
    },
};

export default ApplicationConf;
