import React, { useState } from 'react';
import { StyleSheet, View, Text, Image, TextInput } from 'react-native';
import FireFlowButton from '~/components/FireflowButton';
import { AlbumPicture } from '~/components/Album';

type ImageInfoProps = {
    picture: AlbumPicture;
    isNewPicture: boolean;
    onClose: Function;
    onValid: Function;
};

export default function ImageInfo(props: ImageInfoProps) {
    const [picture, setPicture] = useState<AlbumPicture>({
        name: props.picture.name,
        description: props.picture.description,
        src: props.picture.src,
    });

    return (
        <View
            style={{
                alignItems: 'center',
                justifyContent: 'center',
            }}
        >
            <Text
                style={[
                    styles.Title,
                    { marginBottom: 20, fontSize: 40, textAlign: 'center' },
                ]}
            >
                {props.isNewPicture
                    ? 'Ajouter une photo'
                    : 'Détails de la photo'}
            </Text>
            <View
                style={{
                    flexDirection: 'row',
                    height: '75%',
                }}
            >
                <Image
                    source={{ uri: props.picture.src }}
                    style={{
                        width: '50%',
                        height: '100%',
                        marginRight: 20,
                    }}
                />
                <View>
                    <Text style={[styles.Title, { textAlign: 'left' }]}>
                        Nom
                    </Text>
                    {props.isNewPicture ? (
                        <TextInput
                            testID={'name-input'}
                            style={styles.TextInput}
                            textAlignVertical={'top'}
                            placeholder="Entrez le nom de l'image"
                            value={picture.name}
                            onChangeText={(newTitle) => {
                                setPicture({ ...picture, name: newTitle });
                            }}
                        />
                    ) : (
                        <Text style={styles.Text}>{picture.name}</Text>
                    )}
                    <Text
                        style={[
                            styles.Title,
                            { textAlign: 'left', marginTop: 20 },
                        ]}
                    >
                        Description
                    </Text>
                    {props.isNewPicture ? (
                        <TextInput
                            testID={'description-input'}
                            style={[styles.TextInput, { height: 155 }]}
                            multiline={true}
                            numberOfLines={4}
                            maxLength={300}
                            textAlignVertical={'top'}
                            placeholder="Entrez la description de l'image"
                            value={picture.description}
                            onChangeText={(newDescription) => {
                                setPicture({
                                    ...picture,
                                    description: newDescription,
                                });
                            }}
                        />
                    ) : (
                        <Text
                            style={[
                                styles.Text,
                                {
                                    fontStyle: picture.description
                                        ? 'normal'
                                        : 'italic',
                                },
                            ]}
                        >
                            {picture.description
                                ? picture.description
                                : 'Pas de description.'}
                        </Text>
                    )}
                    {props.isNewPicture && (
                        <View
                            style={{
                                flexDirection: 'row',
                                maxHeight: 60,
                                marginTop: 20,
                            }}
                        >
                            <FireFlowButton
                                testID={'validate-btn'}
                                text="Valider"
                                width={175}
                                height={70}
                                color={'#54b854'}
                                onPress={() => props.onValid(picture)}
                            />
                            <FireFlowButton
                                testID={'cancel-btn'}
                                style={{ marginLeft: 15 }}
                                text="Annuler"
                                width={175}
                                height={70}
                                onPress={() => props.onClose()}
                            />
                        </View>
                    )}
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    Title: {
        fontSize: 30,
        fontFamily: 'Poppins_600SemiBold',
    },
    Text: {
        fontFamily: 'Poppins_400Regular',
        fontSize: 25,
        width: 350,
    },
    TextInput: {
        paddingTop: 25,
        paddingBottom: 25,
        paddingLeft: 30,
        borderWidth: 0,
        backgroundColor: '#f5f5f5',
        borderColor: '#FB4B4F',
        borderRadius: 30,
        fontFamily: 'Poppins_400Regular',
        fontSize: 16,
        width: 350,
    },
});
