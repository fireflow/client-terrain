import React from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
    ScrollView,
} from 'react-native';
import { Divider } from 'react-native-paper';

type Options = {
    id?: string;
    name?: string;
}[];
type SelectList = {
    text: string;
    options: Options;
    selected?: string;
    onItemSelected: Function;
};

export default function SelectList(props: SelectList) {
    const { text, options, selected, onItemSelected } = props;

    return (
        <View>
            <Text style={styles.Title}>{text}</Text>
            <ScrollView style={{ maxHeight: 400 }}>
                {options.map((opt, i) => {
                    return (
                        <View style={{ alignItems: 'center' }} key={i}>
                            <TouchableOpacity
                                testID={`option-${opt.id}`}
                                key={opt.id}
                                onPress={() => onItemSelected(opt.id)}
                            >
                                <View style={styles.ItemView}>
                                    <Text
                                        style={[
                                            styles.ItemText,
                                            opt.id === selected
                                                ? styles.ItemTextSelected
                                                : {},
                                        ]}
                                    >
                                        {opt.name}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            {i !== options.length - 1 && (
                                <Divider
                                    style={{
                                        width: '40%',
                                        height: 3,
                                        backgroundColor: '#000',
                                    }}
                                />
                            )}
                        </View>
                    );
                })}
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    Title: {
        fontSize: 30,
        fontFamily: 'Poppins_600SemiBold',
        textAlign: 'center',
        marginBottom: 10,
    },
    ItemView: {
        marginTop: 15,
        marginBottom: 15,
    },
    ItemText: {
        fontFamily: 'Poppins_400Regular',
        fontSize: 30,
        textAlign: 'center',
    },
    ItemTextSelected: {
        fontWeight: 'bold',
        color: '#FB4B4F',
    },
});
