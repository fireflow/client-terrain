import {Image, Platform, StyleSheet, View} from "react-native";
import MapView, {Marker, Polygon} from "react-native-maps";
import React, {useEffect, useState} from 'react';
import * as Location from 'expo-location';
import * as TaskManager from 'expo-task-manager';
import {LocationObject} from "expo-location/src/Location.types";
import {EventsType, UnitPayload} from "~/services/events/types";
import {ContextConsumerHook} from "~/store/context";
import uuid from "react-native-uuid";
import MarkerDao from "~/models/marker.dao";
import MarkerModel, {MarkerTypeToLabel} from "~/models/marker.model";
import OctModel from "~/models/oct.model";
import OctDao from "~/models/oct.dao";
import NotificationHelper from "~/services/NotificationHelper";
import {Coordinate} from "~/models/intervention.model";
import {EventStatus} from "~/models/event.model";
import {RouteProp, useFocusEffect} from "@react-navigation/native";
import {DrawerNavigationHelpers} from "@react-navigation/drawer/lib/typescript/src/types";
import {ViewParams} from "~/views/ViewParams";
import {VehiclesTypesFormatted} from "~/models/vehicle.model";

interface ILocations {
    locations: Array<LocationObject>
}

interface ILocations {
    locations: Array<LocationObject>
}

type CartographyProps = {
    unitsLocations: UnitPayload[];
    fireStatus: number;
    editingFire: boolean,
    addingToPolygon: boolean,
    getWeather: Function,
    calculateCentroid: Function,
    getSector: Function,
    navigation:  DrawerNavigationHelpers,
    route: RouteProp<ViewParams, 'Home'>,
    setMapView: Function,
    mapView: MapView | null,
}


const LOCATION_TASK_NAME = 'background-location-task';

export default function Cartography(props: CartographyProps) {
    const [context, dispatch] = ContextConsumerHook();
    const [customizedMarkers, setCustomizedMarkers] = useState<MarkerModel[]>([]);
//    const [currentLocation, setCurrentLocation] = useState<LocationObject | null>(null);
    const [oct, setOct] = useState<OctModel>(new OctModel())

    const addToPolygon = (coordinate: Coordinate) => {
        dispatch("SetFireZonePolygon", [...context.fireZonePolygon, coordinate]);
    }

    const handleMarkerModification = () => {
        MarkerDao.getAll(context.intervention!.id!).then(markers => setCustomizedMarkers(markers))
        NotificationHelper.schedulePushNotification({
            content: {
                title: "Nouveaux Marqueurs",
                body: "De nouveaux marqueurs ont étés ajoutés à l'intervention en cours",
            },
            trigger: {seconds: 1},
        })
    }

    const requestLocationPermissions = async () => {
        const fStatus = await Location.requestForegroundPermissionsAsync()
        if (fStatus.status === 'granted') {
            const bStatus = await Location.requestBackgroundPermissionsAsync()
            if (bStatus.status === 'granted') {
                let location = await Location.getCurrentPositionAsync({});
                dispatch("SetCurrentLocation", location);
                TaskManager.defineTask(LOCATION_TASK_NAME, ({ data, error }) => {
                    const res = data as ILocations
                    if (error) {
                        console.error(error)
                        return;
                    }
                    if (data && res.locations.length) {
                        dispatch("SetCurrentLocation", res.locations[0]);
                        context.orgEventService.sendMessage(EventsType.UnitLocation, {
                            longitude: res.locations[0].coords.longitude,
                            latitude: res.locations[0].coords.latitude,
                            vehicleType: context.session.vehicle!.type!,
                            unitId: context.unit?.id!
                        })
                    }
                })
                await Location.startLocationUpdatesAsync(LOCATION_TASK_NAME, {
                    accuracy: Location.Accuracy.Highest,
                    timeInterval: 1,
                    distanceInterval: 1
                });
            }
        }
    };

    const isInside = (point: Coordinate) => {
        const vs = context.fireZonePolygon
        var x = point.longitude, y = point.latitude;

        var inside = false;
        for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
            var xi = vs[i].longitude, yi = vs[i].latitude;
            var xj = vs[j].longitude, yj = vs[j].latitude;

            var intersect = ((yi > y) != (yj > y))
                && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) inside = !inside;
        }

        return inside;
    };

    const getFire = () => {
        if (context.intervention === undefined) {
            return null
        }
        const centroid = props.calculateCentroid()
        const intervention = context.intervention;
        return (
            <View>
                { context.fireZonePolygon.length >= 3 &&
                <Marker
                    key={1}
                    coordinate={centroid}
                    title={'Feu'}
                    description={intervention.description}
                >
                    <Image
                        style={{width: 50, height: 50}}
                        source={require('../assets/images/markers/fire.png')}
                    />
                </Marker>
                }
                { context.fireZonePolygon.length < 3 &&
                <Marker
                    key={1}
                    coordinate={{ latitude: parseFloat(intervention.latitude || '0'), longitude: parseFloat(intervention.longitude || '0') }}
                    title={'Feu'}
                    description={intervention.description}
                >
                    <Image
                        style={{width: 50, height: 50}}
                        source={require('../assets/images/markers/fire.png')}
                    />
                </Marker>
                }
                { context.fireZonePolygon.length >= 3 &&
                <Polygon
                    coordinates={context.fireZonePolygon}
                    strokeColor={polygonTypes[props.fireStatus].stokeColor}
                    fillColor={polygonTypes[props.fireStatus].fillColor}
                    geodesic={true}
                    strokeWidth={6}
                />
                }
            </View>
        )
    }

    const getUnitsMarkers = () =>
        props.unitsLocations.map(u =>
        <>
        { context.userLocation && context.session.vehicle && context.session.vehicle.type &&
                <Marker
                    key={`${uuid.v4()}`}
                    coordinate={{
                        latitude: u.value.latitude,
                        longitude: u.value.longitude
                    }}
                    title={`${u.author.firstname} ${u.author.lastname}`}
                >
                    <Image
                        style={{width: 50, height: 50}}
                        source={VehiclesTypesFormatted[u.value.vehicleType].icon}
                    />
                </Marker>
            }
        </>
        )

    useFocusEffect(
        React.useCallback(() => {
            if (props.route.params?.unitId && props.mapView) {
                const unit = props.unitsLocations.find(unit => unit.author.id === props.route.params?.unitId);
                props.navigation.setParams({unitId : null})

                if (!unit)
                    return NotificationHelper.Error("Impossible de localiser l'unité", "Une erreur s'est produite, impossible de localiser l'unité sur la carte.");

                const r = {
                    latitude: unit.value.latitude,
                    longitude: unit.value.longitude,
                    latitudeDelta: 0.05,
                    longitudeDelta: 0.05,
                };
                props.mapView.animateToRegion(r, 1000);
            }
        }, [props.route])
    );

    useEffect(() => {
        if (Platform.OS !== "web")
            requestLocationPermissions().then()
    }, []);

    useEffect(() => {
        if (!context.intervention) {
            return
        }
        context.orgEventService.registerEventHandler(EventsType.MarkerUpdated, handleMarkerModification);
        context.orgEventService.registerEventHandler(EventsType.MarkerCreated, handleMarkerModification);
        context.orgEventService.registerEventHandler(EventsType.MarkerDeleted, handleMarkerModification);
        OctDao.getOct(context.intervention!.id!).then(oct => setOct(oct))
        MarkerDao.getAll(context.intervention!.id!).then(markers => setCustomizedMarkers(markers))
    }, [context.intervention]);

    useEffect(() => {
        if (context.userLocation && isInside(context.userLocation.coords)) {
            dispatch("SetCurrentStatus", EventStatus.Engaged);
        }
    }, [context.userLocation]);
    const subAreas = oct?.sectors?.map(sec => props.getSector(sec))

    return (
        <MapView
            style={styles.map}
            ref = {(mapView) => props.setMapView(mapView)}
            initialRegion={{
                latitude: 46.073231,
                longitude: 2.433629,
                latitudeDelta: 11,
                longitudeDelta: 11,
            }}

            onPress={e => {
                const coordinates = e.nativeEvent.coordinate
                props.getWeather(coordinates)
                if (props.editingFire && props.addingToPolygon)
                    addToPolygon(coordinates)
            }}
        >
            {
                customizedMarkers.map(m => (
                        <Marker
                            key={`${uuid.v4()}`}
                            coordinate={{
                                latitude: parseFloat(m.latitude!),
                                longitude: parseFloat(m.longitude!)
                            }}
                            title={m.name}
                            description={m.description}
                        >
                            { MarkerTypeToLabel[m.type!].image }
                        </Marker>
                    )
                )
            }
            { context.userLocation && context.session.vehicle && context.session.vehicle.type &&
            <Marker
                key={0}
                coordinate={{
                    latitude: context.userLocation.coords.latitude,
                    longitude: context.userLocation.coords.longitude
                }}
                title={context.unit?.name}
                description={context.unit?.role}
            >
                <Image
                    style={{width: 50, height: 50}}
                    source={VehiclesTypesFormatted[context.session.vehicle!.type!].icon}
                />
            </Marker>
            }
            { getUnitsMarkers() }
            { getFire() }
            {subAreas}
        </MapView>
    )
}


const polygonTypes = [
    {
        stokeColor: 'rgb(0,0,0)',
        fillColor: 'rgba(0,0,0,0.26)',
        message: 'Sous contrôle'
    },
    {
        stokeColor: 'rgb(238,255,0)',
        fillColor: 'rgba(238,255,0,0.26)',
        message: 'Faible'
    },
    {
        stokeColor: 'rgb(255,115,0)',
        fillColor: 'rgba(255,115,0,0.26)',
        message: 'Fort'
    },
    {
        stokeColor: 'rgb(255,0,0)',
        fillColor: 'rgba(255,0,0,0.26)',
        message: 'Critique'
    },
]

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        height: '100%',
    },
    contained: {
        flex: 5.5,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
    },
    overlay: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        padding: 10,
        top: 10,
        width: '100%',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    sub_overlay: {
        flexDirection: 'column',
        justifyContent: 'flex-end',
        flex: 1,
        padding: 10,
        top: 10,
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    legend_display: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#fff'
    },
    iText: {
        fontSize: 20,
        fontStyle: 'italic',
        color: '#fff'
    },
    map: {
        width: '100%',
        height: '100%',
    },
});
