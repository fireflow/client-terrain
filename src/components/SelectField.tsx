import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Surface } from 'react-native-paper';

type Options = {
    id?: string;
    name?: string;
}[];
type SelectFieldProps = {
    text: string;
    options: Options;
    onOpenSelect: Function;
    selected?: string;
    style?: object;
    readOnly?: boolean;
};

export default function SelectField(props: SelectFieldProps) {
    const openSelectModal = () => {
        if (!props.readOnly) props.onOpenSelect();
    };

    const { text, options, selected, style = {} } = props;

    return (
        <View>
            <View style={{ ...style }}>
                <Text style={styles.Title}>{text}</Text>
                <Surface>
                    <TouchableOpacity onPress={openSelectModal}>
                        <View style={styles.Rect}>
                            <Text style={styles.Field}>
                                {options.find((opt) => opt.id === selected)
                                    ?.name || '-'}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </Surface>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    Row: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    Center: {
        alignItems: 'center',
    },
    Title: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 25,
        marginBottom: 20,
    },
    Rect: {
        borderWidth: 2,
        paddingLeft: 20,
        paddingRight: 20,
        height: 70,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFF',
    },
    Field: {
        fontSize: 40,
        fontFamily: 'Poppins_600SemiBold',
    },
});
