import React from 'react';
import { StyleSheet, View, TouchableOpacity, Image } from 'react-native';
import { Avatar } from 'react-native-paper';
import { DrawerNavigationHelpers } from '@react-navigation/drawer/lib/typescript/src/types';
import { ContextConsumerHook } from '~/store/context';
import NavBarButton from './NavBarButton';

type FireflowNavBarProps = {
    navigation?: DrawerNavigationHelpers;
};

export default function FireflowNavBar(props: FireflowNavBarProps) {
    const [context, dispatch] = ContextConsumerHook();
    const defaultProfilPicture = require('~/assets/images/defaultProfilPicture.png');

    const navigateTo = (to: string, saveInContext = true) => {
        const { navigation } = props;
        if (saveInContext) dispatch('SetCurrentScreen', to);
        navigation?.navigate(to);
    };

    const openDrawer = () => {
        const { navigation } = props;
        navigation?.openDrawer();
    };

    return (
        <View style={styles.stickLeft}>
            <View style={styles.container}>
                <TouchableOpacity testID={'open'} onPress={() => openDrawer()}>
                    <Image
                        style={{ width: 40, height: 30, resizeMode: 'stretch' }}
                        source={require('../assets/icons/burger.png')}
                    />
                </TouchableOpacity>
                <NavBarButton
                    testId={'toHome'}
                    onPress={() => navigateTo('Home')}
                    icon={require('../assets/icons/map.png')}
                    iconSize={50}
                    selected={context.currentScreen === 'Home'}
                />
                <NavBarButton
                    testId={'toCommunication'}
                    onPress={() => navigateTo('Communication')}
                    icon={require('../assets/icons/radio.png')}
                    iconSize={50}
                    selected={context.currentScreen === 'Communication'}
                />
                <NavBarButton
                    testId={'toStatus'}
                    onPress={() => navigateTo('Status')}
                    icon={require('../assets/icons/speaker.png')}
                    iconSize={50}
                    selected={context.currentScreen === 'Status'}
                />
                <NavBarButton
                    testId={'toAlert'}
                    onPress={() => navigateTo('Alert')}
                    icon={require('../assets/icons/danger.png')}
                    iconSize={50}
                    selected={context.currentScreen === 'Alert'}
                />
                <NavBarButton
                    testId={'toInfo'}
                    onPress={() => navigateTo('Information')}
                    icon={require('../assets/icons/info.png')}
                    iconSize={45}
                    selected={context.currentScreen === 'Information'}
                />
                <TouchableOpacity
                    testID={'toProfile'}
                    onPress={() => navigateTo('ProfileAndTeam', false)}
                >
                    <Avatar.Image
                        size={50}
                        source={
                            context.profilePicture !== undefined
                                ? { uri: context.profilePicture }
                                : defaultProfilPicture
                        }
                    />
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'column',
    },
    stickLeft: {
        height: '100%',
        textAlign: 'center',
        backgroundColor: '#FF5B5B',
        paddingTop: 30,
        paddingBottom: 30,
        elevation: 20,
        width: '6%',
    },
    ImageView: {
        width: 80,
        alignItems: 'center',
    },
});
