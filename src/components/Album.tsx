import React, { forwardRef, Ref } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';

export type AlbumPicture = {
    name: string;
    description: string;
    src: string;
};
export interface AlbumRef {
    addPicture: (picture: AlbumPicture) => void;
}
type AlbumProps = {
    onInspectImage: Function;
    pictures: AlbumPicture[];
};

function Album(props: AlbumProps, ref: Ref<AlbumRef>) {
    return (
        <>
            {props.pictures.length != 0 ? (
                <View
                    style={{
                        alignItems: 'center',
                        width: '100%',
                    }}
                >
                    <View style={styles.PictureContainer}>
                        {props.pictures.map((picture, i) => {
                            return (
                                <TouchableOpacity
                                    testID={`picture-${i}`}
                                    style={{
                                        width: 200,
                                        height: 200,
                                        marginRight: 10,
                                        marginBottom: 10,
                                    }}
                                    key={i}
                                    onPress={() => props.onInspectImage(i)}
                                >
                                    <Image
                                        source={{ uri: picture.src }}
                                        style={{
                                            width: '100%',
                                            height: '100%',
                                        }}
                                    />
                                </TouchableOpacity>
                            );
                        })}
                    </View>
                </View>
            ) : (
                <View style={styles.Container}>
                    <Text testID={'no-picture-msg'} style={[styles.Text, { marginTop: 50 }]}>
                        L'intervention n'a aucune photo.
                    </Text>
                </View>
            )}
        </>
    );
}

const styles = StyleSheet.create({
    Container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    Text: {
        fontFamily: 'Poppins_400Regular',
        fontSize: 25,
    },
    PictureContainer: {
        flexDirection: 'row',
        width: '90%',
        flexWrap: 'wrap',
        paddingLeft: 10,
        paddingTop: 10,
    },
});

export default forwardRef(Album);
