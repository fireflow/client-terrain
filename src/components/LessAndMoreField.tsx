import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import FireflowButton from './FireflowButton';
import NotificationHelper from '~/services/NotificationHelper';

interface LessAndMoreFieldProps {
    text: string;
    value: number;
    style?: object;
    readOnly?: boolean;
    negativeErrorMessage?: string;
    onUpdate?: (value: number) => void;
}

export default function LessAndMoreField(props: LessAndMoreFieldProps) {
    const [value, setValue] = useState(props.value);

    const onAdd = () => {
        setValue((oldValue) => oldValue + 1);
        props.onUpdate?.(value + 1);
    };

    const onMinus = () => {
        if (value <= 1)
            return NotificationHelper.Error(
                'Erreur de saisie',
                props.negativeErrorMessage || 'Ce champ ne peut pas être nulle.'
            );
        setValue((oldValue) => oldValue - 1);

        props.onUpdate?.(value - 1);
    };

    let { text = '', style = {} } = props;
    return (
        <View style={{ ...style }}>
            <Text style={styles.Title}>{text}</Text>
            <View style={[styles.Row, styles.Center]}>
                {!props.readOnly && (
                    <FireflowButton
                        testID="onAddButton"
                        text="+"
                        width={72}
                        height={70}
                        onPress={onAdd}
                    />
                )}
                <View style={styles.Rect}>
                    <Text testID="valueText" style={styles.Field}>
                        {value}
                    </Text>
                </View>
                {!props.readOnly && (
                    <FireflowButton
                        testID="onMinusButton"
                        text="-"
                        width={72}
                        height={70}
                        onPress={onMinus}
                    />
                )}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    Row: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    Center: {
        alignItems: 'center',
    },
    Title: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 25,
        marginBottom: 20,
    },
    Rect: {
        borderWidth: 2,
        paddingLeft: 20,
        paddingRight: 20,
        marginLeft: 20,
        marginRight: 20,
        height: 70,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFF',
    },
    Field: {
        fontSize: 40,
        fontFamily: 'Poppins_600SemiBold',
    },
});
