import { StyleSheet, Text, View } from 'react-native';
import MapView from 'react-native-maps';
import React from 'react';
import { ActivityIndicator, Button } from 'react-native-paper';
import { floor } from 'lodash';
import WeatherModel from '~/models/weather.model';
import { ContextConsumerHook } from '~/store/context';

type MapWeatherProps = {
    dataRequested: boolean;
    dataLoaded: boolean;
    weather: WeatherModel;
    getWeather: Function;
    mapView: MapView | null;
    calculateCentroid: Function;
};

export default function MapWeather(props: MapWeatherProps) {
    const [context] = ContextConsumerHook();

    const convertDegreeToCompassPoint = (wind_deg: number) => {
        const compassPoints = [
            'Nord',
            'Nord Nord Est',
            'Nord Est',
            'Est Nord Est',
            'Est',
            'Est Sud Est',
            'Sud Est',
            'Sud Sud Est',
            'Sud',
            'Sud Sud Ouest',
            'Sud Ouest',
            'Ouest Sud Ouest',
            'Ouest',
            'Ouest Nord Ouest',
            'Nord Ouest',
            'Nord Nord Ouest',
        ];
        const rawPosition = Math.floor(wind_deg / 22.5 + 0.5);
        const arrayPosition = rawPosition % 16;
        return compassPoints[arrayPosition];
    };

    const animateToFire = () => {
        if (context.intervention === undefined) return;
        if (props.mapView) {
            const intervention = context.intervention;
            const r = {
                ...(context.fireZonePolygon.length >= 3
                    ? props.calculateCentroid()
                    : {
                          latitude: parseFloat(intervention.latitude || '0'),
                          longitude: parseFloat(intervention.longitude || '0'),
                      }),
                latitudeDelta: 0.2,
                longitudeDelta: 0.2,
            };
            props.mapView.animateToRegion(r, 1000);
        }
    };

    return (
        <View>
            {!props.dataLoaded && !props.dataRequested && (
                <View
                    style={{
                        flex: 0.3,
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: 300,
                        height: 150,
                        zIndex: 10,
                        backgroundColor: 'rgba(0,0,0,0.6)',
                        borderRadius: 5,
                    }}
                >
                    <Text style={styles.iText}>Touchez la carte pour</Text>
                    <Text style={styles.iText}>obtenir des informations</Text>
                    <Text style={styles.iText}>météorologiques</Text>
                </View>
            )}
            {!props.dataLoaded && props.dataRequested && (
                <View
                    style={{
                        flex: 0.3,
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: 300,
                        height: 150,
                        zIndex: 10,
                        backgroundColor: 'rgba(0,0,0,0.6)',
                        borderRadius: 20,
                    }}
                >
                    <ActivityIndicator
                        animating={true}
                        size={'large'}
                        color={'#f64c4c'}
                    />
                </View>
            )}
            {props.dataLoaded &&
                props.weather.Weather &&
                props.weather.Coord &&
                props.weather.Time && (
                    <View
                        style={{
                            flex: 0.3,
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: 300,
                            height: 150,
                            zIndex: 10,
                            backgroundColor: 'rgba(0,0,0,0.6)',
                            borderRadius: 20,
                        }}
                    >
                        <Text
                            style={styles.text}
                        >{`Température: ${props.weather.Weather.Temp}°C`}</Text>
                        <Text
                            style={styles.text}
                        >{`Humidité: ${props.weather.Weather.Humidity}%`}</Text>
                        <Text style={styles.text}>{`Vit.Vent: ${floor(
                            props.weather.Weather.Wind_speed * 3.6,
                            2
                        )} Km/h`}</Text>
                        <Text
                            style={styles.text}
                        >{`Dir.Vent: ${convertDegreeToCompassPoint(
                            props.weather.Weather.Wind_deg
                        )}`}</Text>
                    </View>
                )}
            <Button
                style={{ marginTop: 5 }}
                icon="target"
                mode="contained"
                labelStyle={{ fontSize: 25 }}
                color={'#f64c4c'}
                onPress={() => {
                    props.getWeather();
                    animateToFire();
                }}
            >
                <Text style={{ fontSize: 16 }}>Cibler le chantier</Text>
            </Button>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        height: '100%',
    },
    contained: {
        flex: 5.5,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
    },
    overlay: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        padding: 10,
        top: 10,
        width: '100%',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    sub_overlay: {
        flexDirection: 'column',
        justifyContent: 'flex-end',
        flex: 1,
        padding: 10,
        top: 10,
        height: '100%',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    legend_display: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#fff',
    },
    iText: {
        fontSize: 20,
        fontStyle: 'italic',
        color: '#fff',
    },
    map: {
        width: '100%',
        height: '100%',
    },
});
