import React from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
    ImageSourcePropType,
} from 'react-native';

type NavBarButton = {
    onPress: Function;
    icon: ImageSourcePropType;
    iconSize: number;
    selected: boolean;
    testId: string;
};

export default function NavBarButton(props: NavBarButton) {
    return (
        <TouchableOpacity
            onPress={() => props.onPress()}
            testID={props.testId}
            style={styles.Row}
        >
            {props.selected && <View style={styles.Selected} />}
            <View style={styles.ImageView}>
                <Image
                    style={{
                        width: props.iconSize,
                        height: props.iconSize,
                    }}
                    source={props.icon}
                />
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    Row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
    },
    ImageView: {
        width: '100%',
        alignItems: 'center',
    },
    Selected: {
        backgroundColor: '#FFFFFF',
        height: '80%',
        width: '5%',
        borderTopRightRadius: 90,
        borderBottomRightRadius: 90,
        position: 'absolute',
    },
});
