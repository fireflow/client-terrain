import React from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
    ImageSourcePropType,
    Text,
} from 'react-native';

type DrawerContentButton = {
    name: string;
    onPress: Function;
    icon: ImageSourcePropType;
    iconSize: number;
    selected: boolean;
    testId: string;
};

export default function DrawerContentButton(props: DrawerContentButton) {
    return (
        <TouchableOpacity
            style={styles.Row}
            testID={props.testId}
            onPress={() => props.onPress()}
        >
            {props.selected && <View style={styles.Selected} />}
            <View style={[styles.Row, { justifyContent: 'flex-end' }]}>
                <Text style={styles.Label}>{props.name}</Text>
                <View style={styles.ImageView}>
                    <Image
                        style={{
                            width: props.iconSize,
                            height: props.iconSize,
                        }}
                        source={props.icon}
                    />
                </View>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    Row: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
    },
    Label: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 24,
        color: '#fff',
        marginRight: 10,
    },
    ImageView: {
        width: 70,
        alignItems: 'center',
    },
    Selected: {
        backgroundColor: '#FFFFFF',
        width: '2%',
        height: '80%',
        borderTopRightRadius: 90,
        borderBottomRightRadius: 90,
        position: 'absolute',
    },
});
