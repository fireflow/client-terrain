import React, { useState, useImperativeHandle, forwardRef, Ref } from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native';

type ModalProps = {
    children: JSX.Element;
};
export interface ModalRef {
    openSelect: () => void;
    closeSelect: () => void;
}

function Modal(props: ModalProps, ref: Ref<ModalRef>) {
    const [open, setOpen] = useState<boolean>(false);

    useImperativeHandle(
        ref,
        () => ({
            openSelect() {
                setOpen(true);
            },
            closeSelect() {
                setOpen(false);
            },
        }),
        []
    );

    return (
        <>
            {open && (
                <TouchableOpacity
                    style={styles.SelectionBackground}
                    onPress={() => setOpen(false)}
                >
                    <TouchableWithoutFeedback>
                        <View style={styles.Selection}>{props.children}</View>
                    </TouchableWithoutFeedback>
                </TouchableOpacity>
            )}
        </>
    );
}

const styles = StyleSheet.create({
    SelectionBackground: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
    },
    Selection: {
        justifyContent: 'center',
        backgroundColor: '#fff',
        elevation: 5,
        borderRadius: 7,
        borderWidth: 0,
        padding: 30,
        zIndex: 1,
    },
});

export default forwardRef(Modal);
