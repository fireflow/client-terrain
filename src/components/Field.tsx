import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Surface } from 'react-native-paper';

type SelectFieldProps = {
    text: string;
    value: string;
    onOpenField: Function;
    style?: object;
    readOnly?: boolean;
};

export default function Field(props: SelectFieldProps) {
    const { text, value, onOpenField, style = {} } = props;

    const openSelectModal = () => {
        if (!props.readOnly) onOpenField();
    };

    return (
        <View>
            <View style={{ ...style }}>
                <Text style={styles.Title}>{text}</Text>
                <Surface>
                    <TouchableOpacity onPress={openSelectModal}>
                        <View style={styles.Rect}>
                            <Text style={styles.Field}>{value}</Text>
                        </View>
                    </TouchableOpacity>
                </Surface>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    Title: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 25,
        marginBottom: 20,
    },
    Rect: {
        borderWidth: 2,
        paddingLeft: 20,
        paddingRight: 20,
        height: 70,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFF',
    },
    Field: {
        fontSize: 40,
        fontFamily: 'Poppins_600SemiBold',
    },
});
