import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import FireFlowButton from '~/components/FireflowButton';
import * as ImagePicker from 'expo-image-picker';

type ImagePicker = {
    onImagePick: Function;
};

export default function ImagePickerButtons(props: ImagePicker) {
    const pickGalery = async () => {
        const result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: false,
            aspect: [4, 3],
            quality: 1,
        });
        props.onImagePick(result);
    };

    const pickCamera = async () => {
        const result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: false,
            aspect: [4, 3],
            quality: 1,
        });
        props.onImagePick(result);
    };

    return (
        <View style={styles.Column}>
            <Text style={styles.Title}>Prendre une photo depuis la :</Text>
            <FireFlowButton
                testID={"gallery-btn"}
                text="Galerie"
                width={259}
                height={70}
                onPress={pickGalery}
                style={{ marginBottom: 15 }}
            />
            <FireFlowButton
                testID={"camera-btn"}
                text="Caméra"
                width={259}
                height={70}
                onPress={pickCamera}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    Title: {
        fontSize: 30,
        fontFamily: 'Poppins_600SemiBold',
        textAlign: 'center',
        marginBottom: 10,
    },
    Column: {
        alignItems: 'center',
    },
});
