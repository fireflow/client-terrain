import React from 'react';
import { StyleSheet, TouchableOpacity, Image, View, Text } from 'react-native';
import { Divider, Drawer, Avatar } from 'react-native-paper';
import { DrawerNavigationHelpers } from '@react-navigation/drawer/lib/typescript/src/types';
import { ContextConsumerHook } from '~/store/context';
import DrawerContentButton from './DrawerContentButton';

type DrawerContentProps = {
    navigation: DrawerNavigationHelpers;
};

export default function DrawerContent(props: DrawerContentProps) {
    const [context, dispatch] = ContextConsumerHook();
    const defaultProfilPicture = require('~/assets/images/defaultProfilPicture.png');

    const navigateTo = (to: string, saveInContext = true) => {
        const { navigation } = props;
        if (saveInContext) dispatch('SetCurrentScreen', to);
        navigation.navigate(to);
    };

    const closeDrawer = () => {
        const { navigation } = props;
        navigation.closeDrawer();
    };

    return (
        <Drawer.Section style={styles.container}>
            <View style={styles.Row}>
                <Image
                    style={styles.logo}
                    source={require('~/assets/fireflow-text.png')}
                />
                <TouchableOpacity
                    testID={'close'}
                    style={styles.ImageView}
                    onPress={closeDrawer}
                >
                    <Image
                        style={{ marginTop: 20, width: 36, height: 36 }}
                        source={require('../assets/icons/close.png')}
                    />
                </TouchableOpacity>
            </View>
            <Divider style={{ backgroundColor: '#fff', width: '75%' }} />
            <DrawerContentButton
                name="Cartographie"
                onPress={() => navigateTo('Home')}
                icon={require('../assets/icons/map.png')}
                selected={context.currentScreen === 'Home'}
                testId="toHome"
                iconSize={70}
            />
            <DrawerContentButton
                name="Canaux Radio"
                onPress={() => navigateTo('Communication')}
                icon={require('../assets/icons/radio.png')}
                selected={context.currentScreen === 'Communication'}
                testId="toCommunication"
                iconSize={70}
            />
            <DrawerContentButton
                name="Statut"
                onPress={() => navigateTo('Status')}
                icon={require('../assets/icons/speaker.png')}
                selected={context.currentScreen === 'Status'}
                testId="toStatus"
                iconSize={60}
            />
            <DrawerContentButton
                name="Urgence"
                onPress={() => navigateTo('Alert')}
                icon={require('../assets/icons/danger.png')}
                selected={context.currentScreen === 'Alert'}
                testId="toAlert"
                iconSize={60}
            />
            <DrawerContentButton
                name="Informations"
                onPress={() => navigateTo('Information')}
                icon={require('../assets/icons/info.png')}
                selected={context.currentScreen === 'Information'}
                testId="toInfo"
                iconSize={56}
            />
            <Divider style={{ backgroundColor: '#fff', width: '75%' }} />
            <TouchableOpacity
                testID={'toProfile'}
                style={styles.Row}
                onPress={() => navigateTo('ProfileAndTeam', false)}
            >
                <Text style={styles.Label}>Profil & équipe</Text>
                <View style={styles.ImageView}>
                    <Avatar.Image
                        size={65}
                        source={
                            context.profilePicture !== undefined
                                ? { uri: context.profilePicture }
                                : defaultProfilPicture
                        }
                    />
                </View>
            </TouchableOpacity>
        </Drawer.Section>
    );
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        flexDirection: 'column',
        backgroundColor: '#FF5B5B',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 10,
    },
    Title: {
        color: '#6A93F4',
        fontSize: 60,
        fontFamily: 'Poppins_700Bold',
        marginRight: 20,
    },
    Label: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 24,
        color: '#fff',
        marginRight: 10,
    },
    Row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        width: '100%',
    },
    ImageView: {
        width: 80,
        alignItems: 'center',
    },
    logo: {
        marginTop: 20,
        width: 220,
        height: 40,
        resizeMode: 'center',
    },
});
