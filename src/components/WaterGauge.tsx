import {StyleSheet, Text, View} from "react-native";
import React, {useState} from 'react';
import NotificationHelper from "~/services/NotificationHelper";
import {Button, ProgressBar} from "react-native-paper";

type WaterGaugeProps = {
    value: number;
    maxValue: number;
    readOnly?: boolean;
    negativeErrorMessage?: string;
    onUpdate?: (value: number) => void;
};

export default function WaterGauge(props: WaterGaugeProps) {
    const [value, setValue] = useState(props.value);

    const onAdd = () => {
        if ((value + (1*props.maxValue/100)) > props.maxValue)
            return NotificationHelper.Error(
                'Erreur de saisie',
                props.negativeErrorMessage || 'La limite ne peut pas être dépassée.'
            );
        setValue((oldValue) => oldValue + (1*props.maxValue/100));
        props.onUpdate?.(value + (1*props.maxValue/100));
    };

    const onMinus = () => {
        if ((value - (1*props.maxValue/100)) < 0)
            return NotificationHelper.Error(
                'Erreur de saisie',
                props.negativeErrorMessage || 'Ce champ ne peut pas être nulle.'
            );
        setValue((oldValue) => oldValue - (1*props.maxValue/100));
        props.onUpdate?.(value - (1*props.maxValue/100));
    };

    return (
        <View style={styles.WaterGauge}>
            <View style={[styles.Row]}>
                {!props.readOnly && (
                    <Button
                        testID={"onAddButton"}
                        style={styles.AddButton}
                        contentStyle={{width: 250, height: 45, alignSelf: 'center', backgroundColor: 'grey'}}
                        labelStyle={{
                            fontFamily: 'Poppins_600SemiBold',
                            fontSize: 25,
                            textTransform: 'none',
                        }}
                        onPress={onAdd}
                    >
                        {'+'}
                    </Button>
                )}
                <View style={styles.Rect}>
                    <Text testID="valueText" style={styles.Field}>
                        {`${Math.round((100*value/props.maxValue))}%`}
                    </Text>
                </View>
                <ProgressBar
                    progress={(100*value/props.maxValue)*0.01}
                    color='blue'
                    style={styles.ProgressBar}
                />
                {!props.readOnly && (
                    <Button
                        testID={"onMinusButton"}
                        style={styles.MinusButton}
                        contentStyle={{width: 250, height: 45, alignSelf: 'center', backgroundColor: 'grey'}}
                        labelStyle={{
                            fontFamily: 'Poppins_600SemiBold',
                            fontSize: 25,
                            textTransform: 'none',
                        }}
                        onPress={onMinus}
                    >
                        {'-'}
                    </Button>
                )}
            </View>


    </View>);
}

const styles = StyleSheet.create({
    WaterGauge: {
        width: 120,
        height: 250,
        backgroundColor: "#5FB3CE",
        marginLeft: 190
    },
    ProgressBar: {
        width: 180,
        height: 120,
        marginTop: -40,
        marginLeft: -30,
        opacity: .5,
        transform: [{ rotate: '-90deg'}],
    },
    Row: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    AddButton: {
        width: 120,
        height: 45,
    },
    MinusButton: {
        width: 120,
        height: 45,
        marginTop: 30,
    },
    Center: {
        alignItems: 'center',
    },
    Rect: {
        paddingLeft: 20,
        marginRight: 20,
        width: 120,
        display: 'flex',
        alignItems: 'center',
        height: 70,
    },
    Field: {
        fontSize: 20,
        fontFamily: 'Poppins_600SemiBold',
        marginLeft: -10,
    },
});