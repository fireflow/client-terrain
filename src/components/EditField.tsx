import React from 'react';
import { StyleSheet, View, TextInput, Text } from 'react-native';
import FireFlowButton from './FireflowButton';

type SelectList = {
    text: string;
    value: string;
    onValueSelected: Function;
    onCancel: Function;
};

export default function EditField(props: SelectList) {
    const { text, value, onValueSelected, onCancel } = props;
    const [inputValue, onChangeInputValue] = React.useState(value);

    return (
        <View>
            <Text style={styles.Title}>{text}</Text>
            <View>
                <TextInput
                    keyboardType="number-pad"
                    value={inputValue}
                    onChangeText={onChangeInputValue}
                    autoFocus={true}
                    style={styles.Input}
                />
                <View style={styles.Row}>
                    <FireFlowButton
                        onPress={() => onValueSelected(inputValue)}
                        text="Valider"
                        width={259}
                        height={70}
                        color={'#54b854'}
                        style={{ marginRight: 20 }}
                    />
                    <FireFlowButton
                        onPress={() => onCancel()}
                        text="Annuler"
                        width={259}
                        height={70}
                    />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    Title: {
        fontSize: 30,
        fontFamily: 'Poppins_600SemiBold',
        textAlign: 'center',
        marginBottom: 10,
    },
    Row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    Input: {
        borderWidth: 2.5,
        borderRadius: 5,
        paddingLeft: 10,
        marginBottom: 20,
        fontSize: 40,
        fontFamily: 'Poppins_600SemiBold',
    },
});
