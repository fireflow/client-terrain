import { useEffect, useState } from 'react';
import * as Notifications from 'expo-notifications';
import Constants from 'expo-constants';
import { Platform } from 'react-native';
import { Subscription } from 'expo-modules-core';

interface NotificationProviderProps {
    children: React.ReactNode;
}

export default function NotificationProvider(props: NotificationProviderProps) {
    const [expoPushToken, setExpoPushToken] = useState<string | undefined>('');
    const [notification, setNotification] =
        useState<Notifications.Notification | null>(null);
    const [notificationListener, setNotificationListener] =
        useState<Subscription | null>(null);
    const [responseListener, setResponseListener] =
        useState<Subscription | null>(null);

    const registerForPushNotificationsAsync = async () => {
        if (Constants.isDevice) {
            const { status: existingStatus } =
                await Notifications.getPermissionsAsync();
            let finalStatus = existingStatus;
            if (existingStatus !== 'granted') {
                const { status } =
                    await Notifications.requestPermissionsAsync();
                finalStatus = status;
            }
            if (finalStatus !== 'granted') {
                alert('Failed to get push token for push notification!');
                return;
            }
            const token = (await Notifications.getExpoPushTokenAsync()).data;
            console.log(token);
            setExpoPushToken(token);
        } else {
            alert('Must use physical device for Push Notifications');
        }

        if (Platform.OS === 'android') {
            Notifications.setNotificationChannelAsync('default', {
                name: 'default',
                importance: Notifications.AndroidImportance.MAX,
                vibrationPattern: [0, 250, 250, 250],
                lightColor: '#FF231F7C',
            });
        }

        return expoPushToken;
    };

    useEffect(() => {
        registerForPushNotificationsAsync().then((token) =>
            setExpoPushToken(token)
        );

        setNotificationListener(
            Notifications.addNotificationReceivedListener(setNotification)
        );

        setResponseListener(
            Notifications.addNotificationResponseReceivedListener(console.log)
        );

        console.log(notification);
        return () => {
            if (notificationListener && responseListener) {
                Notifications.removeNotificationSubscription(
                    notificationListener
                );
                Notifications.removeNotificationSubscription(responseListener);
            }
        };
    }, []);

    return <>{props.children}</>;
}
