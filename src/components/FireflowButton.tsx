import React from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import { Button, Surface } from 'react-native-paper';

type FireflowButtonProps = {
    text: string;
    width: number;
    height: number;
    color?: string;
    icon?: string;
    onPress: () => void;
    style?: StyleProp<ViewStyle>;
    testID?: string;
};

export default function FireflowButton(props: FireflowButtonProps) {
    const {
        text,
        width,
        height,
        color = '#FB4B4F',
        icon,
        onPress,
        style = {},
    } = props;
    return (
        <Surface style={style}>
            <Button
                icon={icon}
                testID={props.testID}
                mode="contained"
                contentStyle={{
                    width,
                    height,
                    backgroundColor: color,
                }}
                labelStyle={{
                    fontFamily: 'Poppins_600SemiBold',
                    fontSize: 25,
                    textTransform: 'none',
                }}
                onPress={onPress}
            >
                {text}
            </Button>
        </Surface>
    );
}
