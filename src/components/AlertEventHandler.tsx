import React, { useEffect, useRef, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Modal from 'react-native-modalbox';
import FireFlowButton from '~/components/FireflowButton';
import { ContextConsumerHook } from '~/store/context';
import { EventPayload, EventsType } from '~/services/events/types';
import * as RootNavigation from '~/RootNavigation';

type AlertEventHandlerProps = {};

export default function AlertEventHandler(props: AlertEventHandlerProps) {
    const [context] = ContextConsumerHook();
    const modal = useRef<Modal>(null);
    const [alerts, setAlerts] = useState<EventPayload[]>([]);
    const [alert, setAlert] = useState<EventPayload | null>(null);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isInTransition, setIsInTransition] = useState(false);

    const handleAlertEvent = (payload: EventPayload) => {
        const newAlerts = [...alerts, payload];
        setAlerts(newAlerts);
        setAlert(newAlerts[0]);
        setIsModalOpen((oldState) => (isInTransition ? oldState : true));
    };

    useEffect(() => {
        context.interventionEventService.registerEventHandler(
            EventsType.Alert,
            handleAlertEvent
        );
    });

    const onClosed = () => {
        const copiedAlerts = JSON.parse(JSON.stringify(alerts));
        copiedAlerts.shift();
        setAlerts(copiedAlerts);
        setIsModalOpen(false);
        setIsInTransition(copiedAlerts.length !== 0);
        setAlert(null);
        if (copiedAlerts.length !== 0) {
            setTimeout(() => {
                setAlert(copiedAlerts[0]);
                setIsModalOpen(true);
                setIsInTransition(false);
            }, 3000);
        }
    };

    const accessAlertLocation = () => {
        RootNavigation.navigate('MainPage', {
            screen: 'Home',
            params: { unitId: alert?.author.id },
        });
        modal.current?.close();
    };

    return (
        <Modal
            ref={modal}
            position={'center'}
            style={styles.modal}
            isOpen={isModalOpen}
            onClosed={onClosed}
        >
            <Text style={styles.title}>Nouvelle urgence !</Text>
            {alert != null && (
                <View>
                    <Text style={styles.message}>
                        {`Urgence envoyée par ${alert.author.firstname} ${alert.author.lastname}.` ||
                            ''}
                    </Text>
                </View>
            )}
            <View>
                <FireFlowButton
                    text={'Localisation'}
                    color={'#54b854'}
                    width={259}
                    height={70}
                    onPress={() => accessAlertLocation()}
                    style={{ marginBottom: 15 }}
                />
                <FireFlowButton
                    text={'Fermer'}
                    width={259}
                    height={70}
                    onPress={() => modal.current?.close()}
                />
            </View>
        </Modal>
    );
}

const styles = StyleSheet.create({
    modal: {
        height: 400,
        width: 500,
        paddingLeft: 15,
        paddingRight: 15,
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 7,
    },
    title: {
        fontFamily: 'Poppins_700Bold',
        fontSize: 30,
    },
    message: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 25,
        textAlign: 'center',
    },
});
