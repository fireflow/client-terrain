import React from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
    ImageSourcePropType,
} from 'react-native';
import { Avatar } from 'react-native-paper';

type ProfilPicture = {
    onPress: Function;
    editable: boolean;
    source?: ImageSourcePropType | string | undefined;
};

export default function ProfilPicture(props: ProfilPicture) {
    const defaultProfilPicture = require('~/assets/images/defaultProfilPicture.png');

    return (
        <View style={{ flexDirection: 'row' }}>
            <Avatar.Image
                size={250}
                source={
                    props.source ? { uri: props.source } : defaultProfilPicture
                }
            />
            {props.editable && (
                <TouchableOpacity
                    style={styles.ProfilPictureEditButton}
                    onPress={() => props.onPress()}
                >
                    <Image
                        source={require('~/assets/icons/edit.png')}
                        style={styles.ProfilPictureEditIcon}
                    />
                </TouchableOpacity>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    ProfilPictureEditButton: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        width: 60,
        height: 60,
        backgroundColor: '#FB4B4F',
        borderRadius: 100,
    },
    ProfilPictureEditIcon: {
        height: 30,
        width: 30,
    },
});
