import { NavigationContainerRef } from '@react-navigation/native';
import * as React from 'react';

export const navigationRef: React.RefObject<NavigationContainerRef<any>> =
    React.createRef<NavigationContainerRef<any>>();

export function navigate(name: string, params: object = {}) {
    navigationRef.current?.navigate(name, params);
}
