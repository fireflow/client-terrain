import { configureFonts, DefaultTheme } from 'react-native-paper';

const fontConfig = {
    default: {
        regular: {
            fontFamily: 'Poppins_400Regular',
            fontWeight: 'normal',
        },
        medium: {
            fontFamily: 'Poppins_500Medium',
            fontWeight: 'normal',
        },
        bold: {
            fontFamily: 'Poppins_700Bold',
            fontWeight: 'normal',
        },
    },
};

const theme = {
    ...DefaultTheme,
    roundness: DefaultTheme.roundness,
    colors: {
        ...DefaultTheme.colors,
    },
    // @ts-ignore
    fonts: configureFonts(fontConfig),
};

export default theme;
