import {VehiclesType} from "~/models/vehicle.model";

export type EventUser = {
    id: string;
    firstname: string;
    lastname: string;
};

export type AlertValue = {
    message: string;
};

export type StatusValue = {
    status: string;
    unitId: string;
};

export type LocationValue = {
    longitude: number;
    latitude: number;
    vehicleType: VehiclesType,
    unitId: string;
};

export type InterventionAssignedValue = {
    message: string;
};

export type OctUpdateValue = {
    message: string;
};

export type UnitPayload = {
    author: EventUser;
    value: LocationValue;
};

export type MarkerValue = {
    longitude: number;
    latitude: number;
    name: string;
    description: string;
};

/*export type PayloadValue = AlertValue | StatusValue | LocationValue

export type EventPayload = {
    author: EventUser;
    type: EventType;
    value: PayloadValue;
}*/

export enum EventsType {
    Status = 'status',
    MarkerUpdated = 'marker_updated',
    MarkerDeleted = 'marker_deleted',
    MarkerCreated = 'marker_created',
    Alert = 'alert',
    UnitLocation = 'unit_location',
    AreaUpdate = 'area_update',
    InterventionAssigned = 'intervention_assigned',
    OctUpdate = 'oct_update',
}

export type EventsMap = {
    [EventsType.Status]: StatusValue;
    [EventsType.MarkerUpdated]: MarkerValue;
    [EventsType.MarkerCreated]: MarkerValue;
    [EventsType.MarkerDeleted]: MarkerValue;
    [EventsType.Alert]: AlertValue;
    [EventsType.UnitLocation]: LocationValue;
    [EventsType.InterventionAssigned]: InterventionAssignedValue;
    [EventsType.OctUpdate]: OctUpdateValue;
};

export type EventPayload = {
    [Key in keyof EventsMap]: {
        author: EventUser;
        type: Key;
        value: EventsMap[Key];
    };
}[keyof EventsMap];

export type EventDispatcher = <
    Type extends EventPayload['type'],
    Payload extends EventsMap[Type]
>(
    type: Type,
    // This line makes it so if there shouldn't be a payload then
    // you only need to call the function with the type, but if
    // there should be a payload then you need the second argument.
    ...payload: Payload extends undefined ? [undefined?] : [Payload]
) => void;
