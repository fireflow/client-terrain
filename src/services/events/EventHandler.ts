import { EventPayload, EventsMap, EventsType } from '~/services/events/types';

/*
1- EventService receives Event
2- EventService send Event to EventHandler
3- EventHandler check if there is any callback registered for this EventsType
4- If any, callback is called with Event as parameter
6- If not, Event is Buffered

Alt
- if a new Callback is registered later, recheck all buffered Event to check if any fit with new callback.
- if any, call callback with Event as parameter and remove Event from buffer
 */

const uid = (): string => {
    return (
        String.fromCharCode(Math.floor(Math.random() * 26) + 97) +
        Math.random().toString(16).slice(2) +
        Date.now().toString(16).slice(4)
    );
};

type HandlerType = {
    [Key in keyof EventsMap]: {
        type: Key;
        handler: (payload: EventPayload) => void;
    };
}[keyof EventsMap];

type EventHandlerDispatcher = <
    Type extends HandlerType['type'],
    Handler extends (payload: EventPayload) => void
>(
    type: Type,
    // This line makes it so if there shouldn't be a payload then
    // you only need to call the function with the type, but if
    // there should be a payload then you need the second argument.
    ...callback: Handler extends undefined ? [undefined?] : [Handler]
) => void;

export class EventHandler {
    buffer: Map<string, EventPayload> = new Map<string, EventPayload>();
    handlers: HandlerType[] = [];

    registerEventHandler: EventHandlerDispatcher = (type, callback) => {
        this.handlers.push({ type: type, handler: callback });

        this.applyBuffer(type);
    };

    handleEvent(event: EventPayload) {
        //this.buffer.push(event);
        const key = uid();
        this.buffer.set(key, event);

        this.applyBuffer(event.type);
    }

    private applyBuffer(type: EventsType) {
        // get events of wanted type
        const events = [...this.buffer.entries()].filter(
            (item) => item[1].type === type
        );
        // get handled of wanted type
        const handlers = this.handlers.filter((h) => h.type === type);

        if (events.length === 0 || handlers.length === 0) {
            return;
        }

        // apply event and delete it from buffer
        events.forEach((event) => {
            handlers.forEach((handler) => handler.handler(event[1]));
            this.buffer.delete(event[0]);
        });
    }
}
