import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    EventDispatcher,
    EventPayload,
    EventsType,
} from '~/services/events/types';
import { EventHandler } from '~/services/events/EventHandler';

export default class EventService {
    url: string = '';
    userId: string = '';
    ws: WebSocket | null = null;
    clientId: string | null = null;
    ready: boolean = false;
    eventHandler: EventHandler = new EventHandler();

    registerEventHandler = this.eventHandler.registerEventHandler;

    async connect(url?: string) {
        if (!!this.ws || (!url && this.url === '')) {
            return;
        }
        if (!!url) {
            this.url = url;
        }
        this.ws = new WebSocket(
            `${this.url}?id=${await AsyncStorage.getItem('fireflow-auth')}`
        );
        this.ws.onerror = (err) => {
            console.log(err);
        };

        this.ws.onopen = (data) => {
            console.log('WebSocket Client Connected');
            this.clientId = this.userId || null;
            this.ready = true;
        };

        this.ws.onclose = (data) => {
            console.log('WebSocket Client Closed');
            this.ws = null;
        };

        this.ws.onmessage = (msg) => {
            const payload: EventPayload = JSON.parse(msg.data);

            if (payload.author.id !== this.clientId) {
                /*if (this.displayCallback !== null) {
                    this.displayCallback(payload);
                }*/
            }
            this.eventHandler.handleEvent(payload);
            //this.handleEvent(payload);
        };
    }

    async reconnect() {
        this.connect(this.url);
    }

    async disconnect() {
        if (this.ws !== null) {
            this.ws.close(1000, 'disconnected'); // see codes here: https://developer.mozilla.org/fr/docs/Web/API/CloseEvent
            this.ready = false;
        }
    }

    sendMessage: EventDispatcher = (type: EventsType, payload) => {
        if (this.ws === null) {
            //console.error('Websocket not initialized. Trying reconnection...');
            this.reconnect();
            return;
        }

        if (this.ready) {
            this.ws.send(
                JSON.stringify({
                    type: type,
                    value: payload,
                })
            );
        }
    };

    /*handleEvent(payload: EventPayload) {
        switch (payload.type) {
            case EventsType.Alert:
                if (this.alertCallback !== null)
                    return this.alertCallback(payload);
                else
                    return;
            default:
                return;
        }
    }*/
}
