import User from '~/models/user.model';
import Vehicle from '~/models/vehicle.model';

export default class Session {
    user: User | null = null;
    vehicle: Vehicle | null = null;

    constructor() {}

    createUserSession(user: User | null) {
        this.user = user;
    }

    deleteUserSession() {
        this.user = null;
    }

    isUserAuth() {
        return this.user !== null;
    }

    createVehicleSession(vehicle: Vehicle | null) {
        this.vehicle = vehicle;
    }

    deleteVehicleSession() {
        this.vehicle = null;
    }

    isVehicleAuth() {
        return this.vehicle !== null;
    }
}
