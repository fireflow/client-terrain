import { Icon, MessageType, showMessage } from 'react-native-flash-message';
import * as Notifications from 'expo-notifications';
import { NotificationRequestInput } from 'expo-notifications';

export default class NotificationHelper {
    private static displayMessage(
        title: string,
        description: string,
        type: MessageType,
        icon: Icon
    ) {
        showMessage({
            message: title,
            description: description,
            type: type,
            icon: icon,
        });
    }

    static Success = (title: string, description: string) =>
        this.displayMessage(title, description, 'success', 'success');
    static Error = (title: string, err: string) =>
        this.displayMessage(title, err, 'danger', 'danger');
    static Info = (title: string, description: string) =>
        this.displayMessage(title, description, 'info', 'info');
    static Warning = (title: string, description: string) =>
        this.displayMessage(title, description, 'warning', 'warning');

    static schedulePushNotification = async (
        notification: NotificationRequestInput
    ) => {
        await Notifications.scheduleNotificationAsync(notification);
    };
}
