import ApplicationConf from '~/ApplicationConf';

class FireflowAPIHelper {
    static async get<Type>(url: string): Promise<Type> {
        return fetch(url, {
            method: 'GET',
            headers: await ApplicationConf.getHeaders(),
            credentials: 'same-origin',
        }).then(async (res) => {
            if (res.ok) return res.json();
            else throw await res.json();
        });
    }

    static async post<Type>(url: string, body: object): Promise<Type> {
        return fetch(url, {
            method: 'POST',
            headers: await ApplicationConf.getHeaders(),
            credentials: 'same-origin',
            body: JSON.stringify(body),
        }).then(async (res) => {
            if (res.ok) return res.json();
            else throw await res.json();
        });
    }

    static async postFormData<Type>(
        url: string,
        formData: FormData
    ): Promise<Type> {
        return new Promise(async (resolve, reject) => {
            const xmlhttp = new XMLHttpRequest();
            xmlhttp.onload = function () {
                if (this.status >= 200 && this.status < 300) {
                    resolve(JSON.parse(xmlhttp.response));
                } else {
                    reject(JSON.parse(xmlhttp.response));
                }
            };
            xmlhttp.onerror = function (e) {
                console.log(e);
            };
            xmlhttp.open('POST', url, true);
            xmlhttp.setRequestHeader(
                'Authorization',
                await ApplicationConf.getAuthorization()
            );
            xmlhttp.send(formData);
        });
    }

    static async put<Type>(url: string, body: object): Promise<Type> {
        return fetch(url, {
            method: 'PUT',
            headers: await ApplicationConf.getHeaders(),
            credentials: 'same-origin',
            body: JSON.stringify(body),
        }).then(async (res) => {
            if (res.ok) return res.json();
            else throw await res.json();
        });
    }
}

export { FireflowAPIHelper };
