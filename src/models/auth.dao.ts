import AsyncStorage from '@react-native-async-storage/async-storage';
import ApplicationConf from '../ApplicationConf';
import { FireflowAPIHelper } from '~/services/FireflowAPIHelper';
import { UserType } from '~/models/user.model';

export default class AuthDao {
    constructor() {}

    static async loginUser(serial: string, password: string): Promise<void> {
        const body = {
            serial,
            password,
            type: UserType.Terrain,
        };
        return FireflowAPIHelper.post<string>(
            ApplicationConf.auth.loginUser(),
            body
        ).then((data) => {
            AsyncStorage.setItem('fireflow-auth', data);
        });
    }

    static async logoutUser(): Promise<void> {
        return FireflowAPIHelper.post<string>(
            ApplicationConf.auth.logoutUser(),
            {}
        ).then((data) => {
            AsyncStorage.setItem('fireflow-auth', data);
        });
    }

    static async loginVehicle(
        serial: string,
        password: string,
        department: number
    ): Promise<void> {
        const body = {
            serial,
            password,
            department,
        };
        return FireflowAPIHelper.post<string>(
            ApplicationConf.auth.loginVehicle(),
            body
        ).then((data) => {
            AsyncStorage.setItem('fireflow-auth', data);
        });
    }
}
