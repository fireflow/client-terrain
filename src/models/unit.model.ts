import { Serializable } from './serializable.model';
import {EventStatus} from "~/models/event.model";

type LeaderData = {
    first_name: string;
    last_name: string;
    serial: string;
};

export enum UnitRoles {
    AgentChief = 'agent_chief',
    GroupChief = 'group_chief',
    ColumnChief = 'column_chief',
}

export default class Unit extends Serializable {
    constructor(
        public id?: string,
        public name?: string,
        public leader?: string,
        public vehicle_id?: string,
        public capacity?: number,
        public intervention_id?: string,
        public role?: UnitRoles,
        public children?: Unit[],
        public nb_men?: number,
        public leader_data?: LeaderData,
        public parent_id?: string,
        public status?: EventStatus
    ) {
        super();
    }

    fromJson(json: object): this {
        super.fromJson(json);

        if (!this.children) this.children = [];
        return this;
    }
}
