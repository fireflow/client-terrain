import { Serializable } from './serializable.model';

export enum UserType {
    Terrain = 'terrain',
    Operator = 'operator',
}

export default class User extends Serializable {
    constructor(
        public id?: string,
        public org_id?: string,
        public matricule?: string,
        public first_name?: string,
        public last_name?: string,
        public profile_picture?: string,
        public grade_id?: string
    ) {
        super();
    }

    public getName(): string {
        return `${this.first_name} ${this.last_name}`;
    }
}
