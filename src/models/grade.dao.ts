import { FireflowAPIHelper } from '~/services/FireflowAPIHelper';
import ApplicationConf from '~/ApplicationConf';
import Grade from '~/models/grade.model';

export default class GradeDao {
    constructor() {}

    static async getGrades(org_id: string): Promise<Grade[]> {
        return FireflowAPIHelper.get<Grade[]>(
            ApplicationConf.grade.getGrades(org_id)
        ).then((data) => {
            console.log(data);
            return data.map((grade: Object) => new Grade().fromJson(grade));
        });
    }
}
