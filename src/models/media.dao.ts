import { Platform } from 'react-native';
import Media from './media.model';
import ApplicationConf from '~/ApplicationConf';
import { FireflowAPIHelper } from '~/services/FireflowAPIHelper';
import * as ImagePicker from 'expo-image-picker';
import { AlbumPicture } from '~/components/Album';
import { LocationObject } from 'expo-location/build/Location.types';

export default class MediaDao {
    constructor() {}

    static async getInterventionPictures(
        interventionId: string
    ): Promise<Media[]> {
        return FireflowAPIHelper.get<Media[]>(
            ApplicationConf.media.getInterventionPictures(interventionId)
        ).then((data) => {
            console.log(data);
            return data;
        });
    }

    static async uploadProfilePicture(
        picture: ImagePicker.ImagePickerResult
    ): Promise<Media> {
        const formData = await this.convertPictureToFormdata(picture.uri);

        return FireflowAPIHelper.postFormData<Media>(
            ApplicationConf.media.uploadProfilePicture(),
            formData
        ).then((data) => {
            console.log(data);
            return new Media().fromJson(data);
        });
    }

    static async uploadInterventionPicture(
        interventionId: string,
        picture: AlbumPicture,
        coordinates: LocationObject
    ): Promise<Media> {
        const formData = await this.convertPictureToFormdata(picture.src);

        formData.append('name', picture.name);
        formData.append('description', picture.description);
        formData.append('lat', coordinates.coords.latitude.toString())
        formData.append('lng', coordinates.coords.longitude.toString())

        return FireflowAPIHelper.postFormData<Media>(
            ApplicationConf.media.uploadInterventionPicture(interventionId),
            formData
        ).then((data) => {
            console.log(data);
            return new Media().fromJson(data);
        });
    }

    private static async convertPictureToFormdata(
        pictureSrc: string
    ): Promise<FormData> {
        const formData = new FormData();
        if (Platform.OS === 'android') {
            const filename = pictureSrc.split('/').pop();
            const type = 'image/' + filename?.split('.').pop();
            const img = {
                uri: pictureSrc,
                name: filename,
                type: type,
            };
            formData.append('file', img);
        } else {
            const blob = await (await fetch(pictureSrc)).blob();
            formData.append('file', blob);
        }

        return formData;
    }
}
