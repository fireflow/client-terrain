import User from './user.model';
import ApplicationConf from '~/ApplicationConf';
import { FireflowAPIHelper } from '~/services/FireflowAPIHelper';

export default class UserDao {
    constructor() {}

    static async getMe(): Promise<User> {
        return FireflowAPIHelper.get<User>(ApplicationConf.user.getMe()).then(
            (data) => {
                console.log(data);
                return new User().fromJson(data);
            }
        );
    }
}
