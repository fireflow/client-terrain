import {Serializable} from './serializable.model';
import {ImageSourcePropType} from "react-native";

export enum VehiclesType {
    BEA = 'bea',
    CCF = 'ccf',
    CCGC = 'ccgc',
    CYNO = 'cyno',
    EPA = 'epa',
    FMOGP = 'fmogp',
    FPT = 'fpt',
    VBS = 'vbs',
    VIRT = 'virt',
    VL = 'vl',
    VLHR = 'vlhr',
    VPI = 'vpi',
    VPL = 'vpl',
    VSAV = 'vsav',
    VSR = 'vsr'
}

export const VehiclesTypesFormatted: { [key in VehiclesType]: { icon: ImageSourcePropType, label: string } } = {
    [VehiclesType.BEA]: {
        icon: require('../assets/images/markers/vehicles/bea.png'),
        label: "BEA"
    },
    [VehiclesType.CCF]: {
        icon: require('../assets/images/markers/vehicles/ccf.png'),
        label: "CCF"
    },
    [VehiclesType.CCGC]: {
        icon: require('../assets/images/markers/vehicles/ccgc.png'),
        label: "CCGC"
    },
    [VehiclesType.CYNO]: {
        icon: require('../assets/images/markers/vehicles/cyno.png'),
        label: "CYNO"
    },
    [VehiclesType.EPA]: {
        icon: require('../assets/images/markers/vehicles/epa.png'),
        label: "EPA"
    },
    [VehiclesType.FMOGP]: {
        icon: require('../assets/images/markers/vehicles/fmogp.png'),
        label: "FMOGP"
    },
    [VehiclesType.FPT]: {
        icon: require('../assets/images/markers/vehicles/fpt.png'),
        label: "FPT"
    },
    [VehiclesType.VBS]: {
        icon: require('../assets/images/markers/vehicles/vbs.png'),
        label: "VBS"
    },
    [VehiclesType.VIRT]: {
        icon: require('../assets/images/markers/vehicles/virt.png'),
        label: "VIRT"
    },
    [VehiclesType.VL]: {
        icon: require('../assets/images/markers/vehicles/vl.png'),
        label: "VL"
    },
    [VehiclesType.VLHR]: {
        icon: require('../assets/images/markers/vehicles/vlhr.png'),
        label: "VLHR"
    },
    [VehiclesType.VPI]: {
        icon: require('../assets/images/markers/vehicles/vpi.png'),
        label: "VPI"
    },
    [VehiclesType.VPL]: {
        icon: require('../assets/images/markers/vehicles/vpl.png'),
        label: "VPL"
    },
    [VehiclesType.VSAV]: {
        icon: require('../assets/images/markers/vehicles/vsav.png'),
        label: "VSAV"
    },
    [VehiclesType.VSR]: {
        icon: require('../assets/images/markers/vehicles/vsr.png'),
        label: "VSR"
    }
};

//TODO: Remplacer les icones firetruck.png par ces images dans Cartography.tsx

export default class Vehicle extends Serializable {
    constructor(
        public id?: string,
        public name?: string,
        public serial?: string,
        public org_id?: string,
        public water_capacity?: number,
        public type?: VehiclesType
    ) {
        super();
    }
}
