import { Serializable } from './serializable.model';

export default class Media extends Serializable {
    constructor(
        public id?: string,
        public id_intervention?: string,
        public link?: string,
        public name?: string,
        public description?: string,
        public address?: string
    ) {
        super();
    }
}
