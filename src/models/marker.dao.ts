import ApplicationConf from '../ApplicationConf';
import { FireflowAPIHelper } from '~/services/FireflowAPIHelper';
import MarkerModel from '~/models/marker.model';

export default class MarkerDao {
    constructor() {}

    static async getAll(interventionId: string): Promise<MarkerModel[]> {
        return FireflowAPIHelper.get<MarkerModel[]>(
            ApplicationConf.marker.getAll(interventionId)
        ).then((data) => {
            console.log(data);
            return data.map((d) => new MarkerModel().fromJson(d));
        });
    }
}
