import { FireflowAPIHelper } from '~/services/FireflowAPIHelper';
import ApplicationConf from '~/ApplicationConf';
import Unit from '~/models/unit.model';

export default class UnitDao {
    constructor() {}

    static async get(unitId: string): Promise<Unit> {
        return FireflowAPIHelper.get<Unit>(
            ApplicationConf.unit.get(unitId)
        ).then((data) => {
            console.log(data);
            return new Unit().fromJson(data);
        });
    }

    static async getMe(): Promise<Unit> {
        return FireflowAPIHelper.get<Unit>(ApplicationConf.unit.getMe()).then(
            (data) => {
                console.log(data);
                return new Unit().fromJson(data);
            }
        );
    }

    static async getAvailable(interventionId: string): Promise<Unit[]> {
        return FireflowAPIHelper.get<Unit[]>(
            ApplicationConf.unit.getAvailable(interventionId)
        ).then((data) => {
            console.log(data);
            return data.map((unit: Object) => new Unit().fromJson(unit));
        });
    }

    static async update(unit: Unit): Promise<Unit> {
        return FireflowAPIHelper.put<Unit>(
            ApplicationConf.unit.update(unit.id!),
            unit
        ).then((data) => {
            console.log(data);
            return new Unit().fromJson(data);
        });
    }

    static async assign(
        interventionId: string,
        UnitsId: string[]
    ): Promise<void> {
        return FireflowAPIHelper.put<void>(
            ApplicationConf.unit.assign(interventionId),
            UnitsId
        );
    }
}
