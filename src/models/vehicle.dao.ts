import Vehicle from './vehicle.model';
import ApplicationConf from '~/ApplicationConf';
import { FireflowAPIHelper } from '~/services/FireflowAPIHelper';

export default class VehicleDao {
    constructor() {}

    static async getMe(): Promise<Vehicle> {
        return FireflowAPIHelper.get<Vehicle>(
            ApplicationConf.vehicle.getMe()
        ).then((data) => {
            console.log(data);
            return new Vehicle().fromJson(data);
        });
    }
}
