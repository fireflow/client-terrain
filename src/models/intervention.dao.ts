import { FireflowAPIHelper } from '~/services/FireflowAPIHelper';
import ApplicationConf from '~/ApplicationConf';
import Intervention from '~/models/intervention.model';

export default class InterventionDao {
    constructor() {}

    static async getAssigned(): Promise<Intervention> {
        return FireflowAPIHelper.get<Intervention>(
            ApplicationConf.intervention.getAssigned()
        ).then((data) => {
            console.log(data);
            return new Intervention().fromJson(data);
        });
    }
}
