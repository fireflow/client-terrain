import { Serializable } from './serializable.model';

export interface Sector {
    name: string;
    ltn: string;
    frequencies: Array<number>;
    area?: string;
    child?: Sector;
}

export default class OctModel extends Serializable {
    constructor(
        public codis?: number,
        public pccos?: Array<number>,
        public transit?: Array<number>,
        public sectors?: Array<Sector>
    ) {
        super();
    }
}
