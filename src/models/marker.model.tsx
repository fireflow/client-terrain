import { Serializable } from './serializable.model';
import { Image } from 'react-native';
import React from 'react';

enum MarkerType {
    RISK = 'risk',
    WATERSPOT = 'waterspot',
    INFO = 'info',
}

type MarkerImage = {
    image: React.ReactElement;
};

export const MarkerTypeToLabel: { [key in MarkerType]: MarkerImage } = {
    [MarkerType.RISK]: {
        image: (
            <Image
                style={{ width: 50, height: 50 }}
                source={require('../assets/images/markers/warning.png')}
            />
        ),
    },
    [MarkerType.WATERSPOT]: {
        image: (
            <Image
                style={{ width: 50, height: 50 }}
                source={require('../assets/images/markers/waterSource.jpg')}
            />
        ),
    },
    [MarkerType.INFO]: {
        image: (
            <Image
                style={{ width: 50, height: 50 }}
                source={require('../assets/images/markers/information.png')}
            />
        ),
    },
};

export default class MarkerModel extends Serializable {
    constructor(
        public id?: string,
        public name?: string,
        public description?: string,
        public latitude?: string,
        public longitude?: string,
        public type?: MarkerType
    ) {
        super();
    }
}
