import { Serializable } from './serializable.model';

interface Coord {
    Lat: number;
    Lon: number;
}

interface WeatherData {
    Type: string;
    Description: number;
    Temp: number;
    Feels_like: number;
    Pressure: number;
    Humidity: number;
    Dew_point: number;
    Uvi: number;
    Clouds: number;
    Visibility: number;
    Wind_speed: number;
    Wind_deg: number;
}

export default class WeatherModel extends Serializable {
    constructor(
        public Coord?: Coord,
        public Time?: string,
        public Weather?: WeatherData
    ) {
        super();
    }
}
