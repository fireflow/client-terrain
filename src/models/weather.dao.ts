import ApplicationConf from '../ApplicationConf';
import { FireflowAPIHelper } from '~/services/FireflowAPIHelper';
import WeatherModel from '~/models/weather.model';

type Coordinate = {
    latitude: number;
    longitude: number;
};

export default class WeatherDao {
    constructor() {}

    static async getWeatherByCoordinates(
        coordinate: Coordinate
    ): Promise<WeatherModel> {
        return FireflowAPIHelper.get<WeatherModel>(
            ApplicationConf.weather.getByCoordinate(
                coordinate.latitude,
                coordinate.longitude
            )
        ).then((data) => {
            return new WeatherModel().fromJson(data);
        });
    }
}
