enum EventStatus {
    Transit = 'transit',
    InFirehouse = 'inFirehouse',
    Engaged = 'engaged',
    Disengaged = 'disengaged',
}

type EventStatusInfo = {
    label: string;
    color: string;
};

const EventStatusFormatted: { [key in EventStatus]: EventStatusInfo } = {
    [EventStatus.Engaged]: { label: 'Engagé', color: '#fda247' },
    [EventStatus.Transit]: { label: 'En transit', color: '#e6cb20' },
    [EventStatus.InFirehouse]: { label: 'En caserne', color: '#54b854' },
    [EventStatus.Disengaged]: { label: 'Désengagé', color: '#fd6148' },
};

export { EventStatus, EventStatusFormatted };
