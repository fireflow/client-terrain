import { Serializable } from './serializable.model';

export default class Grade extends Serializable {
    constructor(
        public id?: string,
        public org_id?: string,
        public name?: string
    ) {
        super();
    }
}
