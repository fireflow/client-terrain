import { Serializable } from './serializable.model';

export type Coordinate = {
    latitude: number;
    longitude: number;
};

export enum InterventionStatus {
    Incipient = 'incipient',
    Growth = 'growth',
    FullyDeveloped = 'fully developed',
    Decay = 'decay',
    Extinct = 'extinct',
}

export enum InterventionCategory {
    Civil = 'civil',
    Forest = 'forest',
    Industrial = 'industrial',
}

export default class Intervention extends Serializable {
    constructor(
        public id?: string,
        public longitude?: string,
        public latitude?: string,
        public status?: InterventionStatus,
        public category?: InterventionCategory,
        public org_id?: string,
        public name?: string,
        public address?: string,
        public description?: string,
        public creation_date?: Date
    ) {
        super();
    }
}
