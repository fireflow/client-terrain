import { RouteProp } from '@react-navigation/native';

export type CustomRouteProp<Type extends object> = RouteProp<
    { params: Type },
    'params'
>;
