import ApplicationConf from '../ApplicationConf';
import { FireflowAPIHelper } from '~/services/FireflowAPIHelper';
import OctModel from '~/models/oct.model';

export default class OctDao {
    constructor() {}

    static async getOct(interventionId: string): Promise<OctModel> {
        return FireflowAPIHelper.get<OctModel>(
            ApplicationConf.oct.get(interventionId)
        ).then((data) => {
            console.log(data);
            return new OctModel().fromJson(data);
        });
    }
}
