import Constants from 'expo-constants';

function getEnvVars() {
    const env = process.env.ENV;

    if (env === null || env === undefined || env === '')
        return Constants.manifest.extra.preprod;
    if (env.indexOf('dev') !== -1) return Constants.manifest.extra.preprod;
    if (env.indexOf('prod') !== -1) return Constants.manifest.extra.prod;
    return Constants.manifest.extra.dev;
}

export default getEnvVars();
