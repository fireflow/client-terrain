import {configure} from "enzyme";
//import React16Adapter from "enzyme-adapter-react-16";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock';

import React from "react";
import EventService from "~/services/events/EventService";
//import MockConstants from "./MockGetEnv";

//jest.mock('expo-constants', () => MockConstants);
jest.mock('expo-constants', () => {
    return {
        manifest: {
            extra: {
                dev: {
                    apiUrl: "http://localhost:4242",
                    webSocketEndpoint: "ws://localhost:4242"
                },
                preprod: {
                    apiUrl: "http://40.89.170.20:4242",
                    webSocketEndpoint: "ws://40.89.170.20:4242"
                },
                prod: {
                    apiUrl: "http://51.11.145.179:4242",
                    webSocketEndpoint: "ws://51.11.145.179:4242"
                }
            }
        }
    }
});

jest.mock('expo-notifications', () => {
    return {
        scheduleNotificationAsync: jest.fn()
    }
});

//scheduleNotificationAsync

jest.mock('./src/services/events/EventService', () => {
    return jest.fn().mockImplementation(() => {
        return {
            sendMessage: jest.fn(),
            connect: jest.fn(),
            disconnect: jest.fn(),
            registerEventHandler: jest.fn()
        };
    });
});
jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);
//jest.mock('@react-navigation/native');
jest.mock('react-navigation', () =>({
    NavigationEvents: 'mockNavigationEvents',
    withNavigation: component => component
}));
jest.mock('react', () => {
    const originReact = jest.requireActual('react');
    const mUseRef = jest.fn();
    return {
        ...originReact,
        useRef: mUseRef,
    };
});
//jest.mock('react-native-paper');
jest.mock('react-native-paper/lib/commonjs/components/Button', () => 'Button');
jest.mock('react-native-paper/lib/commonjs/components/Icon', () => 'Icon');
jest.mock('react-native-paper/lib/commonjs/components/ActivityIndicator', () => 'ActivityIndicator');
jest.mock('react-native-paper/lib/commonjs/components/MaterialCommunityIcon', () => 'MaterialCommunityIcon');
jest.mock('react-native-paper/lib/commonjs/components/FAB/AnimatedFAB', () => 'AnimatedFAB');
jest.mock('expo-location', () => {});
jest.mock('expo-task-manager', () => {});
jest.mock('expo-image-picker', () => {});

jest.mock('react-native/Libraries/Utilities/Platform', () => {
    const Platform = jest.requireActual(
        'react-native/Libraries/Utilities/Platform'
    )
    Platform.OS = 'android'
    return Platform
})

configure({ adapter: new Adapter() });
