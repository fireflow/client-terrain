module.exports = function(api) {
  api.cache(true);

    const rootImportOpts = {
        paths: [
            {
                root: __dirname,
                rootPathPrefix: '~',
                rootPathSuffix: 'src',
            }
        ]
    };
  return {
      presets: ['babel-preset-expo'],
      plugins: [
          'react-native-reanimated/plugin',
          ['babel-plugin-root-import',
          {
              "paths": [
                  {
                      "root": __dirname,
                      "rootPathPrefix": "~",
                      "rootPathSuffix": "src"
                  }
              ]
          }],
          'inline-dotenv',
      ],
      env: {
      production: {
        plugins: [
            'react-native-paper/babel'
        ],
      },
    },
  };
};
