import * as React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from '~/RootNavigation';
import { Provider as PaperProvider } from 'react-native-paper';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import {
    Poppins_400Regular,
    Poppins_500Medium,
    Poppins_600SemiBold,
    Poppins_700Bold,
    useFonts,
} from '@expo-google-fonts/poppins';
import FlashMessage from 'react-native-flash-message';
import Theme from './src/theme/Theme';
import HomePage from './src/views/Home/Home';
import LoginPage from './src/views/Login/Login';
import ProfileAndTeam from './src/views/ProfilAndTeam/ProfileAndTeam';
import EditTeam from './src/views/EditTeam/EditTeam';
import DrawerContent from './src/components/DrawerContent';
import StatusPage from './src/views/Status/StatusPage';
import Alert from './src/views/Alert/Alert';
import Information from './src/views/Information/Information';
import LoginRedirect from './src/views/Login/LoginRedirect';
import AlertEventHandler from './src/components/AlertEventHandler';
import { AppProvider } from '~/store/context';
import CommunicationPage from './src/views/Communication/CommunicationPage';
import * as Notifications from 'expo-notifications';
import * as SplashScreen from 'expo-splash-screen';
import NotificationProvider from './src/components/NotificationProvider';
import {useEffect} from "react";

SplashScreen.preventAutoHideAsync().then()
Notifications.setNotificationHandler({
    handleNotification: async () => ({
        shouldShowAlert: true,
        shouldPlaySound: true,
        shouldSetBadge: true,
    }),
});

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
    const [fontsLoaded] = useFonts({
        Poppins_400Regular,
        Poppins_500Medium,
        Poppins_600SemiBold,
        Poppins_700Bold,
    });

    useEffect(() => {
        if (fontsLoaded) {
            SplashScreen.hideAsync().then();
        }
    }, [fontsLoaded]);

    const MainPage = () => {
        return (
            <Drawer.Navigator
                drawerContent={(navigator) => (
                    <DrawerContent navigation={navigator.navigation} />
                )}
                drawerStyle={{ width: 390 }}
                screenOptions={{ headerShown: false }}
            >
                <Drawer.Screen name="Home" component={HomePage} />
                <Drawer.Screen name="Status" component={StatusPage} />
                <Drawer.Screen name="Alert" component={Alert} />
                <Drawer.Screen name="Information" component={Information} />
                <Drawer.Screen
                    name="Communication"
                    component={CommunicationPage}
                />
            </Drawer.Navigator>
        );
    };

    return (
        <PaperProvider theme={Theme}>
            <AppProvider>
                <NotificationProvider>
                    <NavigationContainer ref={navigationRef}>
                        <Stack.Navigator
                            initialRouteName={'LoginRedirect'}
                            screenOptions={{ header: undefined, headerMode: undefined, headerShown: false }}
                            // screenOptions={{ headerShown: false }}
                        >
                            <Stack.Screen name="Login" component={LoginPage} />
                            <Stack.Screen
                                name="LoginRedirect"
                                component={LoginRedirect}
                            />
                            <Stack.Screen
                                name="MainPage"
                                component={MainPage}
                            />
                            <Stack.Screen
                                name="ProfileAndTeam"
                                component={ProfileAndTeam}
                            />
                            <Stack.Screen
                                name="EditTeam"
                                component={EditTeam}
                            />
                        </Stack.Navigator>
                    </NavigationContainer>
                    <AlertEventHandler />
                    <FlashMessage
                        position="top"
                        titleStyle={{ fontFamily: 'Poppins_600SemiBold' }}
                        textStyle={{ fontFamily: 'Poppins_400Regular' }}
                    />
                </NotificationProvider>
            </AppProvider>
        </PaperProvider>
    );
}
